# README #

## QR Utilities for Android/Java (v1.2.2) ##

SgQr converter files located in SgQr Package.

# Table of Contents
1. [QrUtils](#qrutils)
    1. [Conversion Functions](#conversion)
        1. [SGQR String to NETSQR(Static) String](#conversion1)
        2. [SgQrModel to NETSQR(Static) Model](#conversion2)
        3. [SgQr String to NetsQr(Static) Model](#conversion3)
    2. [Parser Functions](#parser)
        1. [SgQr String](#parser1)
        2. [NetsQr(Static) String](#parser2)
        3. [NetsQr(Dynamic) String](#parser3)
    3. [Checker Functions](#checker)
        1. [Is Valid SgQr](#checker1)
        2. [Is Crc Valid](#checker2)
2. [QrConversionException](#exception)
3. [Model Classes](#models)
    1. [Builders](#builders)
        1. [SG QR](#builders1)
        2. [NETS Static QR](#builders2)
        3. [NETS Dynamic QR](#builders3)
4. [Changelog](#changelog)
    1. [v1.2.2](#changelog6)
    1. [v1.2.1](#changelog5)
    2. [v1.2.0](#changelog4)
    3. [v1.1.1](#changelog3)
    4. [v1.1](#changelog2)
    5. [v1.0](#changelog1)


## General Usage: ##
### QrUtils <a name="qrutils"></a> ###
QrUtils functions are static

#### Conversion <a name="conversion"></a> ####

__Converting SGQR String to NETSQR(Static) String<a name="conversion1"></a>__
>String convertSgQrToNetsQr(String qrData) throws QRConversionException

- Takes in an input SGQR String and returns the corresponding NETSQR String
- Throws QRConversionException with Error description if unable to convert  

__Converting SgQrModel to NETSQR(Static) Model<a name="conversion2"></a>__
>NetsQrModel convertSgQrModelToNetsQrModel(SgQrModel sgQrModel) throws QrConversio

- Takes in an SgQRModel and returns the corresponding NetsQrModel  
- Throws QrConversionException if unable to convert  

__Converting SgQr String to NetsQr(Static) Model<a name="conversion3"></a>__
>NetsQrModel convertSgQrStringToNetsQrModel(String qrData) throws QrConversionException

- Takes in an SgQr String and returns the corresponding NetsQrModel
- Throws QrConversionException if unable to convert

---

#### Parser Functions <a name="parser"></a> ####
__Parse SgQr String to Object<a name="parser1"></a>__
>SgQrModel parseSgQrString(String qrData) throws QrConversionException

- Takes in an input SgQr String and returns the corresponding model
- Throws error if unable to convert  

__Parse NetsQr(Static) String to Object<a name="parser2"></a>__
>NetsQrModel parseNetsQrString(String qrData) throws QrConversionException

- Takes in an input NETSQR(Static) String and returns a NETS Static QR model object
- Throws QrConversionException if unable to parse OR if invalid format, e.g. certain fields too long, too short, etc.

__Parse NetsQr(Dynamic) String to Object<a name="parser3"></a>__
>NetsDynQrModel parseNetsDynQrString(String qrData) throws QrConversionException

- Takes in an input NETSQR(Dynamic) String and returns a NETS Dynamic QR model object
- Throws QrConversionException if unable to parse OR if invalid format, e.g. certain fields too long, too short, etc.

---

#### Checker Functions <a name="checker"></a> ####
__Check is valid SgQr<a name="checker1"></a>__
>boolean isValidSgQrString(String qrData) throws QrConversionException

- Takes in an input SGQR String and returns true if VALID
- Will also check for presence of NETS Merchant information chunk and return false if not present
- Throws QrConversionException with error description if invalid  

__Check if CRC is valid<a name="checker2"></a>__
>boolean crcIsValid(String qrData)

- Takes in an SgQr String
- Return true if valid, false if invalid  

---

#### QrConversionException <a name="exception"></a> ####
- Message contains error code with description on where/why it failed

---

### Sg/Nets QrModels <a name="models"></a> ###
- Model Classes for Sg/Nets QRs
    - SGQR: SgQrModel
    - NETS Static QR: NetsQrModel
    - NETS Dynamic QR: NetsDynQrModel
- Use Getters to access fields, class should generally not need to be modified once generated(either from building or from the parser)  
- Please use the builders when constructing new Sg/NetsQrModels from scratch(if needed)

#### Builders <a name="builders"></a> ####

__SgQrModel.Builder<a name="builder1"></a>__
- Auto generates Crc on build unless explicitly provided

__NetsQrModel.Builder<a name="builder2"></a>__
- Auto generates Number of Records on build  
- Will AutoGenerate signature on build if signature field is null, otherwise will use provided signature  
	- Generated signature will be all 0 if secret string is not provided OR if version is not provided

__NetsDynQrModel.Builder<a name="builder3"></a>__
- Auto generates Crc on build unless explicitly provided
- Auto generates timestamp if not provided


# Changelog <a name="changelog"></a> #

## v1.2.1<a name="changelog6"></a> ##
- Fix for floating MerchantIds.
- Error codes are better defined.

## v1.2.1<a name="changelog5"></a> ##
- Fixed bug in amount parsing
- Now rejects SGQR to NETSQR conversion if currency is not SGD

## v1.2.0<a name="changelog4"></a> ##
- Added changes to NetsQrModel and QrUtils for new NetsStaticQr record ID 10, WebQr
- Added some utility methods to NetsQrModel
- Updated Tests

## v1.1.1 <a name="changelog3"></a> ##
- Now throws error when Txn Reference in SGQR is greater than Max length for NETSQR(16)
- Now throws error when Txn Reference contains invalid characters


## v1.1 <a name="changelog2"></a> ##
- Added support for NETS Dynamic QR
    - Model class: NetsDynQrModel
    - Parser: _QrUtils.ParseNetsDynQrString(String qrData)_
- Fixed Bug in crc checker

## v1.0 <a name="changelog1"></a> ##
- Initial Version