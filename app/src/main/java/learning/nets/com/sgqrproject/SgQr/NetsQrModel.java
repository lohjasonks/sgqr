package learning.nets.com.sgqrproject.SgQr;

import android.support.annotation.NonNull;

import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * NetsQrModel.java
 * Model class for NETS QR data<br>
 * <p>
 * Use the Builder for constructing new instances of this class
 */

public class NetsQrModel {

    //region <Constants for NetsQr>...

    static final String HEADER                   = "NETSqpay";
    static final int    ID_REC_MERCHANT_ID       = 0;
    static final int    ID_REC_MERCHANT_NAME     = 1;
    static final int    ID_REC_TERMINAL_ID       = 2;
    static final int    ID_REC_TXN_AMT           = 3;
    static final int    ID_REC_TIMESTAMP         = 4;
    static final int    ID_REC_BLE_METADATA      = 5;
    static final int    ID_REC_MINMAX_VALUE      = 6;
    static final int    ID_REC_RETRIEVAL_REF     = 7;
    static final int    ID_REC_QR_FORM           = 8;
    static final int    ID_REC_TXN_AMT_QUALIFIER = 9;
    static final int    ID_REC_WEBQR             = 10;

    static final int LEN_HEADER      = 8;
    static final int LEN_VERSION     = 1;
    static final int LEN_NR          = 2;
    static final int LEN_QR_ISSUER   = 10;
    static final int LEN_QR_EXP_DATE = 8;
    static final int LEN_SIG_V1      = 8;
    static final int LEN_SIG_V0      = 64;

    static final int LEN_REC_MERCHANT_ID       = 15;
    static final int LEN_REC_MERCHANT_NAME     = 80;
    static final int LEN_REC_TERMINAL_ID       = 8;
    static final int LEN_REC_TXN_AMT           = 8;
    static final int LEN_REC_TIMESTAMP         = 14;
    static final int LEN_REC_BLE_METADATA      = 40;
    static final int LEN_REC_MINMAX_VALUE      = 16;
    static final int LEN_REC_RETRIEVAL_REF     = 16;
    static final int LEN_REC_QR_FORM           = 1;
    static final int LEN_REC_TXN_AMT_QUALIFIER = 1;
    static final int LEN_RECQR                 = 1;
    static final int LEN_REC_RFU               = 99;
    //endregion...


    String       header;    //  len: 8, val: HEADER
    String       version;   //  len: 1
    String       nr;        //  len: 2, note: number of records
    String       qrIssuer;  //  len: 10
    String       qrExpDate; //  len: 8
    List<Record> records;   //  len: variable
    String       signature; //  len: 64 if version 1 || 8 if version 2


    /**
     * Preferably, use the Builder instead of constructor to instantiate new NetsQrModels
     */
    NetsQrModel() {
        records = new ArrayList<>();
    }

    //region <Getters>...

    public String getHeader() {
        return header;
    }

    public String getVersion() {
        return version;
    }

    public String getNr() {
        return nr;
    }

    public String getQrIssuer() {
        return qrIssuer;
    }

    public String getQrExpDate() {
        return qrExpDate;
    }

    public String getSignature() {
        return signature;
    }

    public String getMid() {
        String recordData = "";
        for (Record record : records) {
            if (record.type.equals(QrUtils.intToString2sf(ID_REC_MERCHANT_ID))) {
                return record.data;
            }
        }
        return recordData;
    }

    public String getTid() {
        for (Record record : records) {
            if (record.type.equals(QrUtils.intToString2sf(ID_REC_TERMINAL_ID))) {
                return record.data;
            }
        }
        return null;
    }

    public String getMerchantName() {
        String recordData = "";
        for (Record record : records) {
            if (record.type.equals(QrUtils.intToString2sf(ID_REC_MERCHANT_NAME))) {
                return record.data;
            }
        }
        return recordData;
    }

    public String getTxnAmt() {
        for (Record record : records) {
            if (record.type.equals(QrUtils.intToString2sf(ID_REC_TXN_AMT))) {
                return record.data;
            }
        }
        return null;
    }

    public String getTxnTimestamp() {
        for (Record record : records) {
            if (record.type.equals(QrUtils.intToString2sf(ID_REC_TIMESTAMP))) {
                return record.data;
            }
        }
        return null;
    }

    public String getMinMaxValue() {
        for (Record record : records) {
            if (record.type.equals(QrUtils.intToString2sf(ID_REC_MINMAX_VALUE))) {
                return record.data;
            }
        }
        return null;
    }

    public String getTxnRefNumber() {
        for (Record record : records) {
            if (record.type.equals(QrUtils.intToString2sf(ID_REC_RETRIEVAL_REF))) {
                return record.data;
            }
        }
        return null;
    }

    public String getQrForm() {
        for (Record record : records) {
            if (record.type.equals(QrUtils.intToString2sf(ID_REC_QR_FORM))) {
                return record.data;
            }
        }
        return null;
    }

    public String getTxnAmtQualifier() {
        for (Record record : records) {
            if (record.type.equals(QrUtils.intToString2sf(ID_REC_TXN_AMT_QUALIFIER))) {
                return record.data;
            }
        }
        return null;
    }

    public String getWebQr() {
        for (Record record : records) {
            if (record.type.equals(QrUtils.intToString2sf(ID_REC_WEBQR))) {
                return record.data;
            }
        }
        return null;
    }

    public String getRecordData(int id) {
        for (Record record : records) {
            if (record.type.equals(QrUtils.intToString2sf(id))) {
                return record.data;
            }
        }
        return null;
    }
    //endregion.........

    /**
     * @return true if txn reference is present and length == 0
     */
    public boolean isTxnReferenceProvidedByUser() {
        for (Record record : records) {
            if (record.type.equals(QrUtils.intToString2sf(ID_REC_RETRIEVAL_REF))) {
                return record.data != null && record.data.length() == 0;
            }
        }
        return false;
    }


    /**
     * @return true if WebQr data field is present and value == 1
     */
    public boolean isWebQr() {
        String webQr = getWebQr();
        return webQr != null && webQr.equals("1");
    }

    /**
     * @return The ASCII String representation of the corresponding NetsQr data
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(header)
                .append(version)
                .append(nr)
                .append(qrIssuer)
                .append(qrExpDate);
        for (Record record : records) {
            sb.append(record.toString());
        }
        sb.append(signature);
        return sb.toString();
    }


    /**
     * @return String which shows class contents(for debugging etc)
     */
    public String toDebugString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Header: ").append(header)
                .append("\nVersion: ").append(version)
                .append("\nNum Records: ").append(nr)
                .append("\nQrIssuer: ").append(qrIssuer)
                .append("\nQrExpDate: ").append(qrExpDate);
        for (Record record : records) {
            sb.append("\nRecord: ").append(record.toString2());
        }
        sb.append("\nSignature: ").append(signature);
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        NetsQrModel model = (NetsQrModel) o;
        if (!(this.header.equals(model.header)
              && this.version.equals(model.version)
              && this.nr.equals(model.nr)
              && this.qrIssuer.equals(model.qrIssuer)
              && this.qrExpDate.equals(model.qrExpDate)
              && this.signature.equals(model.signature))) {
            return false;
        }
        if(this.records.size() != model.records.size()){
            return false;
        }
        for(int i = 0; i < this.records.size(); i++){
            if(!Objects.equals(records.get(i), model.records.get(i))){
                return false;
            }
        }
        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hash(header, version, nr, qrIssuer, qrExpDate, records, signature);
    }

    /**
     * Builder class for NetsQrModel<br>
     * Signature is automatically computed when built unless provided explicitly
     */
    public static class Builder {
        private String header;    //  len: 8, val: HEADER
        private String version;   //  len: 1
        private String qrIssuer;  //  len: 10
        private String qrExpDate; //  len: 8
        private String mid;
        private String merchantName;
        private String tid;
        private String txnAmout;
        private String txnTimestamp;
        private String minMaxValue;
        private String txnRefNumber;
        private String qrForm;
        private String txnAmountQualifier;
        private String webQr;
        private String secretString;
        private String signature;
        private ArrayList<Record> unknownRecords = new ArrayList<>();

        //region <Builder Methods>...
        public Builder header(String header) {
            this.header = header;
            return this;
        }

        public Builder version(String version) {
            this.version = version;
            return this;
        }

        public Builder qrIssuer(String qrIssuer) {
            this.qrIssuer = qrIssuer;
            return this;
        }

        public Builder qrExpDate(String qrExpDate) {
            this.qrExpDate = qrExpDate;
            return this;
        }

        public Builder mid(String mid) {
            this.mid = mid;
            return this;
        }

        public Builder merchantName(String merchantName) {
            this.merchantName = merchantName;
            return this;
        }

        public Builder tid(String tid) {
            this.tid = tid;
            return this;
        }

        public Builder txnAmout(String txnAmout) {
            this.txnAmout = txnAmout;
            return this;
        }

        public Builder txnTimestamp(String txnTimestamp) {
            this.txnTimestamp = txnTimestamp;
            return this;
        }

        public Builder minMaxValue(String minMaxValue) {
            this.minMaxValue = minMaxValue;
            return this;
        }

        public Builder txnRefNumber(String txnRefNumber) {
            this.txnRefNumber = txnRefNumber;
            return this;
        }

        public Builder qrForm(String qrForm) {
            this.qrForm = qrForm;
            return this;
        }

        public Builder webQr(String webQr) {
            this.webQr = webQr;
            return this;
        }

        public Builder txnAmountQualifier(String txnAmountQualifier) {
            this.txnAmountQualifier = txnAmountQualifier;
            return this;
        }

        public Builder secretString(String secretString) {
            this.secretString = secretString;
            return this;
        }

        public Builder signature(String signature) {
            this.signature = signature;
            return this;
        }


        /**
         * Add a record directly to the list of records
         *
         * @param id   Record ID
         * @param data Data to store into the record
         * @return Builder.this;
         */
        public Builder addRecord(int id, String data) {
            switch (id) {
                case ID_REC_MERCHANT_ID:
                    this.mid = data;
                    break;
                case ID_REC_MERCHANT_NAME:
                    this.merchantName = data;
                    break;
                case ID_REC_TERMINAL_ID:
                    this.tid = data;
                    break;
                case ID_REC_TXN_AMT:
                    this.txnAmout = data;
                    break;
                case ID_REC_TIMESTAMP:
                    this.txnTimestamp = data;
                    break;
                case ID_REC_MINMAX_VALUE:
                    this.minMaxValue = data;
                    break;
                case ID_REC_RETRIEVAL_REF:
                    this.txnRefNumber = data;
                    break;
                case ID_REC_QR_FORM:
                    this.qrForm = data;
                    break;
                case ID_REC_TXN_AMT_QUALIFIER:
                    this.txnAmountQualifier = data;
                    break;
                case ID_REC_WEBQR:
                    this.webQr = data;
                    break;
                default:
                    if (data != null) {
                        unknownRecords.add(new Record(id, data.length(), data));
                    }
                    break;
            }
            return this;
        }
        //endregion


        public NetsQrModel build() {
            NetsQrModel netsQrModel       = new NetsQrModel();
            int         numFieldsExcluded = 0;
            netsQrModel.header = this.header;
            netsQrModel.version = this.version;
            netsQrModel.qrIssuer = this.qrIssuer;
            netsQrModel.qrExpDate = this.qrExpDate;


            if (mid != null) {
                netsQrModel.records.add(new Record(ID_REC_MERCHANT_ID, this.mid.length(), this.mid));
            }
            if (merchantName != null) {
                if (version.equals("0")) {
                    netsQrModel.records.add(new Record(ID_REC_MERCHANT_NAME, this.merchantName.length(), this.merchantName));
                } else {
                    numFieldsExcluded++;
                }
            }
            if (tid != null) {
                netsQrModel.records.add(new Record(ID_REC_TERMINAL_ID, this.tid.length(), this.tid));
            }
            if (txnAmout != null) {
                if (version.equals("0")) {
                    netsQrModel.records.add(new Record(ID_REC_TXN_AMT, this.txnAmout.length(), this.txnAmout));
                } else {
                    numFieldsExcluded++;
                }
            }
            if (txnTimestamp != null) {
                netsQrModel.records.add(new Record(ID_REC_TIMESTAMP, this.txnTimestamp.length(), this.txnTimestamp));
            }
            if (minMaxValue != null) {
                netsQrModel.records.add(new Record(ID_REC_MINMAX_VALUE, this.minMaxValue.length(), this.minMaxValue));
            }
            if (txnRefNumber != null) {
                if (version.equals("0")) {
                    netsQrModel.records.add(new Record(ID_REC_RETRIEVAL_REF, this.txnRefNumber.length(), this.txnRefNumber));
                } else {
                    numFieldsExcluded++;
                }
            }
            if (qrForm != null) {
                netsQrModel.records.add(new Record(ID_REC_QR_FORM, this.qrForm.length(), this.qrForm));
            }
            if (txnAmountQualifier != null) {
                netsQrModel.records.add(new Record(ID_REC_TXN_AMT_QUALIFIER, this.txnAmountQualifier.length(), this.txnAmountQualifier));
            }
            if (webQr != null) {
                netsQrModel.records.add(new Record(ID_REC_WEBQR, this.webQr.length(), this.webQr));
            }

            if (!unknownRecords.isEmpty()) {
                netsQrModel.records.addAll(unknownRecords);
            }

            int numRecords = netsQrModel.records.size();
            if (version.equals("1")) {
                numRecords += numFieldsExcluded;
            }
            netsQrModel.nr = Integer.toString(numRecords);
            if (numRecords < 10) {
                netsQrModel.nr = "0" + netsQrModel.nr;
            }


            if (this.signature != null) {
                String maxPadding = "0000000000000000000000000000000000000000000000000000000000000000";
                int    amtToPad   = 64 - this.signature.length();
                String padding    = "";
                if (amtToPad > 0) {
                    padding = maxPadding.substring(0, amtToPad);
                }
                netsQrModel.signature = this.signature + padding;
            } else {
                if (this.secretString != null && this.version != null) {
                    netsQrModel.signature = "";
                    String preSignatureOutput = netsQrModel.toString();
                    netsQrModel.signature = generateSignature(this.secretString, this.version, preSignatureOutput);
                } else {
                    netsQrModel.signature = "0000000000000000000000000000000000000000000000000000000000000000";
                }
            }
            if (version.equals("1")) {
                if (txnRefNumber != null) {
                    netsQrModel.records.add(new Record(ID_REC_RETRIEVAL_REF, this.txnRefNumber.length(), this.txnRefNumber));
                }
                if (txnAmout != null) {
                    netsQrModel.records.add(new Record(ID_REC_TXN_AMT, this.txnAmout.length(), this.txnAmout));
                }
                if (merchantName != null) {
                    netsQrModel.records.add(new Record(ID_REC_MERCHANT_NAME, this.merchantName.length(), this.merchantName));
                }
                Collections.sort(netsQrModel.records);
            }

            return netsQrModel;
        }


        //region <Non-Public methods>...
        private String generateSignature(String secretString, String version, String data) {
            String dataWithSecret = data + secretString;
            try {
                MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
                messageDigest.update(dataWithSecret.getBytes("UTF-8"));
                byte[] digest         = messageDigest.digest();
                String signatureAsHex = bytesToHex(digest);
                if (version.equals("1")) {
                    return signatureAsHex.substring(0, 8) + "00000000000000000000000000000000000000000000000000000000";
                }
                return signatureAsHex;
            } catch (Exception e) {
                return "0000000000000000000000000000000000000000000000000000000000000000";
            }
        }

        String bytesToHex(byte[] bytes) {
            final char[] hexArray = "0123456789ABCDEF".toCharArray();
            char[]       hexChars = new char[bytes.length * 2];
            for (int j = 0; j < bytes.length; j++) {
                int v = bytes[j] & 0xFF;
                hexChars[j * 2] = hexArray[v >>> 4];
                hexChars[j * 2 + 1] = hexArray[v & 0x0F];
            }
            return new String(hexChars);
        }

        private boolean isNullOrEmpty(String s) {
            return s == null || s.isEmpty();
        }
        //endregion
    }



    /**
     * Record class which stores an [ID], [Length of data] and [Data]
     */
    static class Record implements Comparable<Record> {

        String type;    // len: 2
        String length;  // len: 2
        String data;    // len: var

        Record(String type, String length, String data) {
            this.type = type;
            this.length = length;
            this.data = data;
        }

        Record(int type, int length, String data) {
            this.type = QrUtils.intToString2sf(type);
            this.length = QrUtils.intToString2sf(length);
            this.data = data;
        }

        Record(String type, int length, String data) {
            this.type = type;
            this.length = QrUtils.intToString2sf(length);
            this.data = data;
        }

        Record(int type, String length, String data) {
            this.type = QrUtils.intToString2sf(type);
            this.length = length;
            this.data = data;
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append(type)
                    .append(length)
                    .append(data);
            return sb.toString();
        }

        String toString2() {
            StringBuilder sb = new StringBuilder();
            sb.append("Type: ").append(type)
                    .append(", Length: ").append(length)
                    .append(", Data: ").append(data);
            return sb.toString();
        }

        @Override
        public int compareTo(@NonNull Record record) {
            int thisValue  = Integer.valueOf(this.type);
            int otherValue = Integer.valueOf(record.type);

            return thisValue - otherValue;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            Record record = (Record) o;
            return this.type.equals(record.type)
                   && this.length.equals(record.length)
                   && this.data.equals(record.data);
        }

        @Override
        public int hashCode() {

            return Objects.hash(type, length, data);
        }
    }


}
