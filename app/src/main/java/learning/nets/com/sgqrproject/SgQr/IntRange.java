package learning.nets.com.sgqrproject.SgQr;

/**
 * IntRange.java
 */

class IntRange {

    int min;
    int max;

    IntRange(int min, int max) {
        this.min = min;
        this.max = max;
    }

    boolean isInRange(int valueToCheck) {
        return min <= valueToCheck && valueToCheck <= max;
    }
}
