package learning.nets.com.sgqrproject.SgQr;

import android.annotation.SuppressLint;

import java.util.HashMap;
import java.util.TreeSet;

/**
 * SgQrModel.java
 * Model class for SG QR data<br>
 *
 * Use the Builder to generate new instances of this class
 */

public class SgQrModel {

    String                       payloadFormatIndicator;                                     // len: 2
    String                       pointOfInitiationMethod;                                    // len: 2
    HashMap<Integer, SgQrObject> merchantAcctInfoEmv;                                        // len: <=99
    HashMap<Integer, SgQrObject> merchantAcctInfoSg;                                         // len: <=99     /
    String                       sgQrIdentityInformation;                                    // len: <=99       //TODO TBD BY MAS
    String                       merchantCategoryCode;                                       // len: 04
    String                       transactionCurrency;                                        // len: 03
    String                       transactionAmount;                                          // len: <=13
    String                       tipOrConvenienceIndicator;                                  // len: 2
    String                       valueOfConvenienceFeeFixed;                                 // len: <=13
    String                       valueOfConvenienceFeePercentage;                            // len: <=5
    String                       merchantCountryCode;                                        // len: 2
    String                       merchantName;                                               // len: <=25     /
    String                       merchantCity;                                               // len: <=15
    String                       postalCode;                                                 // len: 6 - 10
    HashMap<Integer, SgQrObject> additionalData;                                             // len: <=99
    HashMap<Integer, SgQrObject> merchantInformationLanguageTemplate;                        // len: <=99
    HashMap<Integer, SgQrObject> rfuForEmv;                                                  // len: <=99
    HashMap<Integer, SgQrObject> unreservedTemplates;                                        // len: <=99
    String                       crc;                                                        // len: 4


    /**
     * Please use the SgQrModel.Builder to instantiate new SgQrModels
     */
    @SuppressLint("UseSparseArrays")
    SgQrModel() {
        merchantAcctInfoEmv = new HashMap<>();
        merchantAcctInfoSg = new HashMap<>();
        rfuForEmv = new HashMap<>();
        unreservedTemplates = new HashMap<>();
        additionalData = new HashMap<>();
        merchantInformationLanguageTemplate = new HashMap<>();


        payloadFormatIndicator = "";
        pointOfInitiationMethod = "";
        sgQrIdentityInformation = "";
        merchantCategoryCode = "";
        transactionCurrency = "";
        transactionAmount = "";
        tipOrConvenienceIndicator = "";
        valueOfConvenienceFeeFixed = "";
        valueOfConvenienceFeePercentage = "";
        merchantCountryCode = "";
        merchantName = "";
        merchantCity = "";
        postalCode = "";
        crc = "";

        //additionalData
        //merchantInformationLanguageTemplate

    }

    /**
     * @return The ASCII string containing the corresponding SGQR representation
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        int lenAddnData = 0;
        int lenLangData = 0;
        if (payloadFormatIndicator != null && !payloadFormatIndicator.isEmpty()) {
            sb.append(QrUtils.intToString2sf(SgQrConsts.ID_PAYLOAD_FORMAT_INDICATOR))
                    .append(QrUtils.intToString2sf(payloadFormatIndicator.length()))
                    .append(payloadFormatIndicator);
        }
        if (pointOfInitiationMethod != null && !pointOfInitiationMethod.isEmpty()) {
            sb.append(QrUtils.intToString2sf(SgQrConsts.ID_POINT_OF_INITIATION_METHOD))
                    .append(QrUtils.intToString2sf(pointOfInitiationMethod.length()))
                    .append(pointOfInitiationMethod);
        }
        for (SgQrObject object : new TreeSet<>(merchantAcctInfoEmv.values())) {
            sb.append(object.id)
                    .append(object.length)
                    .append(object.data);
        }
        for (SgQrObject object : new TreeSet<>(merchantAcctInfoSg.values())) {
            sb.append(object.id)
                    .append(object.length)
                    .append(object.data);
        }
        if (sgQrIdentityInformation != null && !sgQrIdentityInformation.isEmpty()) {
            sb.append(QrUtils.intToString2sf(SgQrConsts.ID_SGQR_ID_INFORMATION))
                    .append(QrUtils.intToString2sf(sgQrIdentityInformation.length()))
                    .append(sgQrIdentityInformation);
        }
        if (merchantCategoryCode != null && !merchantCategoryCode.isEmpty()) {
            sb.append(QrUtils.intToString2sf(SgQrConsts.ID_MERCHANT_CATEGORY_CODE))
                    .append(QrUtils.intToString2sf(merchantCategoryCode.length()))
                    .append(merchantCategoryCode);
        }
        if (transactionCurrency != null && !transactionCurrency.isEmpty()) {
            sb.append(QrUtils.intToString2sf(SgQrConsts.ID_TRANSACTION_CURRENCY))
                    .append(QrUtils.intToString2sf(transactionCurrency.length()))
                    .append(transactionCurrency);
        }
        if (transactionAmount != null && !transactionAmount.isEmpty()) {
            sb.append(QrUtils.intToString2sf(SgQrConsts.ID_TRANSACTION_AMOUNT))
                    .append(QrUtils.intToString2sf(transactionAmount.length()))
                    .append(transactionAmount);
        }
        if (tipOrConvenienceIndicator != null && !tipOrConvenienceIndicator.isEmpty()) {
            sb.append(QrUtils.intToString2sf(SgQrConsts.ID_TIP_OR_CONVENIENCE_INDICATOR))
                    .append(QrUtils.intToString2sf(tipOrConvenienceIndicator.length()))
                    .append(tipOrConvenienceIndicator);
        }
        if (valueOfConvenienceFeeFixed != null && !valueOfConvenienceFeeFixed.isEmpty()) {
            sb.append(QrUtils.intToString2sf(SgQrConsts.ID_VALUE_OF_CONVENIENCE_FEE_FIXED))
                    .append(QrUtils.intToString2sf(valueOfConvenienceFeeFixed.length()))
                    .append(valueOfConvenienceFeeFixed);
        }
        if (valueOfConvenienceFeePercentage != null && !valueOfConvenienceFeePercentage.isEmpty()) {
            sb.append(QrUtils.intToString2sf(SgQrConsts.ID_VALUE_OF_CONVENIENCE_FEE_PERCENTAGE))
                    .append(QrUtils.intToString2sf(valueOfConvenienceFeePercentage.length()))
                    .append(valueOfConvenienceFeePercentage);
        }
        if (merchantCountryCode != null && !merchantCountryCode.isEmpty()) {
            sb.append(QrUtils.intToString2sf(SgQrConsts.ID_COUNTRY_CODE))
                    .append(QrUtils.intToString2sf(merchantCountryCode.length()))
                    .append(merchantCountryCode);
        }
        if (merchantName != null && !merchantName.isEmpty()) {
            sb.append(QrUtils.intToString2sf(SgQrConsts.ID_MERCHANT_NAME))
                    .append(QrUtils.intToString2sf(merchantName.length()))
                    .append(merchantName);
        }
        if (merchantCity != null && !merchantCity.isEmpty()) {
            sb.append(QrUtils.intToString2sf(SgQrConsts.ID_MERCHANT_CITY))
                    .append(QrUtils.intToString2sf(merchantCity.length()))
                    .append(merchantCity);
        }
        if (postalCode != null && !postalCode.isEmpty()) {
            sb.append(QrUtils.intToString2sf(SgQrConsts.ID_POSTAL_CODE))
                    .append(QrUtils.intToString2sf(postalCode.length()))
                    .append(postalCode);
        }
        for (SgQrObject object : new TreeSet<>(additionalData.values())) {
            lenAddnData += object.id.length() + object.length.length() + object.data.length();
        }
        if(lenAddnData > 0) {
            sb.append(QrUtils.intToString2sf(SgQrConsts.ID_ADDITIONAL_TEMPLATE_DATA))
                    .append(QrUtils.intToString2sf(lenAddnData));
        }
        for (SgQrObject object : new TreeSet<>(additionalData.values())) {
            sb.append(object.id)
                    .append(object.length)
                    .append(object.data);
        }
        for (SgQrObject object : new TreeSet<>(merchantInformationLanguageTemplate.values())) {
            lenLangData += object.id.length() + object.length.length() + object.data.length();
        }
        if(lenLangData > 0 ){
            sb.append(QrUtils.intToString2sf(SgQrConsts.ID_MERCHANT_INFO_LANGUAGE_TEMPLATE))
                    .append(QrUtils.intToString2sf(lenLangData));
        }
        for (SgQrObject object : new TreeSet<>(merchantInformationLanguageTemplate.values())) {
            sb.append(object.id)
                    .append(object.length)
                    .append(object.data);
        }
        for (SgQrObject object : new TreeSet<>(rfuForEmv.values())) {
            sb.append(object.id)
                    .append(object.length)
                    .append(object.data);
        }
        for (SgQrObject object : new TreeSet<>(unreservedTemplates.values())) {
            sb.append(object.id)
                    .append(object.length)
                    .append(object.data);
        }
        if (crc != null && !crc.isEmpty()) {
            sb.append(QrUtils.intToString2sf(SgQrConsts.ID_CRC))
                    .append(QrUtils.intToString2sf(crc.length()))
                    .append(crc);
        }
        return sb.toString();
    }


    /**
     * @return String which shows values of the class variables(For debugging etc)
     */
    public String toDisplayString() {
        StringBuilder toString = new StringBuilder("SgQrModel{" +
                                                           "payloadFormatIndicator='" + payloadFormatIndicator + '\n' +
                                                           ", pointOfInitiationMethod='" + pointOfInitiationMethod + '\n' +
                                                           ", merchantAcctInfoEmv=" + '\n');
        for (Integer key : merchantAcctInfoEmv.keySet()) {
            toString.append(merchantAcctInfoEmv.get(key).toString()).append('\n');
        }

        toString.append(", merchantAcctInfoSg=");
        for (Integer key : merchantAcctInfoSg.keySet()) {
            toString.append(merchantAcctInfoSg.get(key).toString()).append('\n');
        }
        toString.append(", sgQrIdentityInformation='").append(sgQrIdentityInformation).append('\n')
                .append(", merchantCategoryCode='").append(merchantCategoryCode).append('\n')
                .append(", transactionCurrency='").append(transactionCurrency).append('\n')
                .append(", transactionAmount='").append(transactionAmount).append('\n')
                .append(", tipOrConvenienceIndicator='").append(tipOrConvenienceIndicator).append('\n')
                .append(", valueOfConvenienceFeeFixed='").append(valueOfConvenienceFeeFixed).append('\n')
                .append(", valueOfConvenienceFeePercentage='").append(valueOfConvenienceFeePercentage).append('\n')
                .append(", merchantCountryCode='").append(merchantCountryCode).append('\n')
                .append(", merchantName='").append(merchantName).append('\n')
                .append(", merchantCity='").append(merchantCity).append('\n')
                .append(", postalCode='").append(postalCode).append('\n')
                .append(
//          ", additionalData=" + additionalData.toString() +
//          ", merchantInformationLanguageTemplate=" + merchantInformationLanguageTemplate.toString() +
                        ", rfuForEmv=").append(rfuForEmv).append('\n')
                .append(", unreservedTemplates=").append(unreservedTemplates).append('\n')
                .append(", crc='").append(crc).append('\n')
                .append('}');
        return toString.toString();
    }

    //region <Getters>...
    public String getPayloadFormatIndicator() {
        return payloadFormatIndicator;
    }

    public String getPointOfInitiationMethod() {
        return pointOfInitiationMethod;
    }

    public HashMap<Integer, SgQrObject> getMerchantAcctInfoEmv() {
        return merchantAcctInfoEmv;
    }

    public HashMap<Integer, SgQrObject> getMerchantAcctInfoSg() {
        return merchantAcctInfoSg;
    }

    public String getSgQrIdentityInformation() {
        return sgQrIdentityInformation;
    }

    public String getMerchantCategoryCode() {
        return merchantCategoryCode;
    }

    public String getTransactionCurrency() {
        return transactionCurrency;
    }

    public String getTransactionAmount() {
        return transactionAmount;
    }

    public String getTipOrConvenienceIndicator() {
        return tipOrConvenienceIndicator;
    }

    public String getValueOfConvenienceFeeFixed() {
        return valueOfConvenienceFeeFixed;
    }

    public String getValueOfConvenienceFeePercentage() {
        return valueOfConvenienceFeePercentage;
    }

    public String getMerchantCountryCode() {
        return merchantCountryCode;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public String getMerchantCity() {
        return merchantCity;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public HashMap<Integer, SgQrObject> getAdditionalData() {
        return additionalData;
    }

    public HashMap<Integer, SgQrObject> getMerchantInformationLanguageTemplate() {
        return merchantInformationLanguageTemplate;
    }

    public HashMap<Integer, SgQrObject> getRfuForEmv() {
        return rfuForEmv;
    }

    public HashMap<Integer, SgQrObject> getUnreservedTemplates() {
        return unreservedTemplates;
    }

    public String getCrc() {
        return crc;
    }

    String getAdtBillNumber() {
        SgQrObject tempObject;
        if ((tempObject = additionalData.get(SgQrConsts.AddnData.ID_BILL_NUMBER)) != null) {
            return tempObject.data;
        } else {
            return null;
        }
    }

    String getAdtMobileNumber() {
        SgQrObject tempObject;
        if ((tempObject = additionalData.get(SgQrConsts.AddnData.ID_MOBILE_NUMBER)) != null) {
            return tempObject.data;
        } else {
            return null;
        }
    }

    String getAdtLoyaltyNumber() {
        SgQrObject tempObject;
        if ((tempObject = additionalData.get(SgQrConsts.AddnData.ID_LOYALTY_NUMBER)) != null) {
            return tempObject.data;
        } else {
            return null;
        }
    }

    String getAdtStoreLabel() {
        SgQrObject tempObject;
        if ((tempObject = additionalData.get(SgQrConsts.AddnData.ID_STORE_LABEL)) != null) {
            return tempObject.data;
        } else {
            return null;
        }
    }

    String getAdtReferenceLabel() {
        SgQrObject tempObject;
        if ((tempObject = additionalData.get(SgQrConsts.AddnData.ID_REFERENCE_LABEL)) != null) {
            return tempObject.data;
        } else {
            return null;
        }
    }

    String getAdtCustomerLabel() {
        SgQrObject tempObject;
        if ((tempObject = additionalData.get(SgQrConsts.AddnData.ID_CUSTOMER_LABEL)) != null) {
            return tempObject.data;
        } else {
            return null;
        }
    }

    String getAdtTerminalLabel() {
        SgQrObject tempObject;
        if ((tempObject = additionalData.get(SgQrConsts.AddnData.ID_TERMINAL_LABEL)) != null) {
            return tempObject.data;
        } else {
            return null;
        }
    }

    String getPurposeOfTxn() {
        SgQrObject tempObject;
        if ((tempObject = additionalData.get(SgQrConsts.AddnData.ID_PURPOSE_OF_TXN)) != null) {
            return tempObject.data;
        } else {
            return null;
        }
    }

    String getAdtAddnCustDataReq() {
        SgQrObject tempObject;
        if ((tempObject = additionalData.get(SgQrConsts.AddnData.ID_ADDN_CUST_DATA_REQ)) != null) {
            return tempObject.data;
        } else {
            return null;
        }
    }

    String getAdtPaymentSpecificTemplates() {
        SgQrObject tempObject;
        if ((tempObject = additionalData.get(SgQrConsts.AddnData.ID_PAYMENT_SYSTEMS_SPECIFIC_TEMPLATES)) != null) {
            return tempObject.data;
        } else {
            return null;
        }
    }
    //endregion


    /**
     * Builder class for constructing instances of SgQr<br>
     * Crc is automatically calculated during build unless explicitly provided
     *
     */
    public static class Builder {
        //region <Constants for Default Values>...
        public static final String PAYLOAD_FORMAT_INDICATOR_DEFAULT = "01";
        public static final String POINT_OF_INITIATION_STATIC       = "11";
        public static final String POINT_OF_INITIATION_DYNAMIC      = "12";
        public static final String MERCHANT_CAT_CODE_DEFAULT        = "0000";
        public static final String TXN_CURRENCY_DEFAULT             = "702";
        public static final String MERCHANT_COUNTRY_CODE_DEFAULT    = "SG";
        public static final String MERCHANT_CITY_DEFAULT            = "SINGAPORE";
        //endregion

        private String payloadFormatIndicator;                                     // len: 2
        private String pointOfInitiationMethod;                                    // len: 2
        private HashMap<Integer, SgQrObject> merchantAcctInfoEmv = new HashMap<>();// len: <=99
        private HashMap<Integer, SgQrObject> merchantAcctInfoSg  = new HashMap<>();// len: <=99
        private String sgQrIdentityInformation;                                    // len: <=99       //TODO TBD BY MAS
        private String merchantCategoryCode;                                       // len: 04
        private String transactionCurrency;                                        // len: 03
        private String transactionAmount;                                          // len: <=13
        private String tipOrConvenienceIndicator;                                  // len: 2
        private String valueOfConvenienceFeeFixed;                                 // len: <=13
        private String valueOfConvenienceFeePercentage;                            // len: <=5
        private String merchantCountryCode;                                        // len: 2
        private String merchantName;                                               // len: <=25
        private String merchantCity;                                               // len: <=15
        private String postalCode;                                                 // len: 6 - 10
        private HashMap<Integer, SgQrObject> additionalData                      = new HashMap<>();   // len: <=99
        private HashMap<Integer, SgQrObject> merchantInformationLanguageTemplate = new HashMap<>();   // len: <=99
        private HashMap<Integer, SgQrObject> rfuForEmv                           = new HashMap<>();   // len: <=99
        private HashMap<Integer, SgQrObject> unreservedTemplates                 = new HashMap<>();   // len: <=99
        private String crc;

        public Builder payloadFormatIndicator(String input) {
            this.payloadFormatIndicator = input;
            return this;
        }

        public Builder pointOfInitiationMethod(String input) {
            this.pointOfInitiationMethod = input;
            return this;
        }

        public Builder sgQrIdentityInformation(String input) {
            this.sgQrIdentityInformation = input;
            return this;
        }

        public Builder merchantCategoryCode(String input) {
            this.merchantCategoryCode = input;
            return this;
        }

        public Builder transactionCurrency(String input) {
            this.transactionCurrency = input;
            return this;
        }

        public Builder transactionAmount(String input) {
            this.transactionAmount = input;
            return this;
        }

        public Builder tipOrConvenienceIndicator(String input) {
            this.tipOrConvenienceIndicator = input;
            return this;
        }

        public Builder valueOfConvenienceFeeFixed(String input) {
            this.valueOfConvenienceFeeFixed = input;
            return this;
        }

        public Builder valueOfConvenienceFeePercentage(String input) {
            this.valueOfConvenienceFeePercentage = input;
            return this;
        }

        public Builder merchantCountryCode(String input) {
            this.merchantCountryCode = input;
            return this;
        }

        public Builder merchantName(String input) {
            this.merchantName = input;
            return this;
        }

        public Builder merchantCity(String input) {
            this.merchantCity = input;
            return this;
        }

        public Builder postalCode(String input) {
            this.postalCode = input;
            return this;
        }


        /**
         * Add an entry to the MerchantAccountInfo(EMV) Map
         * @param id ID of the SgQr Data Chunk
         * @param input SgQrObject representation of the Data Chunk
         * @return Builder.this
         */
        public Builder addAcctInfoEmv(Integer id, SgQrObject input) {
            this.merchantAcctInfoEmv.put(id, input);
            return this;
        }

        /**
         * Add an entry to the MerchantAccountInfo(SG) Map
         * @param id ID of the SgQr Data Chunk
         * @param input SgQrObject representation of the Data Chunk
         * @return Builder.this
         */
        public Builder addAcctInfoSg(Integer id, SgQrObject input) {
            this.merchantAcctInfoSg.put(id, input);
            return this;
        }


        /**
         * Add an entry to the additionalData Map
         * @param id ID of the AdditionalData Entry SgQr Data Chunk
         * @param input SgQrObject representation of the Data Chunk
         * @return Builder.this
         */
        public Builder addAdditionalData(Integer id, SgQrObject input) {
            this.additionalData.put(id, input);
            return this;
        }


        /**
         * Set the additionalData Map to the input
         * @param input the Map to set additionalData to
         * @return Builder.this
         */
        public Builder setAdditionalData(HashMap<Integer, SgQrObject> input){
            this.additionalData = input;
            return this;
        }

        /**
         * Set the merchantLanguageInfo Map to the input
         * @param input the Map to set merchantLanguageInfo to
         * @return Builder.this
         */
        public Builder setMerchantInformationLanguageTemplate(HashMap<Integer, SgQrObject> input){
            this.merchantInformationLanguageTemplate= input;
            return this;
        }

        /**
         * Add an entry to the merchantLanguageInfo Map
         * @param id ID of the merchantLanguageInfo Entry SgQr Data Chunk
         * @param input SgQrObject representation of the Data Chunk
         * @return Builder.this
         */
        public Builder addMerchantInformationLanguageTemplate(Integer id, SgQrObject input) {
            this.merchantInformationLanguageTemplate.put(id, input);
            return this;
        }

        public Builder addRfuForEmv(Integer id, SgQrObject input) {
            this.rfuForEmv.put(id, input);
            return this;
        }

        public Builder addUnreservedTemplates(Integer id, SgQrObject input) {
            this.unreservedTemplates.put(id, input);
            return this;
        }

        public Builder crc(String input){
            this.crc = input;
            return this;
        }


        /**
         * Builds the SgQrModel based on inputs provided<br>
         *     Automatically calculates CRC unless explicitly provided
         * @return Generated SgQrModel
         */
        public SgQrModel build() {
            SgQrModel sgQr = new SgQrModel();


            if (!isNullOrEmpty(this.payloadFormatIndicator)) {
                sgQr.payloadFormatIndicator = this.payloadFormatIndicator;
            } else {
                sgQr.payloadFormatIndicator = PAYLOAD_FORMAT_INDICATOR_DEFAULT;
            }

            if (!isNullOrEmpty(this.pointOfInitiationMethod)) {
                sgQr.pointOfInitiationMethod = this.pointOfInitiationMethod;
            }

            if (!isNullOrEmpty(this.sgQrIdentityInformation)) {
                sgQr.sgQrIdentityInformation = this.sgQrIdentityInformation;
            } else {
                sgQrIdentityInformation = "";
            }

            if (!isNullOrEmpty(this.merchantCategoryCode)) {
                sgQr.merchantCategoryCode = this.merchantCategoryCode;
            } else {
                sgQr.merchantCategoryCode = MERCHANT_CAT_CODE_DEFAULT;
            }

            if (!isNullOrEmpty(this.transactionCurrency)) {
                sgQr.transactionCurrency = this.transactionCurrency;
            } else {
                sgQr.transactionCurrency = TXN_CURRENCY_DEFAULT;
            }

            sgQr.merchantAcctInfoEmv = this.merchantAcctInfoEmv;
            sgQr.merchantAcctInfoSg = this.merchantAcctInfoSg;

            if (!isNullOrEmpty(this.transactionAmount)) {
                sgQr.transactionAmount = this.transactionAmount;
            }

            if (!isNullOrEmpty(this.tipOrConvenienceIndicator)) {
                sgQr.tipOrConvenienceIndicator = this.tipOrConvenienceIndicator;
                if (!isNullOrEmpty(this.valueOfConvenienceFeeFixed)) {
                    sgQr.valueOfConvenienceFeeFixed = this.valueOfConvenienceFeeFixed;
                } else {
                    sgQr.valueOfConvenienceFeeFixed = "";
                }
                if (!isNullOrEmpty(this.valueOfConvenienceFeePercentage)) {
                    sgQr.valueOfConvenienceFeePercentage = this.valueOfConvenienceFeePercentage;
                } else {
                    sgQr.valueOfConvenienceFeePercentage = "";
                }
            }

            if (!isNullOrEmpty(this.merchantCountryCode)) {
                sgQr.merchantCountryCode = this.merchantCountryCode;
            } else {
                sgQr.merchantCountryCode = MERCHANT_COUNTRY_CODE_DEFAULT;
            }

            if (!isNullOrEmpty(this.merchantCity)) {
                sgQr.merchantCity = this.merchantCity;
            } else {
                sgQr.merchantCity = MERCHANT_CITY_DEFAULT;
            }

            if (!isNullOrEmpty(this.merchantName)) {
                sgQr.merchantName = this.merchantName;
            } else {
                sgQr.merchantName = "";
            }

            if (!isNullOrEmpty(this.postalCode)) {
                sgQr.postalCode = this.postalCode;
            }

            sgQr.additionalData = this.additionalData;
            sgQr.merchantInformationLanguageTemplate = this.merchantInformationLanguageTemplate;

            sgQr.rfuForEmv = this.rfuForEmv;
            sgQr.unreservedTemplates = this.rfuForEmv;

            String data = sgQr.toString() + "6304";
            if(isNullOrEmpty(crc)){
                String calculatedCrc = Integer.toHexString(CrcUtil.crc16(data));
                if(calculatedCrc.length() < 4){
                    int lengthDiff = 4 - calculatedCrc.length();
                    calculatedCrc = "0000".substring(0, lengthDiff) + calculatedCrc;
//                    calculatedCrc = "0" + calculatedCrc;
                }
                sgQr.crc = calculatedCrc;
            } else {
                sgQr.crc = crc;
            }


            return sgQr;
        }

        private boolean isNullOrEmpty(String s) {
            return s == null || s.isEmpty();
        }
    }
}
