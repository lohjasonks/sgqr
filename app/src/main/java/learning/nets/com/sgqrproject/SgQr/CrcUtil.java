package learning.nets.com.sgqrproject.SgQr;

/**
 * CRC utility class<br>
 * Should be non-public?
 */

class CrcUtil {

    static int crc16(String data, int polynomial, int initialValue) {
        int crc = initialValue;

        for (byte b : data.getBytes()) {
            for (int i = 0; i < 8; i++) {
                boolean bit = ((b >> (7 - i) & 1) == 1);
                boolean c15 = ((crc >> 15 & 1) == 1);
                crc <<= 1;
                if (c15 ^ bit) {
                    crc ^= polynomial;
                }
            }
        }

        crc &= 0xffff;
        return crc;
    }

    static int crc16(String data) {
        int crc        = 0xFFFF;
        int polynomial = 0x1021;

        for (byte b : data.getBytes()) {
            for (int i = 0; i < 8; i++) {
                boolean bit = ((b >> (7 - i) & 1) == 1);
                boolean c15 = ((crc >> 15 & 1) == 1);
                crc <<= 1;
                if (c15 ^ bit) {
                    crc ^= polynomial;
                }
            }
        }

        crc &= 0xffff;
        return crc;
    }

}
