package learning.nets.com.sgqrproject.SgQr;

import android.support.annotation.NonNull;

/**
 * SgQrObject.java
 * Model class for SG QR Entries
 */

class SgQrObject implements Comparable<SgQrObject> {

  String id;
  String length;
  String data;

  SgQrObject(String id, String length, String data) {
    this.id = id;
    this.length = length;
    this.data = data;
  }

  SgQrObject(String id, int length, String data) {
    this.id = id;
    this.length = QrUtils.intToString2sf(length);
    this.data = data;
  }

  SgQrObject(int id, String length, String data) {
    this.id = QrUtils.intToString2sf(id);
    this.length = length;
    this.data = data;
  }

  SgQrObject(int id, int length, String data) {
    this.id = QrUtils.intToString2sf(id);
    this.length = QrUtils.intToString2sf(length);
    this.data = data;
  }

  boolean lengthMatchesData() {
    return Integer.valueOf(length) == data.length();
  }


  @Override
  public String toString() {
    return "SgQrObject{" +
          "id='" + id + '\'' +
          ", length='" + length + '\'' +
          ", data='" + data + '\'' +
          '}';
  }


  @Override
  public int compareTo(@NonNull SgQrObject sgQrObject) {
    return Integer.valueOf(this.id) - Integer.valueOf(sgQrObject.id);
  }
}
