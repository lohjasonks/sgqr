package learning.nets.com.sgqrproject.SgQr;

/**
 * SgQrConsts.java
 * Class to store constants related to SG QR<br>
 *
 * Sub-classes:<br>
 * AddnData<br>
 * LanguageTemplateData<br>
 * MerchantInfo<br>
 * MerchantInfo.Nets
 */

class SgQrConsts {

    static final int MAX_ID_VAL  = 99;
    static final int MAX_LEN_VAL = 99;

    //Field IDs
    static final int ID_PAYLOAD_FORMAT_INDICATOR            = 0;
    static final int ID_POINT_OF_INITIATION_METHOD          = 1;
    static final int ID_MERCHANT_ACCOUNT_INFORMATION_EMV    = 2;
    static final int ID_MERCHANT_ACCOUNT_INFORMATION_SG     = 26;
    static final int ID_SGQR_ID_INFORMATION                 = 51;
    static final int ID_MERCHANT_CATEGORY_CODE              = 52;
    static final int ID_TRANSACTION_CURRENCY                = 53;
    static final int ID_TRANSACTION_AMOUNT                  = 54;
    static final int ID_TIP_OR_CONVENIENCE_INDICATOR        = 55;
    static final int ID_VALUE_OF_CONVENIENCE_FEE_FIXED      = 56;
    static final int ID_VALUE_OF_CONVENIENCE_FEE_PERCENTAGE = 57;
    static final int ID_COUNTRY_CODE                        = 58;
    static final int ID_MERCHANT_NAME                       = 59;
    static final int ID_MERCHANT_CITY                       = 60;
    static final int ID_POSTAL_CODE                         = 61;
    static final int ID_ADDITIONAL_TEMPLATE_DATA            = 62;
    static final int ID_MERCHANT_INFO_LANGUAGE_TEMPLATE     = 64;
    static final int ID_RFU_FOR_EMV                         = 65;
    static final int ID_UNRESERVED_TEMPLATES                = 80;
    static final int ID_CRC                                 = 63;

    //Field Lengths
    static final IntRange LEN_PAYLOAD_FORMAT_INDICATOR            = new IntRange(2, 2);
    static final IntRange LEN_POINT_OF_INITIATION_METHOD          = new IntRange(2, 2);
    static final IntRange LEN_MERCHANT_ACCOUNT_INFORMATION_EMV    = new IntRange(5, 99);
    static final IntRange LEN_MERCHANT_ACCOUNT_INFORMATION_SG     = new IntRange(5, 99);
    static final IntRange LEN_SGQR_ID_INFORMATION                 = new IntRange(1, 99);
    static final IntRange LEN_MERCHANT_CATEGORY_CODE              = new IntRange(4, 4);
    static final IntRange LEN_TRANSACTION_CURRENCY                = new IntRange(3, 3);
    static final IntRange LEN_TRANSACTION_AMOUNT                  = new IntRange(1, 13);
    static final IntRange LEN_TIP_OR_CONVENIENCE_INDICATOR        = new IntRange(2, 2);
    static final IntRange LEN_VALUE_OF_CONVENIENCE_FEE_FIXED      = new IntRange(1, 13);
    static final IntRange LEN_VALUE_OF_CONVENIENCE_FEE_PERCENTAGE = new IntRange(1, 5);
    static final IntRange LEN_COUNTRY_CODE                        = new IntRange(2, 2);
    static final IntRange LEN_MERCHANT_NAME                       = new IntRange(1, 25);
    static final IntRange LEN_MERCHANT_CITY                       = new IntRange(1, 15);
    static final IntRange LEN_POSTAL_CODE                         = new IntRange(6, 10);
    static final IntRange LEN_ADDITIONAL_TEMPLATE_DATA            = new IntRange(1, 99);
    static final IntRange LEN_MERCHANT_INFO_LANGUAGE_TEMPLATE     = new IntRange(1, 99);
    static final IntRange LEN_RFU_FOR_EMV                         = new IntRange(1, 99);
    static final IntRange LEN_UNRESERVED_TEMPLATES                = new IntRange(1, 99);
    static final IntRange LEN_CRC                                 = new IntRange(4, 4);


    static class AddnData {
        static final int ID_BILL_NUMBER                        = 1;
        static final int ID_MOBILE_NUMBER                      = 2;
        static final int ID_STORE_LABEL                        = 3;
        static final int ID_LOYALTY_NUMBER                     = 4;
        static final int ID_REFERENCE_LABEL                    = 5;
        static final int ID_CUSTOMER_LABEL                     = 6;
        static final int ID_TERMINAL_LABEL                     = 7;
        static final int ID_PURPOSE_OF_TXN                     = 8;
        static final int ID_ADDN_CUST_DATA_REQ                 = 9;
        static final int ID_RFU_FOR_EMV                        = 10;
        static final int ID_PAYMENT_SYSTEMS_SPECIFIC_TEMPLATES = 50;

        static final IntRange LEN_BILL_NUMBER                        = new IntRange(1, 25);
        static final IntRange LEN_MOBILE_NUMBER                      = new IntRange(1, 25);
        static final IntRange LEN_STORE_LABEL                        = new IntRange(1, 25);
        static final IntRange LEN_LOYALTY_NUMBER                     = new IntRange(1, 25);
        static final IntRange LEN_REFERENCE_LABEL                    = new IntRange(1, 25);
        static final IntRange LEN_CUSTOMER_LABEL                     = new IntRange(1, 25);
        static final IntRange LEN_TERMINAL_LABEL                     = new IntRange(1, 25);
        static final IntRange LEN_PURPOSE_OF_TXN                     = new IntRange(1, 25);
        static final IntRange LEN_ADDN_CUST_DATA_REQ                 = new IntRange(1, 3);
        static final IntRange LEN_RFU_FOR_EMV                        = new IntRange(1, 99);
        static final IntRange LEN_PAYMENT_SYSTEMS_SPECIFIC_TEMPLATES = new IntRange(1, 99);
    }

    static class LanguageTemplateData{
        static final int ID_LANGUAGE_PREFERENCE = 0;
        static final int ID_NAME_IN_ALT_LANGUAGE = 1;
        static final int ID_CITY_IN_ALT_LANGUAGE = 2;
    }

    static class MerchantInfo {
        static final int ID_VISA_1       = 2;
        static final int ID_VISA_2       = 3;
        static final int ID_MASTERCARD_1 = 4;
        static final int ID_MASTERCARD_2 = 5;
        static final int ID_EMVCO_1      = 6;
        static final int ID_EMVCO_2      = 7;
        static final int ID_EMVCO_3      = 8;
        static final int ID_DISCOVER_1   = 9;
        static final int ID_DISCOVER_2   = 10;
        static final int ID_AMEX_1       = 11;
        static final int ID_AMEX_2       = 12;
        static final int ID_JCB_1        = 13;
        static final int ID_JCB_2        = 14;
        static final int ID_UNIONPAY_1   = 15;
        static final int ID_UNIONPAY_2   = 16;
        static final int ID_EMVCO_4      = 17;
        static final int ID_EMVCO_5      = 18;
        static final int ID_EMVCO_6      = 19;
        static final int ID_EVCO_7       = 20;
        static final int ID_EMVCO_8      = 21;
        static final int ID_EMVCO_9      = 22;
        static final int ID_EMVCO_10     = 23;
        static final int ID_EMVCO_11     = 24;
        static final int ID_EMVCO_12     = 25;

        static final int ID_SINGTEL_DASH = 26;
        static final int ID_LIQUID_PAY   = 27;
        static final int ID_OCBC_P2P     = 28;
        static final int ID_EZI_WALLET   = 29;
        static final int ID_EZ_LINK      = 30;
        static final int ID_GRAB_PAY     = 31;
        static final int ID_DBS          = 32;
        static final int ID_NETS         = 33;
        static final int ID_WECHAT_PAY   = 34;
        static final int ID_UOB          = 35;
        static final int ID_PAYNOW       = 36;
        static final int ID_AIRPAY_SEA   = 37;

        static class Nets {
            static final int MERCHANT_ACCOUNT_ID = 33;
            static final int DATA_LENGTH         = 85;

            static final String UNIQUE_IDENTIFIER = "SG.COM.NETS";

            static final int ID_UNIQUE_ID        = 0;
            static final int ID_QR_METADATA      = 1;
            static final int ID_MERCHANT_ID      = 2;
            static final int ID_TERMINAL_ID      = 3;
            static final int ID_BLE_METADATA     = 4;
            static final int ID_TXN_AMT_MODIFIER = 9;
            static final int ID_SIGNATURE        = 99;

            static final IntRange LEN_UNIQUE_ID        = new IntRange(11, 11);
            static final IntRange LEN_QR_METADATA      = new IntRange(23, 23);
            static final IntRange LEN_MERCHANT_ID      = new IntRange(1, 99);
            static final IntRange LEN_TERMINAL_ID      = new IntRange(1, 99);
            static final IntRange LEN_BLE_METADATA     = new IntRange(80, 80);
            static final IntRange LEN_SIGNATURE        = new IntRange(8, 8);
            static final IntRange LEN_TXN_AMT_MODIFIER = new IntRange(1, 1);
        }
    }


}
