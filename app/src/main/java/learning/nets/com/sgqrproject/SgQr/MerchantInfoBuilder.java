package learning.nets.com.sgqrproject.SgQr;

/**
 * MerchantInfoBuilder
 * Created by jason on 29/1/18.
 *
 * Builder classes to construct SgQrObject representations of MerchantInfo(EMV/SG)<br>
 *     Currently only has NETS Merchant Info
 */

public class MerchantInfoBuilder {


    /**
     * Builder class for NETS Merchant Info<br>
     * Builds an SgQrObject representation of the NETS Merchant Info(SG)
     *
     */
    public static class Nets {
        String qrMetadataVersion;
        String qrMetadataIssuerUen;
        String qrMetadataExpiryTimestamp;
        String mid;
        String tid;
        String txnAmtModifier;
        String signature;
        String id;

        public Nets id(String id) {
            this.id = id;
            return this;
        }

        public Nets qrMetadataVersion(String input) {
            qrMetadataVersion = input;
            return this;
        }
        public Nets qrMetadataIssuerUen(String input) {
            qrMetadataIssuerUen = input;
            return this;
        }
        public Nets qrMetadataExpiryTimestamp(String input) {
            qrMetadataExpiryTimestamp = input;
            return this;
        }
        public Nets mid(String input) {
            mid = input;
            return this;
        }
        public Nets tid(String input) {
            tid = input;
            return this;
        }
        public Nets txnAmtModifier(String input) {
            txnAmtModifier = input;
            return this;
        }
        public Nets signature(String input) {
            signature = input;
            return this;
        }

        public SgQrObject build() {
            StringBuilder output = new StringBuilder();

            output.append(QrUtils.intToString2sf(SgQrConsts.MerchantInfo.Nets.ID_UNIQUE_ID))
                    .append(QrUtils.intToString2sf(SgQrConsts.MerchantInfo.Nets.UNIQUE_IDENTIFIER.length()))
                    .append(SgQrConsts.MerchantInfo.Nets.UNIQUE_IDENTIFIER);

            output.append(QrUtils.intToString2sf(SgQrConsts.MerchantInfo.Nets.ID_QR_METADATA))
                    .append(QrUtils.intToString2sf(SgQrConsts.MerchantInfo.Nets.LEN_QR_METADATA.max))
                    .append(qrMetadataVersion)
                    .append(qrMetadataIssuerUen)
                    .append(qrMetadataExpiryTimestamp);

            if(mid != null && !mid.isEmpty()){
                output.append(QrUtils.intToString2sf(SgQrConsts.MerchantInfo.Nets.ID_MERCHANT_ID))
                        .append(QrUtils.intToString2sf(mid.length()))
                        .append(mid);
            }

            if(tid != null && !tid.isEmpty()){
                output.append(QrUtils.intToString2sf(SgQrConsts.MerchantInfo.Nets.ID_TERMINAL_ID))
                        .append(QrUtils.intToString2sf(tid.length()))
                        .append(tid);
            }
            if(txnAmtModifier != null && !txnAmtModifier.isEmpty()){
                output.append(QrUtils.intToString2sf(SgQrConsts.MerchantInfo.Nets.ID_TXN_AMT_MODIFIER))
                        .append(QrUtils.intToString2sf(txnAmtModifier.length()))
                        .append(txnAmtModifier);
            }

            output.append(QrUtils.intToString2sf(SgQrConsts.MerchantInfo.Nets.ID_SIGNATURE))
                    .append(QrUtils.intToString2sf(signature.length()))
                    .append(signature);

            String finalString = output.toString();

            if(id == null){
                id = "26";
            }

            return new SgQrObject(id, finalString.length(), finalString);
        }
    }
}
