package learning.nets.com.sgqrproject.SgQr;

/**
 * ErrorCodes
 * Created by jason on 17/7/18.
 */
public class ErrorCodes {

    public static final String CAT_CHECK_SGQR              = "0000";
    public static final String DET_CRC_IDLEN_INVALID       = "0001";
    public static final String DET_CRC_VALUE_INVALID       = "0002";
    public static final String DET_CANT_PARSE_IDLEN        = "0003";
    public static final String DET_DUPLICATE_ENTRIES       = "0004";
    public static final String DET_ID_LEN_MISMATCH         = "0005";
    public static final String DET_LEN_TOO_LONG            = "0006";
    public static final String DET_REMAINING_LEN_ERR       = "0007";
    public static final String DET_MANDATORY_FIELD_MISSING = "0008";


    public static final String CAT_CHECK_MERCH_INFO_NETS    = "0001";
    public static final String DET_MERCH_INFO_NULL          = "0000";
    //    public static final String DET_MERCH_INFO_ID_NOT_NETS   = "0001";
//    public static final String DET_MERCH_INFO_LEN_INVALID   = "0002";
    public static final String DET_WRONG_UNIQUE_ID          = "0003";
    public static final String DET_INVALID_QR_METADATA      = "0004";
    public static final String DET_SIGNATURE_ERROR          = "0005";
    public static final String DET_MID_INVALID              = "0006";
    public static final String DET_TID_INVALID              = "0007";
    public static final String DET_BLE_METADATA_INVALID     = "0008";
    public static final String DET_TXN_AMT_MODIFIER_INVALID = "0009";
    public static final String DET_NETS_MI_GENERAL_ERR      = "0010";


    public static final String CAT_CONV_SGQRM_TO_NETSQRM        = "0002";
    public static final String DET_NETS_MI_MISSING              = "0000";
    public static final String DET_QR_METADATA_NULL             = "0001";
    public static final String DET_MID_MISSING                  = "0002";
    public static final String DET_MERCH_NAME_MISSING           = "0003";
    public static final String DET_SIG_MISSING                  = "0004";
    public static final String DET_TXN_REF_INVALID              = "0005";
    public static final String DET_UNACCEPTED_CURRENCY_CODE     = "0006";
    public static final String DET_AMT_INVALID_FOR_NETSQRM      = "0007";
    public static final String DET_SGQRM_TO_NETSQRM_GENERAL_ERR = "0010";


    public static final String CAT_CONV_SGQRS_TO_NETSQRM     = "0003";
    public static final String DET_INVALID_INFO              = "0001";
    public static final String DET_SGQRS_TO_NQRM_GENERAL_ERR = "0010";

    public static final String CAT_CONV_SGQRS_TO_NETSQRS = "0004";
    public static final String DET_SGQRS_TO_NQRS_ERR     = "0001";


    public static final String CAT_PARSE_SGQR_STRING     = "0005";
    public static final String DET_PARSESGQR_GENERAL_ERR = "0001";
    public static final String DET_AMT_INVALID           = "0002";


    public static final String CAT_PARSE_SGQR_MAP         = "0009";
    public static final String DET_EXTRACTION_GENERAL_ERR = "0001";

    public static final String CAT_PARSE_NETSQR           = "0010";
    public static final String DET_NQR_LEN_INVALID        = "0001";
    public static final String DET_NQR_HEADER_MISMATCH    = "0002";
    public static final String DET_NQR_INVALID_VERSION    = "0003";
    public static final String DET_NQR_INVALID_SIG        = "0004";
    public static final String DET_NQR_INVALID_MERCH_NAME = "0005";
    public static final String DET_NQR_INVALID_TXN_REF    = "0006";
    public static final String DET_NQR_FORMAT_INVALID     = "0006";


    public static final String CAT_PARSE_NETS_DYNQR      = "0020";
    public static final String DET_NDYNQR_INVALID_LEN    = "0001";
    public static final String DET_NDYNQR_INVALID_HEADER = "0002";
    public static final String DET_NDYNQR_FORMAT_INVALID = "0003";

    public static String make(String category, String detail) {
        return category + " " + detail;
    }
}
