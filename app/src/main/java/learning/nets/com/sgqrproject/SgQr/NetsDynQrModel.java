package learning.nets.com.sgqrproject.SgQr;

/**
 * NetsDynQrModel<br>
 * Model class for NETS Dynamic QR Code<br>
 * Created by jason on 21/2/18.
 */

public class NetsDynQrModel {
    public static final String HEADER = "NETSQPAY";

    //region <Length Constants...>
    private static final int LEN_HEADER         = 8;
    private static final int LEN_VERSION        = 1;
    private static final int LEN_TID            = 8;
    private static final int LEN_MID            = 15;
    private static final int LEN_STAN           = 6;
    private static final int LEN_TIME_STAMP     = 8;
    private static final int LEN_OP             = 1;
    private static final int LEN_CHANNEL        = 1;
    private static final int LEN_AMT            = 8;
    private static final int LEN_PAYMENT_ACCEPT = 4;
    private static final int LEN_CURRENCY       = 1;
    private static final int LEN_OTRS           = 8;
    private static final int LEN_MERCHANT_NAME  = 17;
    private static final int LEN_CHECKSUM       = 4;
    //endregion

    public static final int   MIN_LENGTH = 86;
    public static final int[] LENGTHS    = {LEN_HEADER,
                                            LEN_VERSION,
                                            LEN_TID,
                                            LEN_MID,
                                            LEN_STAN,
                                            LEN_TIME_STAMP,
                                            LEN_OP,
                                            LEN_CHANNEL,
                                            LEN_AMT,
                                            LEN_PAYMENT_ACCEPT,
                                            LEN_CURRENCY,
                                            LEN_OTRS,
                                            LEN_MERCHANT_NAME,
                                            LEN_CHECKSUM};

    String header;
    String version;
    String tid;
    String mid;
    String stan;
    String timeStamp;
    String op;
    String channel;
    String amt;
    String paymentAccept;
    String currency;
    String otrs;
    String merchantName;
    String checksum;

    //region <Getters...>
    public String getHeader() {
        return header;
    }

    public String getVersion() {
        return version;
    }

    public String getTid() {
        return tid;
    }

    public String getMid() {
        return mid;
    }

    public String getStan() {
        return stan;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public String getOp() {
        return op;
    }

    public String getChannel() {
        return channel;
    }

    public String getAmt() {
        return amt;
    }

    public String getPaymentAccept() {
        return paymentAccept;
    }

    public String getCurrency() {
        return currency;
    }

    public String getOtrs() {
        return otrs;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public String getChecksum() {
        return checksum;
    }
    //endregion

    /**
     * @return The corresponding NETS Dynamic QR String
     */
    public String toString() {
        String FILLER_HEX  = "################";
        String FILLER_ZERO = "000000000";

        StringBuilder sb = new StringBuilder();
        sb.append(header).append(version);
        if (tid != null && tid.length() < LEN_TID) {
            String tempTid = FILLER_HEX.substring(0, LEN_TID - tid.length()) + tid;
            sb.append(tempTid);
        } else {
            sb.append(tid);
        }
        if (mid != null && mid.length() < LEN_MID) {
            String tempMid = FILLER_HEX.substring(0, LEN_MID - mid.length()) + mid;
            sb.append(tempMid);
        } else {
            sb.append(mid);
        }
        sb.append(stan)
                .append(timeStamp)
                .append(op)
                .append(channel);
        if (amt != null && amt.length() < 8) {
            String tempAmt = FILLER_ZERO.substring(0, LEN_AMT - amt.length()) + amt;
            sb.append(tempAmt);
        } else {
            sb.append(amt);
        }
        sb.append(paymentAccept)
                .append(currency)
                .append(otrs);
        if (merchantName != null && merchantName.length() > 17) {
            String tempMerchantName = merchantName.substring(0, 17);
            sb.append(tempMerchantName);
        } else {
            sb.append(merchantName);
        }
        if(checksum != null){
            sb.append(checksum);
        }
        return sb.toString();
    }


    public String toDebugString() {
        final StringBuilder sb = new StringBuilder("NetsDynQrModel{");
        sb.append("header='").append(header).append('\n');
        sb.append(", version='").append(version).append('\n');
        sb.append(", tid='").append(tid).append('\n');
        sb.append(", mid='").append(mid).append('\n');
        sb.append(", stan='").append(stan).append('\n');
        sb.append(", timeStamp='").append(timeStamp).append('\n');
        sb.append(", op='").append(op).append('\n');
        sb.append(", channel='").append(channel).append('\n');
        sb.append(", amt='").append(amt).append('\n');
        sb.append(", paymentAccept='").append(paymentAccept).append('\n');
        sb.append(", currency='").append(currency).append('\n');
        sb.append(", otrs='").append(otrs).append('\n');
        sb.append(", merchantName='").append(merchantName).append('\n');
        sb.append(", checksum='").append(checksum).append('\n');
        sb.append('}');
        return sb.toString();
    }


    /**
     * Builder class for Nets Dynamic Qr<br>
     * The following fields are autogenerated if NOT explicitly provided:<br>
     * timestamp, checksum
     */
    public static class Builder {
        private String header  = HEADER;
        private String version = "0";
        private String tid;
        private String mid;
        private String stan;
        private String timeStamp;
        private String op;
        private String channel;
        private String amt;
        private String paymentAccept;
        private String currency = "0";
        private String otrs;
        private String merchantName;
        private String checksum = "";


        public Builder header(String header) {
            this.header = header;
            return this;
        }

        public Builder version(String version) {
            this.version = version;
            return this;
        }

        public Builder tid(String tid) {
            this.tid = tid;
            return this;
        }

        public Builder mid(String mid) {
            this.mid = mid;
            return this;
        }

        public Builder stan(String stan) {
            this.stan = stan;
            return this;
        }

        public Builder timeStamp(String timeStamp) {
            this.timeStamp = timeStamp;
            return this;
        }

        public Builder op(String op) {
            this.op = op;
            return this;
        }

        public Builder channel(String channel) {
            this.channel = channel;
            return this;
        }

        public Builder amt(String amt) {
            this.amt = amt;
            return this;
        }

        public Builder paymentAccept(String paymentAccept) {
            this.paymentAccept = paymentAccept;
            return this;
        }

        public Builder currency(String currency) {
            this.currency = currency;
            return this;
        }

        public Builder otrs(String otrs) {
            this.otrs = otrs;
            return this;
        }

        public Builder merchantName(String merchantName) {
            this.merchantName = merchantName;
            return this;
        }

        public Builder checksum(String checksum) {
            this.checksum = checksum;
            return this;
        }

        public NetsDynQrModel build() {
            NetsDynQrModel model = new NetsDynQrModel();
            model.header = this.header;
            model.version = this.version;
            model.tid = this.tid;
            model.mid = this.mid;
            model.stan = this.stan;
            if (this.timeStamp == null || this.timeStamp.isEmpty()) {
                model.timeStamp = getCurrentTimeStamp();
            } else {
                model.timeStamp = this.timeStamp;
            }
            model.op = this.op;
            model.channel = this.channel;
            model.amt = this.amt;
            model.paymentAccept = this.paymentAccept;
            model.currency = this.currency;
            model.otrs = this.otrs;
            model.merchantName = this.merchantName;
            String preCrcString = model.toString();
            System.out.println(preCrcString);
            if (this.checksum != null && !this.checksum.isEmpty()) {
                model.checksum = this.checksum;
            } else {
                String calculatedCrc = Integer.toHexString(CrcUtil.crc16(preCrcString));
                if(calculatedCrc.length() < 4){
                    int lengthDiff = 4 - calculatedCrc.length();
                    calculatedCrc = "0000".substring(0, lengthDiff) + calculatedCrc;
//                    calculatedCrc = "0" + calculatedCrc;
                }
                model.checksum = calculatedCrc;

            }
            return model;
        }

        private String getCurrentTimeStamp() {
            String zeros          = "00000000";
            long   currTime       = System.currentTimeMillis() / 1000L;
            long   julianDateTime = currTime + 8 * 60 * 60 - 9131 * 86400;
            String timeString     = Long.toHexString(julianDateTime);
            if (timeString.length() < 8) {
                timeString = zeros.substring(0, 8 - timeString.length()) + timeString;
            }
            return timeString;
        }
    }


}
