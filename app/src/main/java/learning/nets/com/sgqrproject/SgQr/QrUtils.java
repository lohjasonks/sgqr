package learning.nets.com.sgqrproject.SgQr;

import android.annotation.SuppressLint;

import java.util.ArrayList;
import java.util.Formatter;
import java.util.HashMap;
import java.util.List;
import java.util.NavigableMap;
import java.util.TreeMap;

/**
 * QrUtils.java
 * QR Utility class for checking, parsing, converting
 */

public class QrUtils {


    /**
     * Check whether the SGQR CRC is valid
     *
     * @param qrData String representation of the SGQR
     * @return True if Valid; False if Invalid
     */
    public static boolean crcIsValid(String qrData) {
        int    qrLength  = qrData.length();
        String dataToCrc = qrData.substring(0, qrLength - 4);
        String crc       = qrData.substring(qrLength - 4, qrLength);

        String crcAsHex = Integer.toHexString(CrcUtil.crc16(dataToCrc));
        if (crcAsHex.length() < 4) {
            int lengthDiff = 4 - crcAsHex.length();
            crcAsHex = "0000".substring(0, lengthDiff) + crcAsHex;
//            crcAsHex = "0" + crcAsHex;
        }
        return crc.equalsIgnoreCase(crcAsHex);
    }


    /**
     * Check if the SgQrString is Valid according to pre-defined format(length, order, etc.)
     *
     * @param qrData The SgQr String
     * @return True if valid
     * @throws QrConversionException If QR is Invalid, Exception with message stating where it failed is thrown
     */
    public static boolean isValidSgQrString(String qrData) throws QrConversionException {

        int qrLength = qrData.length();

        List<Integer>                   mandatoryFields = new ArrayList<>();
        NavigableMap<Integer, IntRange> idMap           = new TreeMap<>();
        HashMap<Integer, Boolean>       entryExists     = new HashMap<>();

        initMandatoryFields(mandatoryFields);
        setUpIdToLengthMap(idMap);

        //Check does CRC ID & Length field match
        String  crcIdAndLen         = qrData.substring(qrLength - 8, qrLength - 4);
        String  crcIdAndLenShouldBe = intToString2sf(SgQrConsts.ID_CRC) + intToString2sf(SgQrConsts.LEN_CRC.max);
        boolean isIdAndLenOk        = crcIdAndLen.equalsIgnoreCase(crcIdAndLenShouldBe);

        if (!isIdAndLenOk) {
            throw new QrConversionException(ErrorCodes.make(ErrorCodes.CAT_CHECK_SGQR,
                                                            ErrorCodes.DET_CRC_IDLEN_INVALID),
                                            "Check QR: CRC ID & Len field are invalid");
        }

        //Check Crc validity
        if (!crcIsValid(qrData)) {
            throw new QrConversionException(ErrorCodes.make(ErrorCodes.CAT_CHECK_SGQR,
                                                            ErrorCodes.DET_CRC_VALUE_INVALID),
                                            "Check QR: CRC is invalid");
        }


        //Check for Id, Length & Data fields
        int index = 0;
        while (index < qrLength) {

            int lenFieldLen = 2;
            int idFieldLen  = 2;

            int id;
            int len;

            try {
                id = Integer.valueOf(qrData.substring(index, index + lenFieldLen));
                index += lenFieldLen;

                len = Integer.valueOf(qrData.substring(index, index + idFieldLen));
                index += idFieldLen;
            } catch (NumberFormatException nfe) {
                throw new QrConversionException(ErrorCodes.make(ErrorCodes.CAT_CHECK_SGQR,
                                                                ErrorCodes.DET_CANT_PARSE_IDLEN),
                                                "Check QR: Could not parse ID/Len at index: " + index);
            }


            //Check if ID exists more than once in the QR, then add ID to list
            boolean idAlreadyExists = entryExists.containsKey(id);

            if (idAlreadyExists) {
                throw new QrConversionException(ErrorCodes.make(ErrorCodes.CAT_CHECK_SGQR,
                                                                ErrorCodes.DET_DUPLICATE_ENTRIES),
                                                "Check QR: Duplicate entries found for id: " + id);
            } else {
                entryExists.put(id, true);
            }

            //Check if declared length matches id
            if (!isLengthValid(id, len, idMap)) {
                throw new QrConversionException(ErrorCodes.make(ErrorCodes.CAT_CHECK_SGQR,
                                                                ErrorCodes.DET_ID_LEN_MISMATCH),
                                                "Check QR: Declared length: " + len + " does not match ID: " + id);
            }

            //Check if declared length exceeds total length
            if (len > qrLength - index) {
                throw new QrConversionException(ErrorCodes.make(ErrorCodes.CAT_CHECK_SGQR,
                                                                ErrorCodes.DET_LEN_TOO_LONG),
                                                "Check QR: Declared length: " + len + " exceeds remaining length");
            }


            index += len;

            //Check remaining data length > 4
            if (qrLength != index && qrLength - index < 5) {
                throw new QrConversionException(ErrorCodes.make(ErrorCodes.CAT_CHECK_SGQR,
                                                                ErrorCodes.DET_REMAINING_LEN_ERR),
                                                "Check QR: Remaining length < 5");
            }
        }

        //Check if all mandatory fields exist
        if (!checkMandatoryFieldsExist(mandatoryFields, entryExists)) {
            throw new QrConversionException(ErrorCodes.make(ErrorCodes.CAT_CHECK_SGQR,
                                                            ErrorCodes.DET_MANDATORY_FIELD_MISSING),
                                            "Check QR: Missing a mandatory field");
        }
        return true;
    }


    /**
     * Check if Nets Merchant Info is valid
     *
     * @param merchantInfo The SgQrObject representation of NETS Merchant Info
     * @return True if valid
     * @throws QrConversionException If Invalid, throws Exception with Message showing where it failed
     */
    public static boolean isValidSgQrNetsMerchantInfo(SgQrObject merchantInfo) throws QrConversionException {

        //Check merchant info not null
        if (merchantInfo == null) {
            throw new QrConversionException(ErrorCodes.make(ErrorCodes.CAT_CHECK_MERCH_INFO_NETS,
                                                            ErrorCodes.DET_MERCH_INFO_NULL),
                                            "Check NETS Merchant Info: Is Null");
        }

//        //Check declared ID == NETS ID
//        String  idShouldBe = intToString2sf(SgQrConsts.MerchantInfo.ID_NETS);
//        boolean isValidId  = merchantInfo.id != null && merchantInfo.id.equals(idShouldBe);
//
//        if (!isValidId) {
//            throw new QrConversionException(ErrorCodes.make(ErrorCodes.CAT_CHECK_MERCH_INFO_NETS,
//                                                            ErrorCodes.DET_MERCH_INFO_ID_NOT_NETS),
//                                            "Check NETS Merchant Info: ID != Nets ID");
//        }

//        //Check Len == NETS Len
//        String  lenShouldBe = intToString2sf(SgQrConsts.MerchantInfo.Nets.DATA_LENGTH);
//        boolean isValidLen  = merchantInfo.length != null && merchantInfo.length.equals(lenShouldBe);
//
//        if (!isValidLen) {
//            throw new QrConversionException("0001 0002", "Check NETS Merchant Info: Declared Len != NETS Len");
//        }

        try {
            HashMap<Integer, SgQrObject> objects    = extractSgQrObjectMap(merchantInfo.data);
            SgQrObject                   tempObject = null;


            //Check unique identifier matches nets identifier
            if ((tempObject = objects.get(SgQrConsts.MerchantInfo.Nets.ID_UNIQUE_ID)) == null ||
                !tempObject.lengthMatchesData() ||
                !tempObject.data.equals(SgQrConsts.MerchantInfo.Nets.UNIQUE_IDENTIFIER)) {
                throw new QrConversionException(ErrorCodes.make(ErrorCodes.CAT_CHECK_MERCH_INFO_NETS,
                                                                ErrorCodes.DET_WRONG_UNIQUE_ID),
                                                "Check NETS Merchant Info: Wrong Unique identifier");
            }

            //Check Qr Metadata is len matches and is valid
            if ((tempObject = objects.get(SgQrConsts.MerchantInfo.Nets.ID_QR_METADATA)) == null ||
                !tempObject.lengthMatchesData() ||
                !SgQrConsts.MerchantInfo.Nets.LEN_QR_METADATA.isInRange(tempObject.data.length())) {
                throw new QrConversionException(ErrorCodes.make(ErrorCodes.CAT_CHECK_MERCH_INFO_NETS,
                                                                ErrorCodes.DET_INVALID_QR_METADATA),
                                                "Check NETS Merchant Info: Error in QR Metadata");
            }

            //Check if signature declared len is valid and matches
            if ((tempObject = objects.get(SgQrConsts.MerchantInfo.Nets.ID_SIGNATURE)) == null ||
                !tempObject.lengthMatchesData() ||
                !SgQrConsts.MerchantInfo.Nets.LEN_SIGNATURE.isInRange(tempObject.data.length())
                    ) {
                throw new QrConversionException(ErrorCodes.make(ErrorCodes.CAT_CHECK_MERCH_INFO_NETS,
                                                                ErrorCodes.DET_SIGNATURE_ERROR),
                                                "Check NETS Merchant Info: Error in Signature");
            }

            //Check Merchant ID len matches
            if ((tempObject = objects.get(SgQrConsts.MerchantInfo.Nets.ID_MERCHANT_ID)) != null &&
                (!tempObject.lengthMatchesData() || !SgQrConsts.MerchantInfo.Nets.LEN_MERCHANT_ID.isInRange(tempObject.data.length()))) {
                throw new QrConversionException(ErrorCodes.make(ErrorCodes.CAT_CHECK_MERCH_INFO_NETS,
                                                                ErrorCodes.DET_MID_INVALID),
                                                "Check NETS Merchant Info: Error in Merchant ID");
            }

            //Check Terminal ID len matches
            if ((tempObject = objects.get(SgQrConsts.MerchantInfo.Nets.ID_TERMINAL_ID)) != null &&
                (!tempObject.lengthMatchesData() || !SgQrConsts.MerchantInfo.Nets.LEN_TERMINAL_ID.isInRange(tempObject.data.length()))) {
                throw new QrConversionException(ErrorCodes.make(ErrorCodes.CAT_CHECK_MERCH_INFO_NETS,
                                                                ErrorCodes.DET_TID_INVALID),
                                                "Check NETS Merchant Info: Error in Terminal ID");
            }

            //Check BLE metadata len matches
            if ((tempObject = objects.get(SgQrConsts.MerchantInfo.Nets.ID_BLE_METADATA)) != null &&
                (!tempObject.lengthMatchesData() || !SgQrConsts.MerchantInfo.Nets.LEN_BLE_METADATA.isInRange(tempObject.data.length()))) {
                throw new QrConversionException(ErrorCodes.make(ErrorCodes.CAT_CHECK_MERCH_INFO_NETS,
                                                                ErrorCodes.DET_BLE_METADATA_INVALID),
                                                "Check NETS Merchant Info: Error in BLE Metadata");
            }

            //Check BLE metadata len matches
            if ((tempObject = objects.get(SgQrConsts.MerchantInfo.Nets.ID_TXN_AMT_MODIFIER)) != null &&
                (!tempObject.lengthMatchesData() || !SgQrConsts.MerchantInfo.Nets.LEN_TXN_AMT_MODIFIER.isInRange(tempObject.data.length()))) {
                throw new QrConversionException(ErrorCodes.make(ErrorCodes.CAT_CHECK_MERCH_INFO_NETS,
                                                                ErrorCodes.DET_TXN_AMT_MODIFIER_INVALID),
                                                "Check NETS Merchant Info: Error in Txn Amt Modifier");
            }

            return true;
        } catch (QrConversionException qce) {
            throw qce;
        } catch (Exception e) {
            e.printStackTrace();
            throw new QrConversionException(ErrorCodes.make(ErrorCodes.CAT_CHECK_MERCH_INFO_NETS,
                                                            ErrorCodes.DET_NETS_MI_GENERAL_ERR),
                                            "Check NETS Merchant Info: Unexpected error");
        }
    }


    @SuppressLint("UseSparseArrays")
    static HashMap<Integer, SgQrObject> extractSgQrObjectMap(String qrData) throws QrConversionException {
        int qrLength = qrData.length();
        int index    = 0;

        int lenFieldLen = 2;
        int idFieldLen  = 2;

        HashMap<Integer, SgQrObject> sgQrObjectMap = new HashMap<>();

        try {
            while (index < qrLength) {
                int id = Integer.valueOf(qrData.substring(index, index + idFieldLen));
                index += idFieldLen;

                int len = Integer.valueOf(qrData.substring(index, index + lenFieldLen));
                index += lenFieldLen;

                String value = qrData.substring(index, index + len);
                index += len;

                SgQrObject qrObj = new SgQrObject(id, len, value);
                sgQrObjectMap.put(id, qrObj);
            }
        } catch (Exception e) {
            throw new QrConversionException(ErrorCodes.make(ErrorCodes.CAT_PARSE_SGQR_MAP,
                                                            ErrorCodes.DET_EXTRACTION_GENERAL_ERR),
                                            "SGQRObject Map extraction error, Check format before extracting");
        }

        return sgQrObjectMap;
    }

    /**
     * Parse the SgQrString into an SgQrObject
     *
     * @param qrData The SgQr String to parse
     * @return SgQrObject representing the string
     * @throws QrConversionException Exception thrown if unable to convert
     */
    public static SgQrModel parseSgQrString(String qrData) throws QrConversionException {
        NavigableMap<Integer, IntRange> idMap = new TreeMap<>();
        setUpIdToLengthMap(idMap);
        HashMap<Integer, SgQrObject> sgQrObjectMap = extractSgQrObjectMap(qrData);
        SgQrObject                   tempObject;
        SgQrModel.Builder            builder       = new SgQrModel.Builder();


        try {
            //Mandatory Fields
            builder.payloadFormatIndicator(sgQrObjectMap.get(SgQrConsts.ID_PAYLOAD_FORMAT_INDICATOR).data);
            builder.sgQrIdentityInformation(sgQrObjectMap.get(SgQrConsts.ID_SGQR_ID_INFORMATION).data);
            builder.merchantCategoryCode(sgQrObjectMap.get(SgQrConsts.ID_MERCHANT_CATEGORY_CODE).data);
            builder.transactionCurrency(sgQrObjectMap.get(SgQrConsts.ID_TRANSACTION_CURRENCY).data);
            builder.merchantCountryCode(sgQrObjectMap.get(SgQrConsts.ID_COUNTRY_CODE).data);
            builder.merchantName(sgQrObjectMap.get(SgQrConsts.ID_MERCHANT_NAME).data);
            builder.merchantCity(sgQrObjectMap.get(SgQrConsts.ID_MERCHANT_CITY).data);
            builder.crc(sgQrObjectMap.get(SgQrConsts.ID_CRC).data);

            //Optional Fields
            if ((tempObject = sgQrObjectMap.get(SgQrConsts.ID_POINT_OF_INITIATION_METHOD)) != null) {
                builder.pointOfInitiationMethod(tempObject.data);
            }
            if ((tempObject = sgQrObjectMap.get(SgQrConsts.ID_TRANSACTION_AMOUNT)) != null) {
                String amount = tempObject.data;
                int    count  = amount.length() - amount.replaceAll("\\.", "").length();
                if (count > 1) {
                    throw new QrConversionException(ErrorCodes.make(ErrorCodes.CAT_PARSE_SGQR_STRING,
                                                                    ErrorCodes.DET_AMT_INVALID),
                                                    "Invalid Amount format, Too many decimals - found: " + count);
                }
                for (Character character : amount.toCharArray()) {
                    if (!Character.isDigit(character) && !character.equals('.')) {
                        throw new QrConversionException(ErrorCodes.make(ErrorCodes.CAT_PARSE_SGQR_STRING,
                                                                        ErrorCodes.DET_AMT_INVALID),
                                                        "Invalid Amount format, Contains non-numeric/decimal character: " + character);
                    }
                }

                builder.transactionAmount(tempObject.data);
            }
            if ((tempObject = sgQrObjectMap.get(SgQrConsts.ID_TIP_OR_CONVENIENCE_INDICATOR)) != null) {
                builder.tipOrConvenienceIndicator(tempObject.data);
            }
            if ((tempObject = sgQrObjectMap.get(SgQrConsts.ID_VALUE_OF_CONVENIENCE_FEE_FIXED)) != null) {
                builder.valueOfConvenienceFeeFixed(tempObject.data);
            }
            if ((tempObject = sgQrObjectMap.get(SgQrConsts.ID_VALUE_OF_CONVENIENCE_FEE_PERCENTAGE)) != null) {
                builder.valueOfConvenienceFeePercentage(tempObject.data);
            }
            if ((tempObject = sgQrObjectMap.get(SgQrConsts.ID_POSTAL_CODE)) != null) {
                builder.postalCode(tempObject.data);
            }

            //Templates
            if ((tempObject = sgQrObjectMap.get(SgQrConsts.ID_ADDITIONAL_TEMPLATE_DATA)) != null) {
                builder.setAdditionalData(extractSgQrObjectMap(tempObject.data));
            }
            if ((tempObject = sgQrObjectMap.get(SgQrConsts.ID_MERCHANT_INFO_LANGUAGE_TEMPLATE)) != null) {
                builder.setMerchantInformationLanguageTemplate(extractSgQrObjectMap(tempObject.data));
            }


            for (int i = SgQrConsts.ID_MERCHANT_ACCOUNT_INFORMATION_EMV;
                 i < SgQrConsts.ID_MERCHANT_ACCOUNT_INFORMATION_SG;
                 i++) {
                if ((tempObject = sgQrObjectMap.get(i)) != null) {
                    builder.addAcctInfoEmv(Integer.valueOf(tempObject.id), tempObject);
                }
            }

            for (int i = SgQrConsts.ID_MERCHANT_ACCOUNT_INFORMATION_SG;
                 i < SgQrConsts.ID_SGQR_ID_INFORMATION;
                 i++) {
                if ((tempObject = sgQrObjectMap.get(i)) != null) {
                    builder.addAcctInfoSg(Integer.valueOf(tempObject.id), tempObject);
                }
            }

            for (int i = SgQrConsts.ID_RFU_FOR_EMV;
                 i < SgQrConsts.ID_UNRESERVED_TEMPLATES;
                 i++) {
                if ((tempObject = sgQrObjectMap.get(i)) != null) {
                    builder.addRfuForEmv(Integer.valueOf(tempObject.id), tempObject);
                }
            }

            for (int i = SgQrConsts.ID_UNRESERVED_TEMPLATES;
                 i < SgQrConsts.MAX_ID_VAL + 1;
                 i++) {
                if ((tempObject = sgQrObjectMap.get(i)) != null) {
                    builder.addUnreservedTemplates(Integer.valueOf(tempObject.id), tempObject);
                }
            }


        } catch (QrConversionException qce) {
            throw qce;
        } catch (Exception e) {
            e.printStackTrace();
            throw new QrConversionException(ErrorCodes.make(ErrorCodes.CAT_PARSE_SGQR_STRING,
                                                            ErrorCodes.DET_PARSESGQR_GENERAL_ERR),
                                            "parseSgQrString: Error occurred");
        }
        return builder.build();
    }


    /**
     * Convert an SgQrModel to a NetsQrModel
     *
     * @param sgQrModel The SgQrModel to convert
     * @return The corresponding NetsQrModel
     * @throws QrConversionException Exception thrown if unable to convert
     */
    public static NetsQrModel convertSgQrModelToNetsQrModel(SgQrModel sgQrModel) throws QrConversionException {
        NavigableMap<Integer, IntRange> idMap = new TreeMap<>();
        setUpIdToLengthMap(idMap);
        SgQrObject          tempObject;
        NetsQrModel.Builder builder = new NetsQrModel.Builder();

        try {
            SgQrObject merchantDataObj = findNetsMerchantAcctInfo(sgQrModel.merchantAcctInfoSg);

            if (merchantDataObj == null) {
                throw new QrConversionException(ErrorCodes.make(ErrorCodes.CAT_CONV_SGQRM_TO_NETSQRM,
                                                                ErrorCodes.DET_NETS_MI_MISSING),
                                                "convertSgQrModelToNetsQrModel: Could not find NETS Merchant Info");
            }
            if (sgQrModel.getTransactionCurrency() == null
                || !sgQrModel.getTransactionCurrency().equals("702")) {
                throw new QrConversionException(ErrorCodes.make(ErrorCodes.CAT_CONV_SGQRM_TO_NETSQRM,
                                                                ErrorCodes.DET_UNACCEPTED_CURRENCY_CODE),
                                                "convertSgQrModelToNetsQrModel: Invalid Currency code: " + sgQrModel.getTransactionCurrency());
            }

            String                       merchantData = merchantDataObj.data;
            HashMap<Integer, SgQrObject> netsAcctInfo = extractSgQrObjectMap(merchantData);

            builder.header(NetsQrModel.HEADER);
            builder.version("1");

            //Mandatory QR Metadata Field
            SgQrObject qrMetadataObj = netsAcctInfo.get(SgQrConsts.MerchantInfo.Nets.ID_QR_METADATA);
            if (qrMetadataObj == null) {
                throw new QrConversionException(ErrorCodes.make(ErrorCodes.CAT_CONV_SGQRM_TO_NETSQRM,
                                                                ErrorCodes.DET_QR_METADATA_NULL),
                                                "convertSgQrModelToNetsQrModel: QR Metadata is null");
            }
            String qrMetadata = qrMetadataObj.data;
            builder.qrIssuer(qrMetadata.substring(1, 11));

            String year  = qrMetadata.substring(11, 13);
            String month = qrMetadata.substring(13, 15);
            String day   = qrMetadata.substring(15, 17);
            builder.qrExpDate(day + month + "20" + year);

            //Mandatory Records
            SgQrObject merchantIdObj = netsAcctInfo.get(SgQrConsts.MerchantInfo.Nets.ID_MERCHANT_ID);
            if (merchantIdObj == null) {
                throw new QrConversionException(ErrorCodes.make(ErrorCodes.CAT_CONV_SGQRM_TO_NETSQRM,
                                                                ErrorCodes.DET_MID_MISSING),
                                                "convertSgQrModelToNetsQrModel: Could not find Merchant ID");
            }
            builder.mid(merchantIdObj.data);

            String merchantName = sgQrModel.merchantName;
            if (merchantName == null || merchantName.isEmpty()) {
                throw new QrConversionException(ErrorCodes.make(ErrorCodes.CAT_CONV_SGQRM_TO_NETSQRM,
                                                                ErrorCodes.DET_MERCH_NAME_MISSING),
                                                "convertSgQrModelToNetsQrModel: Could not find Merchant Name");
            }
            if (merchantName.length() > NetsQrModel.LEN_REC_MERCHANT_NAME) {
                merchantName = merchantName.substring(0, NetsQrModel.LEN_REC_MERCHANT_NAME);
            }
            builder.merchantName(merchantName);

            //Optional Records
            if ((tempObject = netsAcctInfo.get(SgQrConsts.MerchantInfo.Nets.ID_TERMINAL_ID)) != null) {
                builder.tid(tempObject.data);
            }
            if (sgQrModel.transactionAmount != null && !sgQrModel.transactionAmount.isEmpty()) {
                String amount          = sgQrModel.transactionAmount;
                String convertedAmount = convertSgQrAmtToNetsQrAmt(amount);
                builder.txnAmout(convertedAmount);
            }
            if ((tempObject = netsAcctInfo.get(SgQrConsts.MerchantInfo.Nets.ID_TXN_AMT_MODIFIER)) != null) {
                builder.txnAmountQualifier(tempObject.data);
            }
            String billNumber;
            if ((billNumber = sgQrModel.getAdtBillNumber()) != null && !billNumber.isEmpty()) {
                if (billNumber.length() > NetsQrModel.LEN_REC_RETRIEVAL_REF) {
                    throw new QrConversionException(ErrorCodes.make(ErrorCodes.CAT_CONV_SGQRM_TO_NETSQRM,
                                                                    ErrorCodes.DET_TXN_REF_INVALID),
                                                    "Txn Reference Error: Number too long: " + billNumber.length() + ", Max: " + NetsQrModel.LEN_REC_RETRIEVAL_REF);
                } else if (billNumber.equals("***")) {
                    billNumber = "";
                } else if (!billNumber.matches("^[a-zA-Z0-9]*$")) {
                    throw new QrConversionException(ErrorCodes.make(ErrorCodes.CAT_CONV_SGQRM_TO_NETSQRM,
                                                                    ErrorCodes.DET_TXN_REF_INVALID),
                                                    "Txn Reference Error: Number contains invalid character(s)");
                }
                builder.txnRefNumber(billNumber);
            }

            SgQrObject signatureObj = netsAcctInfo.get(SgQrConsts.MerchantInfo.Nets.ID_SIGNATURE);
            if (signatureObj == null) {
                throw new QrConversionException(ErrorCodes.make(ErrorCodes.CAT_CONV_SGQRM_TO_NETSQRM,
                                                                ErrorCodes.DET_SIG_MISSING),
                                                "convertSgQrModelToNetsQrModel: Could not find signature");
            }
            builder.signature(signatureObj.data);
        } catch (QrConversionException qce) {
            throw qce;
        } catch (Exception e) {
            throw new QrConversionException(ErrorCodes.make(ErrorCodes.CAT_CONV_SGQRM_TO_NETSQRM,
                                                            ErrorCodes.DET_SGQRM_TO_NETSQRM_GENERAL_ERR),
                                            "convertSgQrModelToNetsQrModel: Error in conversion");
        }
        return builder.build();
    }


    private static String convertSgQrAmtToNetsQrAmt(String amount) throws QrConversionException {
        String   errorCode        = ErrorCodes.make(ErrorCodes.CAT_CONV_SGQRM_TO_NETSQRM,
                                                    ErrorCodes.DET_AMT_INVALID_FOR_NETSQRM);
        String[] amountSubStrings = amount.split("\\.");
        if (amountSubStrings.length > 2) {
            throw new QrConversionException(errorCode, "convertSgQrModelToNetsQrModel : Amount has too many decimal points");
        }
        for (Character character : amount.toCharArray()) {
            if (!Character.isDigit(character) && !character.equals('.')) {
                throw new QrConversionException(errorCode, "Invalid Amount format, Contains non-numeric/decimal character: " + character);
            }
        }
        if (amountSubStrings.length == 2) {
            if (amountSubStrings[1].length() > 2) {
                throw new QrConversionException(errorCode, "convertSgQrModelToNetsQrModel : Amount has too many chars after the decimal point: " + amountSubStrings[1].length());
            } else if (amountSubStrings[1].length() != 2) {
                int missingLength = 2 - amountSubStrings[1].length();
                amountSubStrings[1] = amountSubStrings[1] + "00".substring(0, missingLength);
            }
        }
        String convertedAmount = amountSubStrings[0];
        if (amountSubStrings.length == 1) {
            convertedAmount = convertedAmount + "00";
        } else {
            convertedAmount = convertedAmount + amountSubStrings[1];
        }
        try {
            if (Integer.valueOf(convertedAmount) > 99999999) {
                throw new QrConversionException(errorCode, "convertSgQrModelToNetsQrModel : Amount of: " + convertedAmount + " is too large");
            }
        } catch (NumberFormatException nfe) {
            throw new QrConversionException(errorCode, "convertSgQrModelToNetsQrModel : Could not convert Amount");
        }

        if (convertedAmount.length() > NetsQrModel.LEN_REC_TXN_AMT) {
            convertedAmount = convertedAmount.substring(convertedAmount.length() - NetsQrModel.LEN_REC_TXN_AMT, convertedAmount.length());
        } else if (convertedAmount.length() < NetsQrModel.LEN_REC_TXN_AMT) {
            int missingLength = NetsQrModel.LEN_REC_TXN_AMT - convertedAmount.length();
            convertedAmount = "00000000".substring(0, missingLength) + convertedAmount;
        }

        return convertedAmount;
    }

    /**
     * Convert an SGQR String to Corresponding NetsQrModel
     *
     * @param qrData The SGQR String
     * @return The Corresponding NetsQrModel
     * @throws QrConversionException Error Thrown if QR is invalid(e.g. Nets Info is blank, wrong format, etc)
     */
    public static NetsQrModel convertSgQrStringToNetsQrModel(String qrData) throws QrConversionException {
        try {
            SgQrModel  sgQrModel        = parseSgQrString(qrData);
            SgQrObject netsMerchantInfo = findNetsMerchantAcctInfo(sgQrModel.merchantAcctInfoSg);
//            SgQrObject netsMerchantInfo = sgQrModel.merchantAcctInfoSg.get(SgQrConsts.MerchantInfo.Nets.MERCHANT_ACCOUNT_ID);
            boolean    isValidInfo      = isValidSgQrNetsMerchantInfo(netsMerchantInfo);
            if (isValidInfo) {
                return convertSgQrModelToNetsQrModel(sgQrModel);
            }
            throw new QrConversionException(ErrorCodes.make(ErrorCodes.CAT_CONV_SGQRS_TO_NETSQRM,
                                                            ErrorCodes.DET_INVALID_INFO),
                                            "convertSgQrStringToNetsQrModel: Invalid Info");
        } catch (QrConversionException isqfe) {
            throw isqfe;
        } catch (Exception e) {
            throw new QrConversionException(ErrorCodes.make(ErrorCodes.CAT_CONV_SGQRS_TO_NETSQRM,
                                                            ErrorCodes.DET_SGQRS_TO_NQRM_GENERAL_ERR),
                                            "convertSgQrStringToNetsQrModel: Conversion Error");
        }
    }

    public static SgQrObject findNetsMerchantAcctInfo(HashMap<Integer, SgQrObject> sgMerchantInfoMap){
        SgQrObject merchantDataObj = null;
        String targetSubId = intToString2sf(SgQrConsts.MerchantInfo.Nets.ID_UNIQUE_ID);
        String targetSubLen = intToString2sf(SgQrConsts.MerchantInfo.Nets.LEN_UNIQUE_ID.min);
        String uniqueId = SgQrConsts.MerchantInfo.Nets.UNIQUE_IDENTIFIER;
        String targetFullUniqueId = targetSubId + targetSubLen + uniqueId;

        for(HashMap.Entry<Integer, SgQrObject> entry: sgMerchantInfoMap.entrySet()){
            SgQrObject info = entry.getValue();
            int len = Integer.valueOf(info.length);
            String data = info.data;

            if(len >= 15 && data.startsWith(targetFullUniqueId)){
                merchantDataObj = info;
                break;
            }
        }
        return merchantDataObj;
    }

    public static String convertSgQrToNetsQr(String qrData) throws QrConversionException {
        String outputQr = null;
        try {
            if (isValidSgQrString(qrData)) {
                outputQr = convertSgQrStringToNetsQrModel(qrData).toString();
                return outputQr;
            }
            throw new QrConversionException(ErrorCodes.make(ErrorCodes.CAT_CONV_SGQRS_TO_NETSQRS,
                                                            ErrorCodes.DET_SGQRS_TO_NQRS_ERR),
                                            "convertSgQrToNetsQr: Conversion error");
        } catch (QrConversionException isqfe) {
            throw isqfe;
        } catch (Exception e) {
            throw new QrConversionException(ErrorCodes.make(ErrorCodes.CAT_CONV_SGQRS_TO_NETSQRS,
                                                            ErrorCodes.DET_SGQRS_TO_NQRS_ERR),
                                            "convertSgQrToNetsQr: Conversion error");
        }
    }

    /**
     * Parse the NETS QR String into a NetsQrModel
     *
     * @param qrData The NETS QR String
     * @return NetsQrModel Representing the String
     * @throws QrConversionException If Unable to parse, throws error stating where it failed
     */
    public static NetsQrModel parseNetsQrString(String qrData) throws QrConversionException {
        NetsQrModel.Builder builder = new NetsQrModel.Builder().signature("");
        NetsQrModel         model;
        int                 index   = 0;
        if (qrData == null || qrData.length() < 65) {
            throw new QrConversionException(ErrorCodes.make(ErrorCodes.CAT_PARSE_NETSQR,
                                                            ErrorCodes.DET_NQR_LEN_INVALID),
                                            "Could not parse Nets QR: Length invalid");
        }

        try {
            String header = qrData.substring(index, index += NetsQrModel.LEN_HEADER);
            if (!NetsQrModel.HEADER.equals(header)) {
                throw new QrConversionException(ErrorCodes.make(ErrorCodes.CAT_PARSE_NETSQR,
                                                                ErrorCodes.DET_NQR_HEADER_MISMATCH),
                                                "Could not parse Nets QR: Header mismatch: Expected: [" + NetsQrModel.HEADER + "], Found[" + header + "]");
            } else {
                builder.header(header);
            }

            String version = qrData.substring(index, index += NetsQrModel.LEN_VERSION);
            if (version.equals("1") || version.equals("0")) {
                builder.version(version);
            } else {
                throw new QrConversionException(ErrorCodes.make(ErrorCodes.CAT_PARSE_NETSQR,
                                                                ErrorCodes.DET_NQR_INVALID_VERSION),
                                                "Could not parse Nets QR: Version Mismatch");
            }

            int numRecords = Integer.parseInt(qrData.substring(index, index += NetsQrModel.LEN_NR));
            builder.qrIssuer(qrData.substring(index, index += NetsQrModel.LEN_QR_ISSUER));
            builder.qrExpDate(qrData.substring(index, index += NetsQrModel.LEN_QR_EXP_DATE));
            for (int i = 0; i < numRecords; i++) {
                int    id   = Integer.parseInt(qrData.substring(index, index += 2));
                int    len  = Integer.parseInt(qrData.substring(index, index += 2));
                String data = qrData.substring(index, index += len);

                builder.addRecord(id, data);
            }

            String signature = qrData.substring(index, qrData.length());
            if (signature.length() != NetsQrModel.LEN_SIG_V0 && signature.length() != NetsQrModel.LEN_SIG_V1) {
                throw new QrConversionException(ErrorCodes.make(ErrorCodes.CAT_PARSE_NETSQR,
                                                                ErrorCodes.DET_NQR_INVALID_SIG),
                                                "Could not parse Nets QR: Signature Mismatch");
            } else {
                builder.signature(signature);
            }
            model = builder.build();
//            if(model.getMid().length() != NetsQrModel.LEN_REC_MERCHANT_ID){
//                throw new QrConversionException("0010 0001", "Could not parse Nets QR: Missing/Invalid MID: " + model.getMid());
//            }
            if (model.getMerchantName().isEmpty() || model.getMerchantName().length() > NetsQrModel.LEN_REC_MERCHANT_NAME) {
                throw new QrConversionException(ErrorCodes.make(ErrorCodes.CAT_PARSE_NETSQR,
                                                                ErrorCodes.DET_NQR_INVALID_MERCH_NAME),
                                                "Could not parse Nets QR: Merchant Name Missing/Too Long");
            }
            if (model.getTxnRefNumber() != null && (model.getTxnRefNumber().length() > NetsQrModel.LEN_REC_RETRIEVAL_REF || !model.getTxnRefNumber().matches("^[a-zA-Z0-9]*$"))) {
                throw new QrConversionException(ErrorCodes.make(ErrorCodes.CAT_PARSE_NETSQR,
                                                                ErrorCodes.DET_NQR_INVALID_TXN_REF),
                                                "Txn Reference Invalid");
            }
        } catch (QrConversionException qce) {
            throw qce;
        } catch (Exception e) {
            e.printStackTrace();
            throw new QrConversionException(ErrorCodes.make(ErrorCodes.CAT_PARSE_NETSQR,
                                                            ErrorCodes.DET_NQR_FORMAT_INVALID),
                                            "Could not parse Nets QR: Format invalid");
        }
        return model;
    }

    /**
     * Parse a Nets Dynamic QR String into the model class
     *
     * @param qrData The Nets Dynamic Qr to parse
     * @return A NetsDynQrModel representing the QR Data
     * @throws QrConversionException Thrown if unable to parse(e.g. invalid format, lengths, etc.)
     */
    public static NetsDynQrModel parseNetsDynQrString(String qrData) throws QrConversionException {
//        NetsDynQrModel model = new NetsDynQrModel();
        NetsDynQrModel.Builder builder = new NetsDynQrModel.Builder();
        int                    index   = 0;

        if (qrData.length() < NetsDynQrModel.MIN_LENGTH) {
            throw new QrConversionException(ErrorCodes.make(ErrorCodes.CAT_PARSE_NETS_DYNQR,
                                                            ErrorCodes.DET_NDYNQR_INVALID_LEN),
                                            "Length(" + qrData.length() + ") < Min Length(" + NetsDynQrModel.MIN_LENGTH + ")");
        }

        try {
            for (int i = 0; i < NetsDynQrModel.LENGTHS.length; i++) {
                String data = qrData.substring(index, index += NetsDynQrModel.LENGTHS[i]);
                switch (i) {
                    case 0:
                        if (!data.equals(NetsDynQrModel.HEADER)) {
                            throw new QrConversionException(ErrorCodes.make(ErrorCodes.CAT_PARSE_NETS_DYNQR,
                                                                            ErrorCodes.DET_NDYNQR_INVALID_HEADER),
                                                            "Header is not Nets Dynamic QR Header");
                        }
//                        model.header = data;
                        builder.header(data);
                        break;
                    case 1:
//                        model.version = data;
                        builder.version(data);
                        break;
                    case 2:
//                        model.tid = data.replace("#", "");
                        builder.tid(data.replace("#", ""));
                        break;
                    case 3:
//                        model.mid = data.replace("#", "");
                        builder.mid(data.replace("#", ""));
                        break;
                    case 4:
//                        model.stan = data;
                        builder.stan(data);
                        break;
                    case 5:
//                        model.timeStamp = data;
                        builder.timeStamp(data);
                        break;
                    case 6:
//                        model.op = data;
                        builder.op(data);
                        break;
                    case 7:
//                        model.channel = data;
                        builder.channel(data);
                        break;
                    case 8:
//                        model.amt = data;
                        builder.amt(data);
                        break;
                    case 9:
                        boolean b = hasAdditionalSofBlocks(data);
                        String chainedData;
                        StringBuilder tempData = new StringBuilder(data);
                        while (b) {
                            chainedData = qrData.substring(index, index += 1);
                            b = hasAdditionalSofBlocks(chainedData);
                            tempData.append(chainedData);
                        }
//                        model.paymentAccept = tempData.toString();
                        builder.paymentAccept(data);
                        break;
                    case 10:
//                        model.currency = data;
                        builder.currency(data);
                        break;
                    case 11:
//                        model.otrs = data;
                        builder.otrs(data);
                        break;
                    case 12:
//                        model.merchantName = data;
                        builder.merchantName(data);
                        break;
                    case 13:
//                        model.checksum = data;
                        builder.checksum(data);
                }
            }
        } catch (Exception e) {
            throw new QrConversionException(ErrorCodes.make(ErrorCodes.CAT_PARSE_NETS_DYNQR,
                                                            ErrorCodes.DET_NDYNQR_FORMAT_INVALID),
                                            "Unable to Parse Dynamic Qr: Invalid String Format");
        }
//        return model;
        return builder.build();
    }

    static boolean hasAdditionalSofBlocks(String bitmap) {
        if (bitmap == null || bitmap.isEmpty()) {
            return false;
        }
        int length = bitmap.length();
        if (length > 4) {
            return false;
        } else if (length == 4) {
            return (hexToBytes(bitmap)[0] & 0b01000000) != 0;
        } else if (length == 1) {
            return (hexToBytes(bitmap + "0")[0] & 0b10000000) != 0;
        }
        return false;
    }

    static String bytesToHex(byte[] bytes) {
        final char[] hexArray = "0123456789ABCDEF".toCharArray();
        char[]       hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    static byte[] hexToBytes(String hexString) {
        int    len  = hexString.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(hexString.charAt(i), 16) << 4)
                                  + Character.digit(hexString.charAt(i + 1), 16));
        }
        return data;
    }

    private static void initMandatoryFields(List<Integer> list) {
        list.add(SgQrConsts.ID_PAYLOAD_FORMAT_INDICATOR);
//    list.add(SgQrConsts.ID_MERCHANT_ACCOUNT_INFORMATION_EMV);
//    list.add(SgQrConsts.ID_MERCHANT_ACCOUNT_INFORMATION_SG);
        list.add(SgQrConsts.ID_SGQR_ID_INFORMATION);
        list.add(SgQrConsts.ID_MERCHANT_CATEGORY_CODE);
        list.add(SgQrConsts.ID_TRANSACTION_CURRENCY);
        list.add(SgQrConsts.ID_COUNTRY_CODE);
        list.add(SgQrConsts.ID_MERCHANT_NAME);
        list.add(SgQrConsts.ID_MERCHANT_CITY);
        list.add(SgQrConsts.ID_CRC);
    }

    private static boolean checkMandatoryFieldsExist(List<Integer> mandatoryFields, HashMap<Integer, Boolean> foundFields) {
        for (Integer field : mandatoryFields) {
            if (!foundFields.containsKey(field) || !foundFields.get(field)) {
                return false;
            }
        }
        return true;
    }

    private static void setUpIdToLengthMap(NavigableMap<Integer, IntRange> idMap) {
        idMap.put(SgQrConsts.ID_PAYLOAD_FORMAT_INDICATOR, SgQrConsts.LEN_PAYLOAD_FORMAT_INDICATOR);
        idMap.put(SgQrConsts.ID_POINT_OF_INITIATION_METHOD, SgQrConsts.LEN_POINT_OF_INITIATION_METHOD);
        idMap.put(SgQrConsts.ID_MERCHANT_ACCOUNT_INFORMATION_EMV, SgQrConsts.LEN_MERCHANT_ACCOUNT_INFORMATION_EMV);
        idMap.put(SgQrConsts.ID_MERCHANT_ACCOUNT_INFORMATION_SG, SgQrConsts.LEN_MERCHANT_ACCOUNT_INFORMATION_SG);
        idMap.put(SgQrConsts.ID_SGQR_ID_INFORMATION, SgQrConsts.LEN_SGQR_ID_INFORMATION);
        idMap.put(SgQrConsts.ID_MERCHANT_CATEGORY_CODE, SgQrConsts.LEN_MERCHANT_CATEGORY_CODE);
        idMap.put(SgQrConsts.ID_TRANSACTION_CURRENCY, SgQrConsts.LEN_TRANSACTION_CURRENCY);
        idMap.put(SgQrConsts.ID_TRANSACTION_AMOUNT, SgQrConsts.LEN_TRANSACTION_AMOUNT);
        idMap.put(SgQrConsts.ID_TIP_OR_CONVENIENCE_INDICATOR, SgQrConsts.LEN_TIP_OR_CONVENIENCE_INDICATOR);
        idMap.put(SgQrConsts.ID_VALUE_OF_CONVENIENCE_FEE_FIXED, SgQrConsts.LEN_VALUE_OF_CONVENIENCE_FEE_FIXED);
        idMap.put(SgQrConsts.ID_VALUE_OF_CONVENIENCE_FEE_PERCENTAGE, SgQrConsts.LEN_VALUE_OF_CONVENIENCE_FEE_PERCENTAGE);
        idMap.put(SgQrConsts.ID_COUNTRY_CODE, SgQrConsts.LEN_COUNTRY_CODE);
        idMap.put(SgQrConsts.ID_MERCHANT_NAME, SgQrConsts.LEN_MERCHANT_NAME);
        idMap.put(SgQrConsts.ID_MERCHANT_CITY, SgQrConsts.LEN_MERCHANT_CITY);
        idMap.put(SgQrConsts.ID_POSTAL_CODE, SgQrConsts.LEN_POSTAL_CODE);
        idMap.put(SgQrConsts.ID_ADDITIONAL_TEMPLATE_DATA, SgQrConsts.LEN_ADDITIONAL_TEMPLATE_DATA);
        idMap.put(SgQrConsts.ID_MERCHANT_INFO_LANGUAGE_TEMPLATE, SgQrConsts.LEN_MERCHANT_INFO_LANGUAGE_TEMPLATE);
        idMap.put(SgQrConsts.ID_RFU_FOR_EMV, SgQrConsts.LEN_RFU_FOR_EMV);
        idMap.put(SgQrConsts.ID_UNRESERVED_TEMPLATES, SgQrConsts.LEN_UNRESERVED_TEMPLATES);
        idMap.put(SgQrConsts.ID_CRC, SgQrConsts.LEN_CRC);
    }

    private static boolean isLengthValid(int key, int length, NavigableMap<Integer, IntRange> idMap) {
        if (key < 0 || key > SgQrConsts.MAX_ID_VAL) {
            return false;
        } else {
            IntRange entry;
            entry = idMap.floorEntry(key).getValue();
            return entry.min <= length && length <= entry.max;
        }
    }

    static String intToString2sf(int i) {
        StringBuilder sb        = new StringBuilder();
        Formatter     formatter = new Formatter(sb);
        formatter.format("%02d", i);
        return sb.toString();
    }

}
