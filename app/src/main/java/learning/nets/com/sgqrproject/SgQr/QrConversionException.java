package learning.nets.com.sgqrproject.SgQr;

/**
 * Exception thrown when Error encountered in parsing/converting QRs<br>
 * this.code contains the error description of the failure
 */

public class QrConversionException extends Exception {

    public String code;

    QrConversionException(String code, String message) {
        super(message);
        this.code = code;
    }


    @Override
    public String toString() {
        return "[" + code + "] " + super.toString();
    }
}

//Error codes - Not complete list
//0000: isValidSgQrString(String qrData)
//0000 0001, CRC ID & Len field are invalid
//0000 0002, CRC is invalid
//0000 0003, Could not parse ID/Len at index:
//0000 0004, Duplicate entries found
//0000 0005, Declared length: " + len + " does not match ID:
//0000 0006, Declared length: " + len + " exceeds remaining length
//0000 0007, Remaining length < 5
//0000 0008, Missing a mandatory field

//0001: isValidSgQrNetsMerchantInfo(SgQrObject merchantInfo)
//0001 0000, Is Null
//0001 0001, ID != Nets ID
//0001 0002, Declared Len != NETS Len
//0001 0003, Wrong Unique identifier
//0001 0004, Error in QR Metadata
//0001 0005, Error in Signature
//0001 0006, Error in Merchant ID
//0001 0007, Error in Terminal ID
//0001 0008, Error in BLE Metadata
//0001 0009, Unexpected error

//0009: extractSgQrObjectMap(String qrData)
//0009 0001, SGQRObject Map extraction error, Check format before extracting"

//0005:  extractSgQrObjectMap(String qrData)
//0005 0001, Error occurred

//0002:  convertSgQrModelToNetsQrModel(SgQrModel sgQrModel)
//0002 0000, Could not find NETS Merchant Info
//0002 0001, QR Metadata is null
//0002 0002, Could not find Merchant ID
//0002 0003, Could not find Merchant Name
//0002 0004, Could not find signature
//0002 0000, Error in conversion

//0003:  convertSgQrStringToNetsQrModel(String qrData)
//0003 0001, Invalid Info
//0003 0001, Conversion Error

//0004: convertSgQrToNetsQr(String qrData)
//0004 0001,  Conversion error
//0004 0001,  Conversion error

//0010: Parse NetsQr Error