package learning.nets.com.sgqrproject.SgQr;

import junit.framework.Assert;

import org.junit.Test;

import java.util.HashMap;

/**
 * QrGeneratorMay
 * Created by jason on 28/5/18.
 */
public class QrGeneratorMay {

    boolean GENERATE_NETS_STATIC_QR = true;
    boolean GENERATE_SGQR           = true;
    boolean PRINT_DEBUG             = false;

    String UEN                   = "0123456789";
    String SECRET_STRING         = "THISISNETSSECRETSTRINGFORTESTING";
    String MID                   = "11135036100";
    String TID                   = "35036103";
    String MID_WEBQR             = "868080000";
    String TID_WEBQR             = "00000001";
    String HEADER                = "NETSqpay";
    String EXPIRY                = "31122032";
    String EXPIRY_EXPIRED        = "31122017";
    String WEBQR_TRUE            = "1";
    String WEBQR_FALSE           = "0";
    String VERSION1              = "1";
    String VERSION0              = "0";
    String MERCHANT_NAME         = "TEST_MERCHANT_NETS";
    String MERCHANT_NAME_NEW     = "INTERNAL TESTING MSS JUN";
    String AMOUNT_NETSQR         = "00001234";
    String AMOUNT_SGQR           = "12.34";
    String AMOUNT_NEGATIVE       = "-0001234";
    String REFERENCE             = "1A2B3C4D5E6F";
    String REFERENCE_WEB_QR      = "MI12340000000001";
    String REFERENCE_EMPTY       = "";
    String QUALIFIER_FALSE       = "0";
    String QUALIFIER_TRUE        = "1";
    String TIMESTAMP             = "02042018083015";
    String MINMAX                = "0000000199999999";
    String QRFORM0               = "0";
    String QRFORM1               = "1";
    String SGQRTIMESTAMP         = "321231235959";
    String SGQRTIMESTAMP_EXPIRED = "171231235959";
    String SGQRID                = "1234567890ABCDEF";
    String POSTALCODE            = "888333";

    String MID_ORG = "11135036100";
    String TID_ORG = "35036103";


    @Test
    public void generateNetsAndSgQr() {
        String mid             = null;
        String merchantName    = null;
        String tid             = null;
        String amount          = null;
        String timeStamp       = null;
        String minMax          = null;
        String reference       = null;
        String qrForm          = null;
        String txnAmtQualifier = null;
        String webQr           = null;


        //EDIT HERE
        //COMMENT OUT IF NOT INCLUDED IN QR
        //MODIFY VALUE AS NEEDED

        mid = MID_WEBQR;
        merchantName = MERCHANT_NAME;
        tid = TID_WEBQR;
//        amount = AMOUNT_NEGATIVE;
        reference = REFERENCE_WEB_QR;
//        txnAmtQualifier = QUALIFIER_FALSE;
//        timeStamp = TIMESTAMP;
//        minMax = MINMAX;
//        qrForm = QRFORM0;
        webQr = WEBQR_TRUE;
        String  version            = VERSION0;
        boolean shouldGenerateSgQr = true;


        String sgqrTimestamp = SGQRTIMESTAMP;
        String expiry        = EXPIRY;
        String sgqrVersion   = VERSION1;
        String uen           = UEN;

        //END OF VARIABLES TO EDIT


        NetsQrModel.Builder modelBuilder = new NetsQrModel.Builder()
                .header(HEADER)
                .version(version)
                .qrIssuer(uen)
                .qrExpDate(expiry)
                .mid(mid)
                .merchantName(merchantName);

        if (tid != null) {
            modelBuilder.tid(tid);
        }
        if (amount != null) {
            modelBuilder.txnAmout(amount);
        }
        if (timeStamp != null) {
            modelBuilder.txnTimestamp(timeStamp);
        }
        if (minMax != null) {
            modelBuilder.minMaxValue(minMax);
        }
        if (reference != null) {
            modelBuilder.txnRefNumber(reference);
        }
        if (qrForm != null) {
            modelBuilder.qrForm(qrForm);
        }
        if (txnAmtQualifier != null) {
            modelBuilder.txnAmountQualifier(txnAmtQualifier);
        }
        if (webQr != null) {
            modelBuilder.webQr(webQr);
        }
        NetsQrModel model = modelBuilder.secretString(SECRET_STRING)
                .build();

        String sig = model.signature.substring(0, 8);


        String initial = model.toString();
        System.out.println(initial);
        System.out.println(model.toDebugString());
        try {
            QrUtils.parseNetsQrString(model.toString());
        } catch (Exception e) {
            System.out.println("ERROR");
            Assert.fail();
        }

        if (!shouldGenerateSgQr) {
            return;
        }


        MerchantInfoBuilder.Nets netsInfoBuilder = new MerchantInfoBuilder.Nets()
                .qrMetadataVersion(sgqrVersion)
                .qrMetadataIssuerUen(uen)
                .qrMetadataExpiryTimestamp(sgqrTimestamp)
                .mid(mid)
                .signature(sig);
        if (tid != null) {
            netsInfoBuilder.tid(tid);
        }
        if (txnAmtQualifier != null) {
            netsInfoBuilder.txnAmtModifier(txnAmtQualifier);
        }

        SgQrObject netsMerchantInfo = netsInfoBuilder.build();


        HashMap<Integer, SgQrObject> addnDataWithRef = new HashMap<>();
        if (reference != null && !reference.isEmpty()) {
            addnDataWithRef.put(SgQrConsts.AddnData.ID_BILL_NUMBER,
                                new SgQrObject(SgQrConsts.AddnData.ID_BILL_NUMBER,
                                               reference.length(),
                                               reference));
        } else if (reference != null && reference.isEmpty()) {
            addnDataWithRef.put(SgQrConsts.AddnData.ID_BILL_NUMBER,
                                new SgQrObject(SgQrConsts.AddnData.ID_BILL_NUMBER,
                                               3,
                                               "***"));
        }


        SgQrModel.Builder sgQrBuilder = new SgQrModel.Builder()
                .payloadFormatIndicator(SgQrModel.Builder.PAYLOAD_FORMAT_INDICATOR_DEFAULT)
                .pointOfInitiationMethod(SgQrModel.Builder.POINT_OF_INITIATION_STATIC)
                .addAcctInfoSg(Integer.valueOf(netsMerchantInfo.id), netsMerchantInfo)
                .sgQrIdentityInformation(SGQRID)
                .merchantName(merchantName)
                .postalCode(POSTALCODE);

        if (amount != null) {
            sgQrBuilder.transactionAmount(amount);
        }
        if (reference != null) {
            sgQrBuilder.setAdditionalData(addnDataWithRef);
        }

        SgQrModel sgQrModel = sgQrBuilder.build();
        System.out.println(sgQrModel.toString());

        try {
            String      netsQrModelString = QrUtils.convertSgQrToNetsQr(sgQrModel.toString());
            NetsQrModel netsQrModel       = QrUtils.parseNetsQrString(netsQrModelString);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void generateNewNetsQrs() {
        String uen    = UEN;
        String expiry = EXPIRY;
//        String mid          = "011110360013"; Merchant ID: Not the real MID
        String mid          = "11136001300"; //Retailer ID: the real MID
        String tid          = "36001301";
        String merchantName = MERCHANT_NAME_NEW;

        //1//With Amount, With TxnRef, Qualifier = false
        System.out.println("\n1");
        makeNetsQr(uen, expiry, mid, merchantName, tid, AMOUNT_NETSQR, null, null, REFERENCE, null, QUALIFIER_FALSE, null);
        //2//With Amount, TxnRef Len = 0, Qualifier = false
        System.out.println("\n2");
        makeNetsQr(uen, expiry, mid, merchantName, tid, AMOUNT_NETSQR, null, null, REFERENCE_EMPTY, null, QUALIFIER_FALSE, null);
        //3//With Amt, With TxnRef, Qualifier = true
        System.out.println("\n3");
        makeNetsQr(uen, expiry, mid, merchantName, tid, AMOUNT_NETSQR, null, null, REFERENCE, null, QUALIFIER_TRUE, null);
        //4//No AMt, No TxnRef
        System.out.println("\n4");
        makeNetsQr(uen, expiry, mid, merchantName, tid, null, null, null, null, null, null, null);
        //5//With Amt, With TxnRef
        System.out.println("\n5");
        makeNetsQr(uen, expiry, mid, merchantName, tid, AMOUNT_NETSQR, null, null, REFERENCE, null, null, null);
        //6//No Amt, With TxnRef
        System.out.println("\n6");
        makeNetsQr(uen, expiry, mid, merchantName, tid, null, null, null, REFERENCE, null, null, null);
        //7//Expired
        System.out.println("\n7");
        makeNetsQr(uen, EXPIRY_EXPIRED, mid, merchantName, tid, AMOUNT_NETSQR, null, null, REFERENCE, null, null, null);
        //8//With Amt, No TxnRef
        System.out.println("\n8");
        makeNetsQr(uen, expiry, mid, merchantName, tid, AMOUNT_NETSQR, null, null, null, null, null, null);
        //9//With AMt, TxnRef Len = 0, Qualifier = true
        System.out.println("\n9");
        makeNetsQr(uen, expiry, mid, merchantName, tid, AMOUNT_NETSQR, null, null, REFERENCE_EMPTY, null, QUALIFIER_TRUE, null);
        //10//No Amt, TxnRef Len = 0, No Qualifier
        System.out.println("\n10");
        makeNetsQr(uen, expiry, mid, merchantName, tid, null, null, null, REFERENCE_EMPTY, null, null, null);
        //11//With Amt, No TxnRef, Qualifier = false
        System.out.println("\n11");
        makeNetsQr(uen, expiry, mid, merchantName, tid, AMOUNT_NETSQR, null, null, null, null, QUALIFIER_FALSE, null);
        //12//With Amt, No TxnRef, Qualifier = true
        System.out.println("\n12");
        makeNetsQr(uen, expiry, mid, merchantName, tid, AMOUNT_NETSQR, null, null, null, null, QUALIFIER_TRUE, null);
        //13//With Amt, With TxnRef, WebQr = true
        System.out.println("\n13");
        makeNetsQr(uen, expiry, mid, merchantName, tid, AMOUNT_NETSQR, null, null, null, null, null, WEBQR_TRUE);

        System.out.println("\n14");
        makeNetsQr(uen, expiry, mid, merchantName, tid, null, null, null, REFERENCE, null, QUALIFIER_TRUE, null);
        System.out.println("\n15");
        makeNetsQr(uen, expiry, mid, merchantName, tid, null, null, null, REFERENCE_EMPTY, null, QUALIFIER_TRUE, null);
        System.out.println("\n16");
        makeNetsQr(uen, expiry, mid, merchantName, tid, null, null, null, null, null, QUALIFIER_TRUE, null);
        System.out.println("\n17");
        makeNetsQr(uen, expiry, mid, merchantName, tid, AMOUNT_NETSQR, null, null, REFERENCE_EMPTY, null, null, null);
    }

    @Test
    public void generateNewSgQrs() {
        String uen    = UEN;
        String expiry = EXPIRY;
//        String mid          = "011110360013"; Merchant ID: Not the real MID
        String mid          = "11136001300"; //Retailer ID: the real MID
        String tid          = "36001301";
        String merchantName = MERCHANT_NAME_NEW;

        //1//With Amount, With TxnRef, Qualifier = false
        System.out.println("\n1");
        makeSgQr(uen, expiry, mid, merchantName, tid, AMOUNT_NETSQR, AMOUNT_SGQR, null, null, REFERENCE, null, QUALIFIER_FALSE, null);
        //2//With Amount, TxnRef Len = 0, Qualifier = false
        System.out.println("\n2");
        makeSgQr(uen, expiry, mid, merchantName, tid, AMOUNT_NETSQR, AMOUNT_SGQR, null, null, REFERENCE_EMPTY, null, QUALIFIER_FALSE, null);
        //3//With Amt, With TxnRef, Qualifier = true
        System.out.println("\n3");
        makeSgQr(uen, expiry, mid, merchantName, tid, AMOUNT_NETSQR, AMOUNT_SGQR, null, null, REFERENCE, null, QUALIFIER_TRUE, null);
        //4//No AMt, No TxnRef
        System.out.println("\n4");
        makeSgQr(uen, expiry, mid, merchantName, tid, null, null, null, null, null, null, null, null);
        //5//With Amt, With TxnRef
        System.out.println("\n5");
        makeSgQr(uen, expiry, mid, merchantName, tid, AMOUNT_NETSQR, AMOUNT_SGQR, null, null, REFERENCE, null, null, null);
        //6//No Amt, With TxnRef
        System.out.println("\n6");
        makeSgQr(uen, expiry, mid, merchantName, tid, null, null, null, null, REFERENCE, null, null, null);
        //7//Expired
        System.out.println("\n7");
        makeSgQr(uen, EXPIRY_EXPIRED, mid, merchantName, tid, AMOUNT_NETSQR, AMOUNT_SGQR, null, null, REFERENCE, null, null, null);
        //8//With Amt, No TxnRef
        System.out.println("\n8");
        makeSgQr(uen, expiry, mid, merchantName, tid, AMOUNT_NETSQR, AMOUNT_SGQR, null, null, null, null, null, null);
        //9//With AMt, TxnRef Len = 0, Qualifier = true
        System.out.println("\n9");
        makeSgQr(uen, expiry, mid, merchantName, tid, AMOUNT_NETSQR, AMOUNT_SGQR, null, null, REFERENCE_EMPTY, null, QUALIFIER_TRUE, null);
        //10//No Amt, TxnRef Len = 0, No Qualifier
        System.out.println("\n10");
        makeSgQr(uen, expiry, mid, merchantName, tid, null, null, null, null, REFERENCE_EMPTY, null, null, null);
        //11//With Amt, No TxnRef, Qualifier = false
        System.out.println("\n11");
        makeSgQr(uen, expiry, mid, merchantName, tid, AMOUNT_NETSQR, AMOUNT_SGQR, null, null, null, null, QUALIFIER_FALSE, null);
        //12//With Amt, No TxnRef, Qualifier = true
        System.out.println("\n12");
        makeSgQr(uen, expiry, mid, merchantName, tid, AMOUNT_NETSQR, AMOUNT_SGQR, null, null, null, null, QUALIFIER_TRUE, null);

        System.out.println("\n\n13\n");

        System.out.println("\n14");
        makeSgQr(uen, expiry, mid, merchantName, tid, null, null, null, null, REFERENCE, null, QUALIFIER_TRUE, null);
        System.out.println("\n15");
        makeSgQr(uen, expiry, mid, merchantName, tid, null, null, null, null, REFERENCE_EMPTY, null, QUALIFIER_TRUE, null);
        System.out.println("\n16");
        makeSgQr(uen, expiry, mid, merchantName, tid, null, null, null, null, null, null, QUALIFIER_TRUE, null);
        System.out.println("\n17");
        makeSgQr(uen, expiry, mid, merchantName, tid, AMOUNT_NETSQR, AMOUNT_SGQR, null, null, REFERENCE_EMPTY, null, null, null);
    }

//    @Test
//    public void generateAmtSgqrs() {
//        String uen          = UEN;
//        String expiry       = EXPIRY;
//        String mid          = "011110360013";
//        String tid          = "36001301";
//        String merchantName = MERCHANT_NAME_NEW;
//
//        //New test for amount change
//
//        String decimalOkay      = "1234.56";
//        String decimalTooMany   = "12.345";
//        String decimalTooManyDp = "12.3.4";
//        String decimalTooLong   = "12341234123412";
//        String decimalOkay2     = "1234";
//        String free             = "0";
//        String empty            = "";
//        makeSgQr(uen, expiry, mid, merchantName, tid, decimalOkay, null, null, null, null, null, null);
//        makeSgQr(uen, expiry, mid, merchantName, tid, decimalOkay2, null, null, null, null, null, null);
//        makeSgQr(uen, expiry, mid, merchantName, tid, empty, null, null, null, null, null, null);
//        makeSgQr(uen, expiry, mid, merchantName, tid, free, null, null, null, null, null, null);
//    }


    private void makeSgQr(String uen,
                          String expiry,
                          String mid,
                          String merchantName,
                          String tid,
                          String amountNetsQr,
                          String amountSgQr,
                          String timeStamp,
                          String minMax,
                          String reference,
                          String qrForm,
                          String txnAmtQualifier,
                          String webQr) {

        String  version            = VERSION1;

        //END OF VARIABLES TO EDIT
        NetsQrModel.Builder modelBuilder = new NetsQrModel.Builder()
                .header(HEADER)
                .version(version)
                .qrIssuer(uen)
                .qrExpDate(expiry)
                .mid(mid)
                .merchantName(merchantName);

        if (tid != null) {
            modelBuilder.tid(tid);
        }
        if (amountNetsQr != null) {
            modelBuilder.txnAmout(amountNetsQr);
        }
        if (timeStamp != null) {
            modelBuilder.txnTimestamp(timeStamp);
        }
        if (minMax != null) {
            modelBuilder.minMaxValue(minMax);
        }
        if (reference != null) {
            modelBuilder.txnRefNumber(reference);
        }
        if (qrForm != null) {
            modelBuilder.qrForm(qrForm);
        }
        if (txnAmtQualifier != null) {
            modelBuilder.txnAmountQualifier(txnAmtQualifier);
        }
        if (webQr != null) {
            modelBuilder.webQr(webQr);
        }
        NetsQrModel model = modelBuilder.secretString(SECRET_STRING)
                .build();

        String sig = model.signature.substring(0, 8);


        String initial = model.toString();
//        System.out.println(initial);
//        System.out.println(model.toDebugString());
        try {
            QrUtils.parseNetsQrString(model.toString());
        } catch (Exception e) {
            System.out.println("ERROR");
            Assert.fail();
        }

        String sgQrExpiryTimestamp = SGQRTIMESTAMP;
        if (expiry.equals(EXPIRY_EXPIRED)) {
            sgQrExpiryTimestamp = SGQRTIMESTAMP_EXPIRED;
        }

        MerchantInfoBuilder.Nets netsInfoBuilder = new MerchantInfoBuilder.Nets()
                .qrMetadataVersion(VERSION1)
                .qrMetadataIssuerUen(uen)
                .qrMetadataExpiryTimestamp(sgQrExpiryTimestamp)
                .mid(mid)
                .signature(sig);
        if (tid != null) {
            netsInfoBuilder.tid(tid);
        }
        if (txnAmtQualifier != null) {
            netsInfoBuilder.txnAmtModifier(txnAmtQualifier);
        }

        SgQrObject netsMerchantInfo = netsInfoBuilder.build();


        HashMap<Integer, SgQrObject> addnDataWithRef = new HashMap<>();
        if (reference != null && !reference.isEmpty()) {
            addnDataWithRef.put(SgQrConsts.AddnData.ID_BILL_NUMBER,
                                new SgQrObject(SgQrConsts.AddnData.ID_BILL_NUMBER,
                                               reference.length(),
                                               reference));
        } else if (reference != null && reference.isEmpty()) {
            addnDataWithRef.put(SgQrConsts.AddnData.ID_BILL_NUMBER,
                                new SgQrObject(SgQrConsts.AddnData.ID_BILL_NUMBER,
                                               3,
                                               "***"));
        }


        SgQrModel.Builder sgQrBuilder = new SgQrModel.Builder()
                .payloadFormatIndicator(SgQrModel.Builder.PAYLOAD_FORMAT_INDICATOR_DEFAULT)
                .pointOfInitiationMethod(SgQrModel.Builder.POINT_OF_INITIATION_STATIC)
                .addAcctInfoSg(Integer.valueOf(netsMerchantInfo.id), netsMerchantInfo)
                .sgQrIdentityInformation(SGQRID)
                .merchantName(merchantName)
                .postalCode(POSTALCODE);

        if (amountSgQr != null) {
            sgQrBuilder.transactionAmount(amountSgQr);
        }
        if (reference != null) {
            sgQrBuilder.setAdditionalData(addnDataWithRef);
        }

        SgQrModel sgQrModel = sgQrBuilder.build();

        try {
            String      netsQrModelString = QrUtils.convertSgQrToNetsQr(sgQrModel.toString());
            NetsQrModel netsQrModel       = QrUtils.parseNetsQrString(netsQrModelString);
            System.out.println(sgQrModel);
//        System.out.println(sgQrModel.toDisplayString());
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

    private void makeNetsQr(String uen,
                            String expiry,
                            String mid,
                            String merchantName,
                            String tid,
                            String amount,
                            String timeStamp,
                            String minMax,
                            String reference,
                            String qrForm,
                            String txnAmtQualifier,
                            String webQr) {

        String  version            = VERSION0;
        boolean shouldGenerateSgQr = true;

        //END OF VARIABLES TO EDIT


        NetsQrModel.Builder modelBuilder = new NetsQrModel.Builder()
                .header(HEADER)
                .version(version)
                .qrIssuer(uen)
                .qrExpDate(expiry)
                .mid(mid)
                .merchantName(merchantName);

        if (tid != null) {
            modelBuilder.tid(tid);
        }
        if (amount != null) {
            modelBuilder.txnAmout(amount);
        }
        if (timeStamp != null) {
            modelBuilder.txnTimestamp(timeStamp);
        }
        if (minMax != null) {
            modelBuilder.minMaxValue(minMax);
        }
        if (reference != null) {
            modelBuilder.txnRefNumber(reference);
        }
        if (qrForm != null) {
            modelBuilder.qrForm(qrForm);
        }
        if (txnAmtQualifier != null) {
            modelBuilder.txnAmountQualifier(txnAmtQualifier);
        }
        if (webQr != null) {
            modelBuilder.webQr(webQr);
        }
        NetsQrModel model = modelBuilder.secretString(SECRET_STRING)
                .build();

        String sig = model.signature.substring(0, 8);


        String initial = model.toString();
        System.out.println(initial);
        System.out.println(model.toDebugString());
        try {
            QrUtils.parseNetsQrString(model.toString());
        } catch (Exception e) {
            System.out.println("ERROR");
            Assert.fail();
        }

        if (!shouldGenerateSgQr) {
            return;
        }


        MerchantInfoBuilder.Nets netsInfoBuilder = new MerchantInfoBuilder.Nets()
                .qrMetadataVersion(VERSION1)
                .qrMetadataIssuerUen(uen)
                .qrMetadataExpiryTimestamp(SGQRTIMESTAMP)
                .mid(mid)
                .signature(sig);
        if (tid != null) {
            netsInfoBuilder.tid(tid);
        }
        if (txnAmtQualifier != null) {
            netsInfoBuilder.txnAmtModifier(txnAmtQualifier);
        }

        SgQrObject netsMerchantInfo = netsInfoBuilder.build();


        HashMap<Integer, SgQrObject> addnDataWithRef = new HashMap<>();
        if (reference != null && !reference.isEmpty()) {
            addnDataWithRef.put(SgQrConsts.AddnData.ID_BILL_NUMBER,
                                new SgQrObject(SgQrConsts.AddnData.ID_BILL_NUMBER,
                                               reference.length(),
                                               reference));
        } else if (reference != null && reference.isEmpty()) {
            addnDataWithRef.put(SgQrConsts.AddnData.ID_BILL_NUMBER,
                                new SgQrObject(SgQrConsts.AddnData.ID_BILL_NUMBER,
                                               3,
                                               "***"));
        }


        SgQrModel.Builder sgQrBuilder = new SgQrModel.Builder()
                .payloadFormatIndicator(SgQrModel.Builder.PAYLOAD_FORMAT_INDICATOR_DEFAULT)
                .pointOfInitiationMethod(SgQrModel.Builder.POINT_OF_INITIATION_STATIC)
                .addAcctInfoSg(Integer.valueOf(netsMerchantInfo.id), netsMerchantInfo)
                .sgQrIdentityInformation(SGQRID)
                .merchantName(merchantName)
                .postalCode(POSTALCODE);

        if (amount != null) {
            sgQrBuilder.transactionAmount(amount);
        }
        if (reference != null) {
            sgQrBuilder.setAdditionalData(addnDataWithRef);
        }

        SgQrModel sgQrModel = sgQrBuilder.build();

        try {
            String      netsQrModelString = QrUtils.convertSgQrToNetsQr(sgQrModel.toString());
            NetsQrModel netsQrModel       = QrUtils.parseNetsQrString(netsQrModelString);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

    @Test
    public void testInvalidQr() {
        String invalidQr = "NETSqpay005T08GB0018F311220880103HDB0011111370656000208370656010308-000002000716GVT123456789012347E8C46CA9BE10BBEE4D4EE1ECADE41BE0671B3A894AB43E6A7913BCCCD00105";
        try {
            NetsQrModel model = QrUtils.parseNetsQrString(invalidQr);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
