package learning.nets.com.sgqrproject.SgQr;

import junit.framework.Assert;

import org.junit.Test;

import java.security.SecureRandom;
import java.util.HashMap;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.fail;
import static learning.nets.com.sgqrproject.SgQr.QrUtils.findNetsMerchantAcctInfo;
import static learning.nets.com.sgqrproject.SgQr.QrUtils.hasAdditionalSofBlocks;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by jason on 17/11/2017.
 */
public class QrUtilsTest {


    private String dataLessCrcValue = "00020101021102164761360000000*171110123456789012153123456789012341531250003440001034450003445311000126330015SG.COM.DASH.WWW0110000005550127820014A0000007620001011580000000000000102030010325WWW.LIQUIDPAY.COM/PAT000104050010128660011SG.COM.OCBC0147OCBCP2P629A358D-ECE7-4554-AD56-EBD12D84CA7E4F7329410006SG.EZI010812345678020812345678030312330850013SG.COM.EZLINK01201234567890123456-1230204SGQR0324A123456,B123456,C12345670404A23X31260008COM.GRAB0110A93FO3230Q32390007COM.DBS011012345678900210123456789033850011SG.COM.NETS012302014018328311288235900021500011187032400003088858720199084E5DC3D834430017COM.QQ.WEIXIN.PAY011012345678900204123435660010SG.COM.UOB014845D233507F5E8C306E3871A4E9FACA601A80C114B5645E5D36470009SG.PAYNOW010100208912345670301004082020123137270009SG.AIRPAY0110A11BC0000X51390007SG.SGQR01112017123456X0209S1234567A5204581453037025802SG5916FOOD XYZ PTE LTD6009SINGAPORE61060810066304";
    private String dataFull         = "00020101021102164761360000000*171110123456789012153123456789012341531250003440001034450003445311000126330015SG.COM.DASH.WWW0110000005550127820014A0000007620001011580000000000000102030010325WWW.LIQUIDPAY.COM/PAT000104050010128660011SG.COM.OCBC0147OCBCP2P629A358D-ECE7-4554-AD56-EBD12D84CA7E4F7329410006SG.EZI010812345678020812345678030312330850013SG.COM.EZLINK01201234567890123456-1230204SGQR0324A123456,B123456,C12345670404A23X31260008COM.GRAB0110A93FO3230Q32390007COM.DBS011012345678900210123456789033850011SG.COM.NETS012302014018328311288235900021500011187032400003088858720199084E5DC3D834430017COM.QQ.WEIXIN.PAY011012345678900204123435660010SG.COM.UOB014845D233507F5E8C306E3871A4E9FACA601A80C114B5645E5D36470009SG.PAYNOW010100208912345670301004082020123137270009SG.AIRPAY0110A11BC0000X51390007SG.SGQR01112017123456X0209S1234567A5204581453037025802SG5916FOOD XYZ PTE LTD6009SINGAPORE61060810066304F175";
    private String merchantInfoNets = "33850011SG.COM.NETS012302014018328311288235900021500011187032400003088858720199084E5DC3D8";

    @Test
    public void crcIsValid() throws Exception {
        assertTrue(QrUtils.crcIsValid(dataFull));
        assertFalse(QrUtils.crcIsValid(dataLessCrcValue));
        assertFalse(QrUtils.crcIsValid(dataFull.substring(0, dataFull.length() - 4) + "ffff"));

    }

    @Test
    public void isValidSgQrString() throws Exception {
        assertTrue(QrUtils.isValidSgQrString(dataFull));
        try {
            QrUtils.isValidSgQrString(dataLessCrcValue);
            fail();
        } catch (Exception e) {
            assertTrue(e.getClass() == QrConversionException.class);
        }
        try {
            QrUtils.isValidSgQrString("000110102030203333");
            fail();
        } catch (Exception e) {
            assertTrue(e.getClass() == QrConversionException.class);
        }

    }


    @Test
    public void extractSgQrObjectMap() throws Exception {
        HashMap<Integer, SgQrObject> map = null;
        try {
            map = QrUtils.extractSgQrObjectMap(dataFull);
        } catch (Exception e) {
            fail("Invalid Qr Exception");
        }
        assertTrue("Map is null", map != null);
        try {
            for (Integer key : map.keySet()) {
                SgQrObject tempObject = null;
                tempObject = map.get(key);
                assertTrue("Object is null", tempObject != null);

                assertTrue(tempObject.length != null && !tempObject.length.isEmpty());
                assertTrue(tempObject.id != null && !tempObject.id.isEmpty());
                assertTrue(tempObject.data != null && !tempObject.data.isEmpty());
                assertTrue(Integer.valueOf(tempObject.length) == tempObject.data.length());
//        System.out.println(object.toString());
            }

            assertTrue(map != null);
            assertTrue(map.keySet().size() == 26);
            assertTrue(map.containsKey(0));
            assertTrue(map.containsKey(1));
            assertTrue(map.containsKey(2));
            for (int i = 3; i < 11; i++) {
                assertFalse(map.containsKey(i));
            }
            assertTrue(map.containsKey(11));
            assertTrue(map.containsKey(12));
            assertTrue(map.containsKey(15));
            for (int i = 16; i < 26; i++) {
                assertFalse(map.containsKey(i));
            }
            assertTrue(map.containsKey(26));
            assertTrue(map.containsKey(27));
            assertTrue(map.containsKey(28));
            assertTrue(map.containsKey(29));
            assertTrue(map.containsKey(30));
            assertTrue(map.containsKey(31));
            assertTrue(map.containsKey(32));
            assertTrue(map.containsKey(33));
            assertTrue(map.containsKey(34));
            assertTrue(map.containsKey(35));
            assertTrue(map.containsKey(36));
            assertTrue(map.containsKey(37));
            for (int i = 38; i < 51; i++) {
                assertFalse(map.containsKey(i));
            }
            assertTrue(map.containsKey(51));
            assertTrue(map.containsKey(52));
            assertTrue(map.containsKey(53));
            assertTrue(map.containsKey(58));
            assertTrue(map.containsKey(59));
            assertTrue(map.containsKey(60));
            assertTrue(map.containsKey(61));
            assertTrue(map.containsKey(63));
            for (int i = 64; i < 99; i++) {
                assertFalse(map.containsKey(i));
            }
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void isValidSgQrNetsMerchantInfo() throws Exception {
        boolean exceptionFlag = true;
        String  id            = merchantInfoNets.substring(0, 2);
        String  len           = merchantInfoNets.substring(2, 4);
        String  data          = merchantInfoNets.substring(4);
        try {
            assertTrue(QrUtils.isValidSgQrNetsMerchantInfo(new SgQrObject(id, len, data)));

            HashMap<Integer, SgQrObject> map      = QrUtils.extractSgQrObjectMap(dataFull);
            SgQrObject                   netsInfo = findNetsMerchantAcctInfo(map);
            assertNotNull(netsInfo);
            assertTrue(QrUtils.isValidSgQrNetsMerchantInfo(netsInfo));

        } catch (Exception e) {
            fail("Exception Occurred");
        }

        try {
            String anotherValidId = "34";
            assertTrue(QrUtils.isValidSgQrNetsMerchantInfo(new SgQrObject(anotherValidId, len, data)));
        } catch (Exception e) {
            e.printStackTrace();
            assertTrue(e.getClass() == QrConversionException.class);
        }

        try {
            String invalidData = "0011SG.COM.NETS012302014018328311828823590002150001118770324000030888587201990584E5DC3D8";
            assertFalse(QrUtils.isValidSgQrNetsMerchantInfo(new SgQrObject(id, len, invalidData)));
        } catch (Exception e) {
            assertTrue(e.getClass() == QrConversionException.class);
        }

//    try{
//      String invalidLen = "86";
//      assertFalse(QrUtils.isValidSgQrNetsMerchantInfo(new SgQrObject(id, invalidLen, data)));
//    } catch (Exception e){
//      assertTrue(e.getClass() == QrConversionException.class);
//    }


    }


    @Test
    public void convertSgQrStringToModel() throws Exception {
        SgQrModel sgQrModel = null;
        try {
            sgQrModel = QrUtils.parseSgQrString(dataFull);
            assertTrue("Error occurred in population", sgQrModel != null);
            assertTrue(sgQrModel.payloadFormatIndicator != null && !sgQrModel.payloadFormatIndicator.isEmpty());
            assertTrue(sgQrModel.pointOfInitiationMethod != null && sgQrModel.pointOfInitiationMethod.equals("11"));
            assertTrue(sgQrModel.sgQrIdentityInformation != null && !sgQrModel.sgQrIdentityInformation.isEmpty());
            assertTrue(sgQrModel.merchantCategoryCode != null && sgQrModel.merchantCategoryCode.equals("5814"));
            assertTrue(sgQrModel.transactionCurrency != null && sgQrModel.transactionCurrency.equals("702"));
            assertTrue(sgQrModel.transactionAmount == null || sgQrModel.transactionAmount.isEmpty());
            assertTrue(sgQrModel.tipOrConvenienceIndicator == null || sgQrModel.tipOrConvenienceIndicator.isEmpty());
            assertTrue(sgQrModel.valueOfConvenienceFeeFixed == null || sgQrModel.valueOfConvenienceFeeFixed.isEmpty());
            assertTrue(sgQrModel.valueOfConvenienceFeePercentage == null || sgQrModel.valueOfConvenienceFeePercentage.isEmpty());
            assertTrue(sgQrModel.merchantCountryCode != null && sgQrModel.merchantCountryCode.equals("SG"));
            assertTrue(sgQrModel.merchantName != null && sgQrModel.merchantName.equals("FOOD XYZ PTE LTD"));
            assertTrue(sgQrModel.merchantCity != null && sgQrModel.merchantCity.equals("SINGAPORE"));
            assertTrue(sgQrModel.postalCode != null && sgQrModel.postalCode.equals("081006"));
            assertTrue(sgQrModel.merchantCity != null && sgQrModel.merchantCity.equals("SINGAPORE"));
            assertTrue(sgQrModel.crc != null && sgQrModel.crc.equals("F175"));
            for (Integer key : sgQrModel.merchantAcctInfoEmv.keySet()) {
                SgQrObject tempObject = null;
                tempObject = sgQrModel.merchantAcctInfoEmv.get(key);
                assertTrue("Object is null", tempObject != null);

                assertTrue(tempObject.length != null && !tempObject.length.isEmpty());
                assertTrue(tempObject.id != null && !tempObject.id.isEmpty());
                assertTrue(tempObject.data != null && !tempObject.data.isEmpty());
                assertTrue(Integer.valueOf(tempObject.length) == tempObject.data.length());
            }
            for (Integer key : sgQrModel.merchantAcctInfoSg.keySet()) {
                SgQrObject tempObject = null;
                tempObject = sgQrModel.merchantAcctInfoSg.get(key);
                assertTrue("Object is null", tempObject != null);

                assertTrue(tempObject.length != null && !tempObject.length.isEmpty());
                assertTrue(tempObject.id != null && !tempObject.id.isEmpty());
                assertTrue(tempObject.data != null && !tempObject.data.isEmpty());
                assertTrue(Integer.valueOf(tempObject.length) == tempObject.data.length());
            }
            for (Integer key : sgQrModel.additionalData.keySet()) {
                SgQrObject tempObject = null;
                tempObject = sgQrModel.additionalData.get(key);
                assertTrue("Object is null", tempObject != null);

                assertTrue(tempObject.length != null && !tempObject.length.isEmpty());
                assertTrue(tempObject.id != null && !tempObject.id.isEmpty());
                assertTrue(tempObject.data != null && !tempObject.data.isEmpty());
                assertTrue(Integer.valueOf(tempObject.length) == tempObject.data.length());
            }
            for (Integer key : sgQrModel.merchantInformationLanguageTemplate.keySet()) {
                SgQrObject tempObject = null;
                tempObject = sgQrModel.merchantInformationLanguageTemplate.get(key);
                assertTrue("Object is null", tempObject != null);

                assertTrue(tempObject.length != null && !tempObject.length.isEmpty());
                assertTrue(tempObject.id != null && !tempObject.id.isEmpty());
                assertTrue(tempObject.data != null && !tempObject.data.isEmpty());
                assertTrue(Integer.valueOf(tempObject.length) == tempObject.data.length());
            }
        } catch (Exception e) {
//      System.out.println("Exception occurred");
            fail();
        }
    }

    @Test
    public void convertSgQrModelToNetsQrModel() throws Exception {
        NetsQrModel netsQr = null;
        try {
            netsQr = QrUtils.convertSgQrModelToNetsQrModel(QrUtils.parseSgQrString(dataFull));
            assertEquals(netsQr.getHeader(), NetsQrModel.HEADER);
            assertEquals(netsQr.getVersion(), "1");
            assertEquals(netsQr.getQrIssuer(), "2014018328");
            assertEquals(netsQr.getQrExpDate(), "88122031");
            for (NetsQrModel.Record record : netsQr.records) {
                assertNotNull(record);
                assertTrue(record.length != null && !record.length.isEmpty());
                assertTrue(record.type != null && !record.type.isEmpty());
                assertTrue(record.data != null && !record.data.isEmpty());
                assertTrue(Integer.valueOf(record.length) == record.data.length());
            }
            assertEquals(netsQr.getSignature(), "4E5DC3D800000000000000000000000000000000000000000000000000000000");
        } catch (QrConversionException isqfe) {
            fail();
        }
    }

    @Test
    public void convertSgQrStringToNetsQrModel() throws Exception {
        NetsQrModel netsQr = null;
        try {
            netsQr = QrUtils.convertSgQrStringToNetsQrModel(dataFull);
            assertEquals(netsQr.header, NetsQrModel.HEADER);
            assertEquals(netsQr.version, "1");
            assertEquals(netsQr.qrIssuer, "2014018328");
            assertEquals(netsQr.qrExpDate, "88122031");
            for (NetsQrModel.Record record : netsQr.records) {
                assertNotNull(record);
                assertTrue(record.length != null && !record.length.isEmpty());
                assertTrue(record.type != null && !record.type.isEmpty());
                assertTrue(record.data != null && !record.data.isEmpty());
                assertTrue(Integer.valueOf(record.length) == record.data.length());
            }
            assertEquals(netsQr.signature, "4E5DC3D800000000000000000000000000000000000000000000000000000000");
        } catch (QrConversionException isqfe) {
            fail();
        }
    }

    @Test
    public void convertSgQrToNetsQr() throws Exception {
        String output   = QrUtils.convertSgQrToNetsQr(dataFull);
        String shouldBe = "NETSqpay10320140183288812203100150001118703240000116FOOD XYZ PTE LTD0208885872014E5DC3D8";
        assertEquals(shouldBe + "00000000000000000000000000000000000000000000000000000000", output);

        try {
            QrUtils.convertSgQrToNetsQr(dataLessCrcValue);
            fail();
        } catch (Exception e) {
            assertTrue(e.getClass() == QrConversionException.class);

        }
    }


    @Test
    public void intToString2sf() throws Exception {
        int singleDigit = 1;
        int doubleDigit = 12;

        assertEquals(QrUtils.intToString2sf(singleDigit), "01");
        assertEquals(QrUtils.intToString2sf(doubleDigit), "12");
    }

    @Test
    public void parseNetsQrTest() throws Exception {
        String UEN             = "0123456789";
        String SECRET_STRING   = "THISISNETSSECRETSTRINGFORTESTING";
        String MID             = "11135036100";
        String TID             = "35036103";
        String HEADER          = "NETSqpay";
        String EXPIRY          = "31122032";
        String WEBQR_TRUE      = "1";
        String WEBQR_FALSE     = "0";
        String VERSION1        = "1";
        String VERSION0        = "0";
        String MERCHANT_NAME   = "TEST_MERCHANT_NETS";
        String AMOUNT          = "00001234";
        String REFERENCE       = "1A2B3C4D5E6F";
        String REFERENCE_EMPTY = "";
        String QUALIFIER_FALSE = "0";
        String QUALIFIER_TRUE  = "1";
        String TIMESTAMP       = "02042018083015";
        String MINMAX          = "0000000199999999";

        NetsQrModel.Builder builder = new NetsQrModel.Builder()
                .header(HEADER)
                .version(VERSION0)
                .qrIssuer(UEN)
                .mid(MID)
                .merchantName(MERCHANT_NAME)
                .tid(TID)
                .qrExpDate(EXPIRY)
                .txnAmout(AMOUNT)
                .txnTimestamp(TIMESTAMP)
                .txnAmountQualifier(QUALIFIER_TRUE)
                .qrForm(WEBQR_TRUE)
                .secretString(SECRET_STRING);
        String netsQrWithWebQr = builder.build().toString();
        System.out.println("WebQrNetsQr:\n" + netsQrWithWebQr);

        String webqrdebugString = builder.build().toDebugString();
        System.out.println("WebQrNetsQr:\n" + webqrdebugString);

        NetsQrModel model = QrUtils.parseNetsQrString(netsQrWithWebQr);
        assertEquals(model, builder.build());

    }

    @Test
    public void testParseDyn() {
        String HEADER        = "NETSQPAY";
        String VERSION       = "0";
        String TID           = "10030028";
        String TID_LESS2     = "030028";
        String MID           = "111100300000000";
        String MID_LESS2     = "1100300000000";
        String STAN          = "000326";
        String TIMESTAMP     = "66666666";
        String OP            = "0";
        String CHANNEL       = "1";
        String AMT           = "00001234";
        String AMT_LESS2     = "001234";
        String PAYMENTACCEPT = "0001";
        String CURRENCY      = "0";
        String OTRS          = "ZXCVZXCV";
        String MERCHANT_NAME = "TEST_MERCHANTNAME";
        String CRC           = "3063";
        String CRC_FOR_LESS2 = "0DB7";

        String dynQr  = HEADER + VERSION + TID + MID + STAN + TIMESTAMP + OP + CHANNEL + AMT + PAYMENTACCEPT + CURRENCY + OTRS + MERCHANT_NAME + CRC;
        String dynQr2 = HEADER + VERSION + "##" + TID_LESS2 + "##" + MID_LESS2 + STAN + TIMESTAMP + OP + CHANNEL + AMT + PAYMENTACCEPT + CURRENCY + OTRS + MERCHANT_NAME + CRC_FOR_LESS2;

        System.out.println(dynQr);
        System.out.println(dynQr2);
        NetsDynQrModel.Builder builder = new NetsDynQrModel.Builder()
                .header(HEADER)
                .version(VERSION)
                .tid(TID)
                .mid(MID)
                .stan(STAN)
                .timeStamp(TIMESTAMP)
                .op(OP)
                .channel(CHANNEL)
                .amt(AMT)
                .paymentAccept(PAYMENTACCEPT)
                .currency(CURRENCY)
                .otrs(OTRS)
                .merchantName(MERCHANT_NAME)
                .checksum(CRC);


        NetsDynQrModel.Builder builder2 = new NetsDynQrModel.Builder()
                .header(HEADER)
                .version(VERSION)
                .tid(TID_LESS2)
                .mid(MID_LESS2)
                .stan(STAN)
                .timeStamp(TIMESTAMP)
                .op(OP)
                .channel(CHANNEL)
                .amt(AMT_LESS2)
                .paymentAccept(PAYMENTACCEPT)
                .currency(CURRENCY)
                .otrs(OTRS)
                .merchantName(MERCHANT_NAME)
                .checksum(CRC_FOR_LESS2);
//        System.out.println(builder2.build());

        try {
//            NetsDynQrModel model2 = builder.build();
//            NetsDynQrModel model = QrUtils.parseNetsDynQrString(dynQr);
            NetsDynQrModel model_org  = QrUtils.parseNetsDynQrString(dynQr);
            NetsDynQrModel model_org2 = QrUtils.parseNetsDynQrString(dynQr2);
            System.out.println(model_org.toString());
            assertEquals(model_org.header, HEADER);
            assertEquals(model_org.version, VERSION);
            assertEquals(model_org.tid, TID);
            assertEquals(model_org.mid, MID);
//            assertEquals(model_org.tid, TID_LESS2);
//            assertEquals(model_org.mid, MID_LESS2);
            assertEquals(model_org.stan, STAN);
            assertEquals(model_org.timeStamp, TIMESTAMP);
            assertEquals(model_org.op, OP);
            assertEquals(model_org.channel, CHANNEL);
            assertEquals(model_org.amt, AMT);
//            assertEquals(model_org.amt, AMT_LESS2);
            assertEquals(model_org.paymentAccept, PAYMENTACCEPT);
            assertEquals(model_org.currency, CURRENCY);
            assertEquals(model_org.otrs, OTRS);
            assertEquals(model_org.merchantName, MERCHANT_NAME);
            assertEquals(model_org.checksum, CRC);
            assertTrue(QrUtils.crcIsValid(dynQr));
            assertEquals(model_org.toString(), dynQr);

            System.out.println(model_org2.toString());
            assertEquals(model_org2.header, HEADER);
            assertEquals(model_org2.version, VERSION);
            assertEquals(model_org2.tid, TID_LESS2);
            assertEquals(model_org2.mid, MID_LESS2);
            assertEquals(model_org2.stan, STAN);
            assertEquals(model_org2.timeStamp, TIMESTAMP);
            assertEquals(model_org2.op, OP);
            assertEquals(model_org2.channel, CHANNEL);
//            assertEquals(model_org.amt, AMT_LESS2);
            assertEquals(model_org2.paymentAccept, PAYMENTACCEPT);
            assertEquals(model_org2.currency, CURRENCY);
            assertEquals(model_org2.otrs, OTRS);
            assertEquals(model_org2.merchantName, MERCHANT_NAME);
            assertEquals(model_org2.checksum, CRC_FOR_LESS2);
            assertTrue(QrUtils.crcIsValid(dynQr2));
            assertEquals(model_org2.toString(), dynQr2);

            System.out.println(model_org.toDebugString());
            System.out.println(model_org2.toDebugString());
        } catch (Exception e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void sofChainTest() {
        byte[] firstTrue        = {0b01000000, (byte) 0b11111111};
        byte[] firstFalse       = {(byte) 0b10000000, (byte) 0b11111111};
        byte[] nextTrue         = {(byte) 0b10000000};
        byte[] nextFalse        = {(byte) 0b01000000};
        byte[] bitOneButTooLong = {(byte) 0b10000000, (byte) 0xFF, (byte) 0xFF};
        byte[] bitTwoButTooLong = {(byte) 0b01000000, (byte) 0xFF, (byte) 0xFF};

        System.out.println(bytesToHex(bitOneButTooLong));
        System.out.println(bytesToHex(bitTwoButTooLong));


//        boolean ft = QrUtils.hasAdditionalSofBlocks(bytesToHex(firstTrue));
//        boolean ff = QrUtils.hasAdditionalSofBlocks(bytesToHex(firstFalse));
//        boolean nt = QrUtils.hasAdditionalSofBlocks("8");
//        boolean nf = QrUtils.hasAdditionalSofBlocks("4");
        boolean b1tl = hasAdditionalSofBlocks(bytesToHex(bitOneButTooLong));
        boolean b2tl = hasAdditionalSofBlocks(bytesToHex(bitTwoButTooLong));

        assertFalse(b1tl);
        assertFalse(b2tl);

    }

    String bytesToHex(byte[] bytes) {
        final char[] hexArray = "0123456789ABCDEF".toCharArray();
        char[]       hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    byte[] hexToBytes(String hexString) {
        int    len  = hexString.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(hexString.charAt(i), 16) << 4)
                                  + Character.digit(hexString.charAt(i + 1), 16));
        }
        return data;
    }

    @Test
    public void secureRandomTest() {
        for (int i = 0; i < 20; i++) {
            System.out.println(generateRandom());
        }
    }

    @Test
    public void testAmountWithDecimalSgqrToNetsQr() throws Exception {
        String      sgqrString = "00020101021133820011SG.COM.NETS01231012345678932123123595902120111103600130308360013019908C7F6447F51161234567890ABCDEF52040000530370254071234.565802SG5924INTERNAL TESTING MSS JUN6009SINGAPORE610688833363047191";
        NetsQrModel model      = QrUtils.convertSgQrStringToNetsQrModel(sgqrString);
        System.out.println(model.toDebugString());


        String emptyAmtSgqr = "00020101021133820011SG.COM.NETS01231012345678932123123595902120111103600130308360013019908C7F6447F51161234567890ABCDEF5204000053037025802SG5924INTERNAL TESTING MSS JUN6009SINGAPORE610688833363047ae3";
        String freeAmtSgqr  = "00020101021133820011SG.COM.NETS01231012345678932123123595902120111103600130308360013019908C7F6447F51161234567890ABCDEF520400005303702540105802SG5924INTERNAL TESTING MSS JUN6009SINGAPORE610688833363044199";

        NetsQrModel modelEmpty = QrUtils.convertSgQrStringToNetsQrModel(emptyAmtSgqr);
        System.out.println(modelEmpty.toDebugString());
        NetsQrModel modelFree = QrUtils.convertSgQrStringToNetsQrModel(freeAmtSgqr);
        System.out.println(modelFree.toDebugString());

        String invalidTooManyDecimalsSgqr = "00020101021133820011SG.COM.NETS01231012345678932123123595902120111103600130308360013019908C7F6447F51161234567890ABCDEF520400005303702540612.3455802SG5924INTERNAL TESTING MSS JUN6009SINGAPORE610688833363049b3b";
        try {
            NetsQrModel model2 = QrUtils.convertSgQrStringToNetsQrModel(invalidTooManyDecimalsSgqr);
            Assert.fail();
        } catch (Exception ignored) {

        }
    }

    String generateRandom() {
        SecureRandom  secureRandom = new SecureRandom();
        StringBuilder genOtrs      = new StringBuilder();
        for (int i = 0; i < 4; i++) {
            genOtrs.append(secureRandom.nextInt(10));
        }
        return genOtrs.toString();
    }

    //2449718.500000
    @Test
    public void timeStampTest() {
        String currtime = getCurrentTimeStamp();
        System.out.println(currtime);

    }

    private String getCurrentTimeStamp() {
        String zeros    = "00000000";
        long   currTime = System.currentTimeMillis() / 1000L;
        System.out.println(currTime);
        long julianDateTime = currTime + 8 * 60 * 60 - 9131 * 86400;
        System.out.println(julianDateTime);
        String timeString = Long.toHexString(julianDateTime);
        if (timeString.length() < 8) {
            timeString = zeros.substring(0, 8 - timeString.length()) + timeString;
        }
        return timeString;
    }

    @Test
    public void testFindNetsMerchantInfo(){
        HashMap<Integer, SgQrObject> infos = new HashMap<>();
        infos.put(27, new SgQrObject("27", "15", "0011" + "12341234123"));
        infos.put(29, new SgQrObject("29", "19", "0015" + SgQrConsts.MerchantInfo.Nets.UNIQUE_IDENTIFIER + "1234"));
        infos.put(28, new SgQrObject("28", "30", "0026" + "12341234123412341234123412"));
//        infos.put(26, new SgQrObject("26", "15", "0011" + SgQrConsts.MerchantInfo.Nets.UNIQUE_IDENTIFIER));
        infos.put(26, new SgQrObject("45", "30", "0011" + SgQrConsts.MerchantInfo.Nets.UNIQUE_IDENTIFIER + "123412341234123"));

        SgQrObject object = QrUtils.findNetsMerchantAcctInfo(infos);
        if(object == null){
            Assert.fail();
        } else {
            System.out.println("Found ID: Len: Data: " + object.id + ":" + object.length + ":" + object.data);
        }

    }

//    @Test
//    public void testConvertAmount() {
//        String noDecimal                   = "1234";
//        String withDecimal1dp              = "12.3";
//        String withDecimal2dp              = "12.34";
//        String withDecimal0dp              = "12.";
//        String notTooLargeNoDecimal        = "123456";
//        String notTooLargeWithDecimal      = "123456.78";
//        String manyLeadingZerosNoDecimal   = "000000001234";
//        String manyLeadingZerosWithDecimal = "0000001234.56";
//
//        String tooLargeNoDecimal   = "1234567";
//        String tooLargeWithDecimal = "1234567.89";
//        String tooLongNoDecimal    = "12345678901234";
//        String tooLongWithDecimal  = "1234567890.123";
//
//        try {
//            System.out.println(QrUtils.convertSgQrAmtToNetsQrAmt(noDecimal));
//            System.out.println(QrUtils.convertSgQrAmtToNetsQrAmt(withDecimal1dp));
//            System.out.println(QrUtils.convertSgQrAmtToNetsQrAmt(withDecimal2dp));
//            System.out.println(QrUtils.convertSgQrAmtToNetsQrAmt(withDecimal0dp));
//            System.out.println(QrUtils.convertSgQrAmtToNetsQrAmt(notTooLargeNoDecimal));
//            System.out.println(QrUtils.convertSgQrAmtToNetsQrAmt(notTooLargeWithDecimal));
//            System.out.println(QrUtils.convertSgQrAmtToNetsQrAmt(manyLeadingZerosNoDecimal));
//            System.out.println(QrUtils.convertSgQrAmtToNetsQrAmt(manyLeadingZerosWithDecimal));
//        } catch (QrConversionException qce) {
//            qce.printStackTrace();
//            Assert.fail();
//        }
//        try {
//            QrUtils.convertSgQrAmtToNetsQrAmt(tooLargeNoDecimal);
//            Assert.fail();
//        } catch (QrConversionException ignored) { }
//        try {
//            QrUtils.convertSgQrAmtToNetsQrAmt(tooLargeWithDecimal);
//            Assert.fail();
//        } catch (QrConversionException ignored) { }
//        try {
//            QrUtils.convertSgQrAmtToNetsQrAmt(tooLongNoDecimal);
//            Assert.fail();
//        } catch (QrConversionException ignored) { }
//        try {
//            QrUtils.convertSgQrAmtToNetsQrAmt(tooLongWithDecimal);
//            Assert.fail();
//        } catch (QrConversionException ignored) { }
//    }

}