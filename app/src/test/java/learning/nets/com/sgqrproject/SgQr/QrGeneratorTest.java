package learning.nets.com.sgqrproject.SgQr;

import org.junit.Test;

import java.util.HashMap;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

/**
 * QrGeneratorTest
 * Created by jason on 26/1/18.
 */
public class QrGeneratorTest {

    String UEN             = "0123456789";
    String SECRET_STRING   = "THISISNETSSECRETSTRINGFORTESTING";
    String MID             = "11135036100";
    String TID             = "35036103";
    String HEADER          = "NETSqpay";
    String EXPIRY          = "31122018";
    String VERSION         = "1";
    String MERCHANT_NAME   = "TEST_MERCHANT_NETS";
    String AMOUNT          = "00001234";
    String REFERENCE       = "1A2B3C4D5E6F";
    String REFERENCE_EMPTY = "";
    String QUALIFIER_FALSE = "0";
    String QUALIFIER_TRUE  = "1";
    String WEBQR_TRUE      = "1";
    String WEBQR_FALSE     = "0";

    @Test
    public void doQrGenerationTest() {
        NetsQrModel model = new NetsQrModel.Builder()
                .header("NETSqpay")
                .version("0")
                .qrIssuer(UEN)
                .qrExpDate(EXPIRY)
                .mid(MID)
                .merchantName(MERCHANT_NAME)
                .tid(TID)
                .txnAmout(AMOUNT)
                .txnRefNumber(REFERENCE)
                .secretString(SECRET_STRING)
                .build();
        System.out.println(model.toString());
        try {
            QrUtils.parseNetsQrString(model.toString());
        } catch (Exception e) {
            System.out.println("ERROR");
        }

    }

    @Test
    public void generateHeruQr() {
        NetsQrModel model = new NetsQrModel.Builder()
                .header("NETSqpay")
                .version("0")
                .qrIssuer("0123456789")
                .qrExpDate("31122018")
                .mid("11135036100")
                .merchantName("NETS_TST_QR_DRINKS")
                .tid("35036103")
                .secretString(SECRET_STRING)
                .build();

//        Assert.assertTrue(model.isTxnReferenceProvidedByUser());
//        Assert.assertFalse(model.isTxnReferenceProvidedByUser());

        System.out.println(model.toString());
    }

    @Test
    public void generateQrs() {

        String REFERENCE_EMPTY = "";
        String QUALIFIER_FALSE = "0";
        String QUALIFIER_TRUE  = "1";

        NetsQrModel amtTrueRefTrue = new NetsQrModel.Builder()
                .header(HEADER)
                .version(VERSION)
                .qrIssuer(UEN)
                .qrExpDate(EXPIRY)
                .mid(MID)
                .merchantName(MERCHANT_NAME)
                .tid(TID)
                .txnAmout(AMOUNT)
                .txnRefNumber(REFERENCE)
                .secretString(SECRET_STRING)
                .txnAmountQualifier(QUALIFIER_FALSE)
                .build();

        NetsQrModel amtTrueRef0 = new NetsQrModel.Builder()
                .header(HEADER)
                .version(VERSION)
                .qrIssuer(UEN)
                .qrExpDate(EXPIRY)
                .mid(MID)
                .merchantName(MERCHANT_NAME)
                .tid(TID)
                .txnAmout(AMOUNT)
                .txnRefNumber(REFERENCE_EMPTY)
                .secretString(SECRET_STRING)
                .txnAmountQualifier(QUALIFIER_FALSE)
                .build();

        NetsQrModel amtTrueRefTrueQualifierTrue = new NetsQrModel.Builder()
                .header(HEADER)
                .version(VERSION)
                .qrIssuer(UEN)
                .qrExpDate(EXPIRY)
                .mid(MID)
                .merchantName(MERCHANT_NAME)
                .tid(TID)
                .txnAmout(AMOUNT)
                .txnRefNumber(REFERENCE)
                .secretString(SECRET_STRING)
                .txnAmountQualifier(QUALIFIER_TRUE)
                .build();

        NetsQrModel allFalse = new NetsQrModel.Builder()
                .header(HEADER)
                .version("0")
                .qrIssuer(UEN)
                .qrExpDate(EXPIRY)
                .mid(MID)
                .merchantName(MERCHANT_NAME)
                .tid(TID)
                .secretString(SECRET_STRING)
                .build();

        System.out.println(amtTrueRefTrue.toString());
        System.out.println(amtTrueRef0.toString());
        System.out.println(amtTrueRefTrueQualifierTrue);
        System.out.println(allFalse.toString());
    }

    @Test
    public void conversionTest3() {
        String SgQr1 = "00020101021133810011SG.COM.NETS012310123456789181231235959021111135036100030835036103990803C641E151161234567890ABCDEF5204000053037025802SG5918TEST_MERCHANT_NETS6009SINGAPORE6106888333620064270002EN0117NAMEINALTLANGUAGE6304241b";
        String SgQr2 = "00020101021133860011SG.COM.NETS01231012345678918123123595902111113503610003083503610309011990810E602FC51161234567890ABCDEF5204000053037025408000012345802SG5918TEST_MERCHANT_NETS6009SINGAPORE6106888333621601121A2B3C4D5E6F64270002EN0117NAMEINALTLANGUAGE6304eebb";

        String SgQr3 = "00020101021133810011SG.COM.NETS012310123456789181231235959021111135036100030835036103990803C641E151161234567890ABCDEF5204000053037025802SG5918TEST_MERCHANT_NETS6009SINGAPORE610688833364270002EN0117NAMEINALTLANGUAGE63045a75";

        NetsQrModel model1 = null;
        NetsQrModel model2 = null;

        NetsQrModel model3 = null;
        try {
            model1 = QrUtils.convertSgQrStringToNetsQrModel(SgQr1);
            model2 = QrUtils.convertSgQrStringToNetsQrModel(SgQr2);
            model3 = QrUtils.convertSgQrStringToNetsQrModel(SgQr3);
        } catch (QrConversionException e) {
            System.out.println(e.toString());
            System.out.println(e.code);
            fail();
        }

        assertNull(model3.getTxnAmt());
        assertNull(model3.getTxnRefNumber());
//        assertEquals(model3.getTxnAmtQualifier(), "1");
        assertNull(model3.getTxnAmtQualifier());

//        Assert.assertNull(model2.getTxnAmt());
//        assertNull(model2.getTxnRefNumber());
//        assertEquals(model2.getTxnAmtQualifier(), "1");

    }


    @Test
    public void generateV0Qrs() {

        String REFERENCE_EMPTY = "";
        String QUALIFIER_FALSE = "0";
        String QUALIFIER_TRUE  = "1";
        String VERSION         = "0";

        NetsQrModel amtTrueRefTrue = new NetsQrModel.Builder()
                .header(HEADER)
                .version(VERSION)
                .qrIssuer(UEN)
                .qrExpDate(EXPIRY)
                .mid(MID)
                .merchantName(MERCHANT_NAME)
                .tid(TID)
                .txnAmout(AMOUNT)
                .txnRefNumber(REFERENCE)
                .secretString(SECRET_STRING)
                .txnAmountQualifier(QUALIFIER_FALSE)
                .build();

        NetsQrModel amtTrueRef0 = new NetsQrModel.Builder()
                .header(HEADER)
                .version(VERSION)
                .qrIssuer(UEN)
                .qrExpDate(EXPIRY)
                .mid(MID)
                .merchantName(MERCHANT_NAME)
                .tid(TID)
                .txnAmout(AMOUNT)
                .txnRefNumber(REFERENCE_EMPTY)
                .secretString(SECRET_STRING)
                .txnAmountQualifier(QUALIFIER_FALSE)
                .build();

        NetsQrModel amtTrueRefTrueQualifierTrue = new NetsQrModel.Builder()
                .header(HEADER)
                .version(VERSION)
                .qrIssuer(UEN)
                .qrExpDate(EXPIRY)
                .mid(MID)
                .merchantName(MERCHANT_NAME)
                .tid(TID)
                .txnAmout(AMOUNT)
                .txnRefNumber(REFERENCE)
                .secretString(SECRET_STRING)
                .txnAmountQualifier(QUALIFIER_TRUE)
                .build();

        System.out.println(amtTrueRefTrue.toString());
        System.out.println(amtTrueRef0.toString());
        System.out.println(amtTrueRefTrueQualifierTrue);

    }

//    NETSqpay0030123456789311220180011111350361000118TEST_MERCHANT_NETS020835036103150331E57BD5C448ACFB344DB46BCAD0064116F6CDDDFD8F642A9EDB9812F68D
//1FB641C4

    @Test
    public void generateSgQrTest() {
        MerchantInfoBuilder.Nets netsInfoBuilder = new MerchantInfoBuilder.Nets()
                .qrMetadataVersion(VERSION)
                .qrMetadataIssuerUen(UEN)
                .qrMetadataExpiryTimestamp("181231235959")
                .mid(MID)
                .tid(TID)
                .txnAmtModifier(QUALIFIER_FALSE)
                .signature("A08F2D8D");

        MerchantInfoBuilder.Nets netsInfoBuilderQualifierTrue = new MerchantInfoBuilder.Nets()
                .qrMetadataVersion(VERSION)
                .qrMetadataIssuerUen(UEN)
                .qrMetadataExpiryTimestamp("181231235959")
                .mid(MID)
                .tid(TID)
                .txnAmtModifier(QUALIFIER_TRUE)
                .signature("10E602FC");

        MerchantInfoBuilder.Nets netsInfoBuilderAllFalse = new MerchantInfoBuilder.Nets()
                .qrMetadataVersion(VERSION)
                .qrMetadataIssuerUen(UEN)
                .qrMetadataExpiryTimestamp("181231235959")
                .mid(MID)
                .tid(TID)
                .signature("03C641E1");

        MerchantInfoBuilder.Nets netsInfoBuilderNoQualifier = new MerchantInfoBuilder.Nets()
                .qrMetadataVersion(VERSION)
                .qrMetadataIssuerUen(UEN)
                .qrMetadataExpiryTimestamp("181231235959")
                .mid(MID)
                .tid(TID)
                .signature("1FB641C4");

        MerchantInfoBuilder.Nets netsInfoBuilderMidTidName = new MerchantInfoBuilder.Nets()
                .qrMetadataVersion(VERSION)
                .qrMetadataIssuerUen(UEN)
                .qrMetadataExpiryTimestamp("181231235959")
                .mid(MID)
                .tid(TID)
                .signature("150331E5");


        SgQrObject netsMerchantInfoAllFalse      = netsInfoBuilderAllFalse.build();
        SgQrObject netsMerchantInfo              = netsInfoBuilder.build();
        SgQrObject netsMerchantInfoQualifierTrue = netsInfoBuilderQualifierTrue.build();
        SgQrObject netsMerchantInfoNoQualifier   = netsInfoBuilderNoQualifier.build();
        SgQrObject netsMerchantInfoMidTidName    = netsInfoBuilderMidTidName.build();


        HashMap<Integer, SgQrObject> addnDataWithRef = new HashMap<>();
        addnDataWithRef.put(SgQrConsts.AddnData.ID_BILL_NUMBER,
                            new SgQrObject(SgQrConsts.AddnData.ID_BILL_NUMBER,
                                           REFERENCE.length(),
                                           REFERENCE));

        HashMap<Integer, SgQrObject> addnDataBlankRef = new HashMap<>();
        addnDataBlankRef.put(SgQrConsts.AddnData.ID_BILL_NUMBER,
                             new SgQrObject(SgQrConsts.AddnData.ID_BILL_NUMBER,
                                            3,
                                            "***"));


        HashMap<Integer, SgQrObject> langData = new HashMap<>();
        langData.put(SgQrConsts.LanguageTemplateData.ID_LANGUAGE_PREFERENCE,
                     new SgQrObject(SgQrConsts.LanguageTemplateData.ID_LANGUAGE_PREFERENCE,
                                    2,
                                    "EN"));
        langData.put(SgQrConsts.LanguageTemplateData.ID_NAME_IN_ALT_LANGUAGE,
                     new SgQrObject(SgQrConsts.LanguageTemplateData.ID_NAME_IN_ALT_LANGUAGE,
                                    17,
                                    "NAMEINALTLANGUAGE"));


        SgQrModel.Builder amtTrueRefTrueBuilder = new SgQrModel.Builder()
                .payloadFormatIndicator(SgQrModel.Builder.PAYLOAD_FORMAT_INDICATOR_DEFAULT)
                .pointOfInitiationMethod(SgQrModel.Builder.POINT_OF_INITIATION_STATIC)
                .addAcctInfoSg(Integer.valueOf(netsMerchantInfo.id), netsMerchantInfo)
                .sgQrIdentityInformation("1234567890ABCDEF")
                .transactionAmount(AMOUNT)
                .merchantName(MERCHANT_NAME)
                .setAdditionalData(addnDataWithRef)
                .setMerchantInformationLanguageTemplate(langData)
                .postalCode("888333");

        SgQrModel.Builder amtTrueRef0Builder = new SgQrModel.Builder()
                .payloadFormatIndicator(SgQrModel.Builder.PAYLOAD_FORMAT_INDICATOR_DEFAULT)
                .pointOfInitiationMethod(SgQrModel.Builder.POINT_OF_INITIATION_STATIC)
                .addAcctInfoSg(Integer.valueOf(netsMerchantInfo.id), netsMerchantInfo)
                .sgQrIdentityInformation("1234567890ABCDEF")
                .transactionAmount(AMOUNT)
                .merchantName(MERCHANT_NAME)
                .setAdditionalData(addnDataBlankRef)
                .setMerchantInformationLanguageTemplate(langData)
                .postalCode("888333");

        SgQrModel.Builder amtTrueRefTrueQualifierTrueBuilder = new SgQrModel.Builder()
                .payloadFormatIndicator(SgQrModel.Builder.PAYLOAD_FORMAT_INDICATOR_DEFAULT)
                .pointOfInitiationMethod(SgQrModel.Builder.POINT_OF_INITIATION_STATIC)
                .addAcctInfoSg(Integer.valueOf(netsMerchantInfoQualifierTrue.id), netsMerchantInfoQualifierTrue)
                .sgQrIdentityInformation("1234567890ABCDEF")
                .transactionAmount(AMOUNT)
                .merchantName(MERCHANT_NAME)
                .setAdditionalData(addnDataWithRef)
                .setMerchantInformationLanguageTemplate(langData)
                .postalCode("888333");

        SgQrModel.Builder allfalse = new SgQrModel.Builder()
                .payloadFormatIndicator(SgQrModel.Builder.PAYLOAD_FORMAT_INDICATOR_DEFAULT)
                .pointOfInitiationMethod(SgQrModel.Builder.POINT_OF_INITIATION_STATIC)
                .addAcctInfoSg(Integer.valueOf(netsMerchantInfoAllFalse.id), netsMerchantInfoAllFalse)
                .sgQrIdentityInformation("1234567890ABCDEF")
                .merchantName(MERCHANT_NAME)
                .setMerchantInformationLanguageTemplate(langData)
                .postalCode("888333");

        SgQrModel.Builder midTidMnameAmtRefBuilder = new SgQrModel.Builder()
                .payloadFormatIndicator(SgQrModel.Builder.PAYLOAD_FORMAT_INDICATOR_DEFAULT)
                .pointOfInitiationMethod(SgQrModel.Builder.POINT_OF_INITIATION_STATIC)
                .addAcctInfoSg(Integer.valueOf(netsMerchantInfoNoQualifier.id), netsMerchantInfoNoQualifier)
                .sgQrIdentityInformation("1234567890ABCDEF")
                .transactionAmount(AMOUNT)
                .merchantName(MERCHANT_NAME)
                .setAdditionalData(addnDataWithRef)
                .setMerchantInformationLanguageTemplate(langData)
                .postalCode("888333");

        SgQrModel.Builder midTidMnameBuilder = new SgQrModel.Builder()
                .payloadFormatIndicator(SgQrModel.Builder.PAYLOAD_FORMAT_INDICATOR_DEFAULT)
                .pointOfInitiationMethod(SgQrModel.Builder.POINT_OF_INITIATION_STATIC)
                .addAcctInfoSg(Integer.valueOf(netsMerchantInfoMidTidName.id), netsMerchantInfoMidTidName)
                .sgQrIdentityInformation("1234567890ABCDEF")
                .merchantName(MERCHANT_NAME);


        SgQrModel amtTrueRefTrue              = amtTrueRefTrueBuilder.build();
        SgQrModel amtTrueRef0                 = amtTrueRef0Builder.build();
        SgQrModel amtTrueRefTrueQualifierTrue = amtTrueRefTrueQualifierTrueBuilder.build();
        SgQrModel midTidMnameAmtRef           = midTidMnameAmtRefBuilder.build();
        SgQrModel midtidmname                 = midTidMnameBuilder.build();
        System.out.println(amtTrueRefTrue.toString());
        System.out.println(amtTrueRef0.toString());
        System.out.println(amtTrueRefTrueQualifierTrue.toString());

        try {
            String netsQrAmtTrueRefTrue = QrUtils.convertSgQrToNetsQr(amtTrueRefTrue.toString());
            System.out.println(netsQrAmtTrueRefTrue);

            String netsQramtTrueRef0 = QrUtils.convertSgQrToNetsQr(amtTrueRef0.toString());
            System.out.println(netsQramtTrueRef0);

            String netsQramtTrueRefTrueQualifierTrue = QrUtils.convertSgQrToNetsQr(amtTrueRefTrueQualifierTrue.toString());
            System.out.println(netsQramtTrueRefTrueQualifierTrue);
        } catch (Exception e) {
            e.printStackTrace();
//            System.out.println(e);
        }

        System.out.println(allfalse.build().toString());
        System.out.println(midTidMnameAmtRef.toString());
        System.out.println(midtidmname.toString());
    }


    @Test
    public void parseNetsQrTest() throws Exception {
        String netsQrString = HEADER + VERSION + "06" + UEN + EXPIRY +
                              "0011" + MID +
                              "0116" + "TESTMERCHANTNETS" +
                              "0208" + TID +
                              "0308" + AMOUNT +
                              "0712" + REFERENCE +
                              "0901" + "0" + "d83e3f50";

        System.out.println(netsQrString);

        NetsQrModel finalModel = QrUtils.parseNetsQrString(netsQrString);
        assertFalse(isNullOrEmpty(finalModel.getHeader()));
        assertFalse(isNullOrEmpty(finalModel.getVersion()));
        assertFalse(isNullOrEmpty(finalModel.getQrIssuer()));
        assertFalse(isNullOrEmpty(finalModel.getQrExpDate()));
        assertFalse(isNullOrEmpty(finalModel.getMid()));
        assertFalse(isNullOrEmpty(finalModel.getMerchantName()));
        assertFalse(isNullOrEmpty(finalModel.getTid()));
        assertFalse(isNullOrEmpty(finalModel.getTxnAmt()));
        assertFalse(isNullOrEmpty(finalModel.getTxnRefNumber()));
        assertFalse(isNullOrEmpty(finalModel.getTxnAmtQualifier()));
        assertFalse(isNullOrEmpty(finalModel.getSignature()));
    }

    boolean isNullOrEmpty(String input) {
        return input == null || input.isEmpty();
    }

    @Test
    public void generateMidTidMerchantName() {
        NetsQrModel model = new NetsQrModel.Builder()
                .header(HEADER)
                .version("0")
                .qrIssuer(UEN)
                .qrExpDate(EXPIRY)
                .mid(MID)
                .tid(TID)
                .merchantName(MERCHANT_NAME)
                .secretString(SECRET_STRING)
                .build();

        System.out.println(model.toString());
    }
//    NETSqpay0030123456789311220180011111350361000118TEST_MERCHANT_NETS020835036103150331E57BD5C448ACFB344DB46BCAD0064116F6CDDDFD8F642A9EDB9812F68D
//    00020101021133810011SG.COM.NETS0123101234567891812312359590211111350361000308350361039908150331E551161234567890ABCDEF5204000053037025802SG5918TEST_MERCHANT_NETS6009SINGAPORE6304c613

    @Test
    public void parseMidTidMerchantName() throws Exception {
        NetsQrModel model1 = QrUtils.convertSgQrStringToNetsQrModel("00020101021133810011SG.COM.NETS0123101234567891812312359590211111350361000308350361039908150331E551161234567890ABCDEF5204000053037025802SG5918TEST_MERCHANT_NETS6009SINGAPORE6304c613");
        NetsQrModel model2 = QrUtils.parseNetsQrString("NETSqpay0030123456789311220180011111350361000118TEST_MERCHANT_NETS020835036103150331E57BD5C448ACFB344DB46BCAD0064116F6CDDDFD8F642A9EDB9812F68D");

        System.out.println(model1.toDebugString());
        System.out.println(model2.toDebugString());
    }

    @Test
    public void generateSample() throws QrConversionException {
        NetsQrModel model = new NetsQrModel.Builder()
                .header(HEADER)
                .version("0")
                .qrIssuer(UEN)
                .qrExpDate("31122032")
                .mid(MID)
                .merchantName(MERCHANT_NAME)
                .tid(TID)
                .minMaxValue("0000030000001000")
                .secretString(SECRET_STRING)
                .build();
        System.out.println(model.toDebugString());
        System.out.println(model.toString());
    }

}
