package learning.nets.com.sgqrproject;

import junit.framework.Assert;

import org.junit.Test;

import java.security.MessageDigest;

import static org.junit.Assert.assertEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }


    String data = "The SHA (Secure Hash Algorithm) is one of a number of cryptographic hash functions. A cryptographic hash is like a signature for a text or a data file. SHA-256 algorithm generates an almost-unique, fixed size 256-bit (32-byte) hash. Hash is a one way function – it cannot be decrypted back. This makes it suitable for password validation, challenge hash authentication, anti-tamper, digital signatures.\n" +
            "\n" +
            "SHA-256 is one of the successor hash functions to SHA-1, and is one of the strongest hash functions available.\n" +
            "\n" +
            "With this online tool you can easily generate hashes.";

    String calculatedHash = "4d0b191d40d8db87562ba2b306560a2bdb97ba8941c3bcd4b96792b0f3af7aef";

    @Test
    public void sha256Test(){
        String signature = generateSignature("", "0", data);
        Assert.assertTrue(signature.equalsIgnoreCase(calculatedHash));
        String signaturev1 = generateSignature("", "1", data);
        Assert.assertTrue(signaturev1.equalsIgnoreCase("4d0b"));
    }

    private String generateSignature(String secretString, String version, String data) {
        String dataWithSecret = data + secretString;
        try{
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
            messageDigest.update(dataWithSecret.getBytes("UTF-8"));
            byte[] digest = messageDigest.digest();
            String signatureAsHex =  bytesToHex(digest);
            if(version.equals("1")){
                return signatureAsHex.substring(0, 4);
            }
            return signatureAsHex;
        } catch (Exception e){
            if(version.equals("1")){
                return "0000";
            } else {
                return "0000000000000000000000000000000000000000000000000000000000000000";
            }
        }
    }

    String bytesToHex(byte[] bytes) {
        final char[] hexArray = "0123456789ABCDEF".toCharArray();
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }


    private boolean isNullOrEmpty(String s) {
        return s == null || s.isEmpty();
    }
}