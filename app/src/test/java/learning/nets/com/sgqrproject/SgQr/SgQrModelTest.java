package learning.nets.com.sgqrproject.SgQr;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

/**
 *
 * Created by jason on 22/11/2017.
 */
public class SgQrModelTest {
  private String dataLessCrcValue = "00020101021102164761360000000*171110123456789012153123456789012341531250003440001034450003445311000126330015SG.COM.DASH.WWW0110000005550127820014A0000007620001011580000000000000102030010325WWW.LIQUIDPAY.COM/PAT000104050010128660011SG.COM.OCBC0147OCBCP2P629A358D-ECE7-4554-AD56-EBD12D84CA7E4F7329410006SG.EZI010812345678020812345678030312330850013SG.COM.EZLINK01201234567890123456-1230204SGQR0324A123456,B123456,C12345670404A23X31260008COM.GRAB0110A93FO3230Q32390007COM.DBS011012345678900210123456789033850011SG.COM.NETS012302014018328311288235900021500011187032400003088858720199084E5DC3D834430017COM.QQ.WEIXIN.PAY011012345678900204123435660010SG.COM.UOB014845D233507F5E8C306E3871A4E9FACA601A80C114B5645E5D36470009SG.PAYNOW010100208912345670301004082020123137270009SG.AIRPAY0110A11BC0000X51390007SG.SGQR01112017123456X0209S1234567A5204581453037025802SG5916FOOD XYZ PTE LTD6009SINGAPORE61060810066304";
  private String dataFull = "00020101021102164761360000000*171110123456789012153123456789012341531250003440001034450003445311000126330015SG.COM.DASH.WWW0110000005550127820014A0000007620001011580000000000000102030010325WWW.LIQUIDPAY.COM/PAT000104050010128660011SG.COM.OCBC0147OCBCP2P629A358D-ECE7-4554-AD56-EBD12D84CA7E4F7329410006SG.EZI010812345678020812345678030312330850013SG.COM.EZLINK01201234567890123456-1230204SGQR0324A123456,B123456,C12345670404A23X31260008COM.GRAB0110A93FO3230Q32390007COM.DBS011012345678900210123456789033850011SG.COM.NETS012302014018328311288235900021500011187032400003088858720199084E5DC3D834430017COM.QQ.WEIXIN.PAY011012345678900204123435660010SG.COM.UOB014845D233507F5E8C306E3871A4E9FACA601A80C114B5645E5D36470009SG.PAYNOW010100208912345670301004082020123137270009SG.AIRPAY0110A11BC0000X51390007SG.SGQR01112017123456X0209S1234567A5204581453037025802SG5916FOOD XYZ PTE LTD6009SINGAPORE61060810066304F175";
  private String merchantInfoNets = "33850011SG.COM.NETS012302014018328311288235900021500011187032400003088858720199084E5DC3D8";

  SgQrModel model;

  @Before
  public void setup(){
    try{
      model = QrUtils.parseSgQrString(dataFull);
    } catch (Exception e){
      e.printStackTrace();
    }
  }

  @Test
  public void toSgQrString() throws Exception {
    assertEquals(dataFull, model.toString());
  }

  @Test
  public void toStringTest() throws Exception {
    assertNotNull(model.toString());
  }

  @Test
  public void getAdtBillNumber() throws Exception {
    int itemId = SgQrConsts.AddnData.ID_BILL_NUMBER;
    String testData = "TEST_DATA";
    SgQrObject obj = model.additionalData.put(itemId, new SgQrObject(QrUtils.intToString2sf(itemId), QrUtils.intToString2sf(testData.length()), testData));
    obj = model.additionalData.get(itemId);
    obj.data = "TEST_DATA";
    assertEquals(model.getAdtBillNumber(), "TEST_DATA");
  }

  @Test
  public void getAdtMobileNumber() throws Exception {
    int itemId = SgQrConsts.AddnData.ID_MOBILE_NUMBER;
    String testData = "TEST_DATA";
    SgQrObject obj = model.additionalData.put(itemId, new SgQrObject(QrUtils.intToString2sf(itemId), QrUtils.intToString2sf(testData.length()), testData));
    obj = model.additionalData.get(itemId);
    obj.data = "TEST_DATA";
    assertEquals(model.getAdtMobileNumber(), "TEST_DATA");
  }

  @Test
  public void getAdtLoyaltyNumber() throws Exception {
    int itemId = SgQrConsts.AddnData.ID_LOYALTY_NUMBER;
    String testData = "TEST_DATA";
    SgQrObject obj = model.additionalData.put(itemId, new SgQrObject(QrUtils.intToString2sf(itemId), QrUtils.intToString2sf(testData.length()), testData));
    obj = model.additionalData.get(itemId);
    obj.data = "TEST_DATA";
    assertEquals(model.getAdtLoyaltyNumber(), "TEST_DATA");
  }

  @Test
  public void getAdtStoreLabel() throws Exception {
    int itemId = SgQrConsts.AddnData.ID_STORE_LABEL;
    String testData = "TEST_DATA";
    SgQrObject obj = model.additionalData.put(itemId, new SgQrObject(QrUtils.intToString2sf(itemId), QrUtils.intToString2sf(testData.length()), testData));
    obj = model.additionalData.get(itemId);
    obj.data = "TEST_DATA";
    assertEquals(model.getAdtStoreLabel(), "TEST_DATA");
  }

  @Test
  public void getAdtReferenceLabel() throws Exception {
    int itemId = SgQrConsts.AddnData.ID_REFERENCE_LABEL;
    String testData = "TEST_DATA";
    SgQrObject obj = model.additionalData.put(itemId, new SgQrObject(QrUtils.intToString2sf(itemId), QrUtils.intToString2sf(testData.length()), testData));
    obj = model.additionalData.get(itemId);
    obj.data = "TEST_DATA";
    assertEquals(model.getAdtReferenceLabel(), "TEST_DATA");
  }

  @Test
  public void getAdtCustomerLabel() throws Exception {
    int itemId = SgQrConsts.AddnData.ID_CUSTOMER_LABEL;
    String testData = "TEST_DATA";
    SgQrObject obj = model.additionalData.put(itemId, new SgQrObject(QrUtils.intToString2sf(itemId), QrUtils.intToString2sf(testData.length()), testData));
    obj = model.additionalData.get(itemId);
    obj.data = "TEST_DATA";
    assertEquals(model.getAdtCustomerLabel(), "TEST_DATA");
  }

  @Test
  public void getAdtTerminalLabel() throws Exception {
    int itemId = SgQrConsts.AddnData.ID_TERMINAL_LABEL;
    String testData = "TEST_DATA";
    SgQrObject obj = model.additionalData.put(itemId, new SgQrObject(QrUtils.intToString2sf(itemId), QrUtils.intToString2sf(testData.length()), testData));
    obj = model.additionalData.get(itemId);
    obj.data = "TEST_DATA";
    assertEquals(model.getAdtTerminalLabel(), "TEST_DATA");
  }

  @Test
  public void getPurposeOfTxn() throws Exception {
    int itemId = SgQrConsts.AddnData.ID_PURPOSE_OF_TXN;
    String testData = "TEST_DATA";
    SgQrObject obj = model.additionalData.put(itemId, new SgQrObject(QrUtils.intToString2sf(itemId), QrUtils.intToString2sf(testData.length()), testData));
    obj = model.additionalData.get(itemId);
    obj.data = "TEST_DATA";
    assertEquals(model.getPurposeOfTxn(), "TEST_DATA");
  }

  @Test
  public void getAdtAddnCustDataReq() throws Exception {
    int itemId = SgQrConsts.AddnData.ID_ADDN_CUST_DATA_REQ;
    String testData = "TEST_DATA";
    SgQrObject obj = model.additionalData.put(itemId, new SgQrObject(QrUtils.intToString2sf(itemId), QrUtils.intToString2sf(testData.length()), testData));
    obj = model.additionalData.get(itemId);
    obj.data = "TEST_DATA";
    assertEquals(model.getAdtAddnCustDataReq(), "TEST_DATA");
  }

  @Test
  public void getAdtPaymentSpecificTemplates() throws Exception {
    int itemId = SgQrConsts.AddnData.ID_PAYMENT_SYSTEMS_SPECIFIC_TEMPLATES;
    String testData = "TEST_DATA";
    SgQrObject obj = model.additionalData.put(itemId, new SgQrObject(QrUtils.intToString2sf(itemId), QrUtils.intToString2sf(testData.length()), testData));
    obj = model.additionalData.get(itemId);
    obj.data = "TEST_DATA";
    assertEquals(model.getAdtPaymentSpecificTemplates(), "TEST_DATA");
  }

}