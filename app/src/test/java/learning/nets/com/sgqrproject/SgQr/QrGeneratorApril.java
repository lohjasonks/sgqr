package learning.nets.com.sgqrproject.SgQr;

import junit.framework.Assert;

import org.junit.Test;

import java.util.HashMap;

/**
 * QrGeneratorApril
 * Created by jason on 2/4/18.
 */
public class QrGeneratorApril {

    boolean GENERATE_NETS_STATIC_QR = true;
    boolean GENERATE_SGQR           = true;
    boolean PRINT_DEBUG             = false;

    String UEN             = "0123456789";
    String SECRET_STRING   = "THISISNETSSECRETSTRINGFORTESTING";
//    String MID             = "11135036100";
//    String TID             = "35036104";
        String MID             = "10101254114";
    String TID             = "35036105";
    String HEADER          = "NETSqpay";
    String EXPIRY          = "31122032";
    String VERSION1        = "1";
    String VERSION0        = "0";
    String MERCHANT_NAME   = "TEST_MERCHANT_NETS";
    String AMOUNT          = "00001234";
    String REFERENCE       = "1A2B3C4D5E6F";
    String REFERENCE_EMPTY = "";
    String QUALIFIER_FALSE = "0";
    String QUALIFIER_TRUE  = "1";
    String TIMESTAMP       = "02042018083015";
    String MINMAX          = "0000000199999999";
    String QRFORM0         = "0";
    String QRFORM1         = "1";
    String SGQRTIMESTAMP   = "321231235959";
    String SGQRID          = "1234567890ABCDEF";
    String POSTALCODE      = "888333";

    String MID_ORG = "11135036100";
    String TID_ORG = "35036103";


    //region NETS STATIC QRs


    @Test
    public void generateNetsStaticQrs() {
        if (GENERATE_NETS_STATIC_QR) {
            generateQr1();
            generateQr2();
            generateQr3();
            generateQr4();
            generateQr5();
        }
    }

    @Test
    public void generateSgQrs() {
        if (GENERATE_SGQR) {
            generateSgQr1();
            generateSgQr2();
            generateSgQr3();
//            generateSgQr4();
            generateSgQr5();
            generateSgQr6();
        }
    }

    @Test
    public void samenessTest() {
        NetsQrModel model = new NetsQrModel.Builder()
                .header(HEADER)
                .version(VERSION1)
                .qrIssuer(UEN)
                .qrExpDate(EXPIRY)
                .mid(MID)
                .merchantName(MERCHANT_NAME)
                .tid(TID)
                .txnAmout(AMOUNT)
                .txnRefNumber(REFERENCE)
                .secretString(SECRET_STRING)
                .build();

        String sig = model.signature.substring(0, 8);
//        6E9E3707
        String initial = model.toString();
        System.out.println(initial);
        System.out.println(model.toDebugString());
        try {
            QrUtils.parseNetsQrString(model.toString());
        } catch (Exception e) {
            System.out.println("ERROR");
        }

        MerchantInfoBuilder.Nets netsInfoBuilder = new MerchantInfoBuilder.Nets()
                .qrMetadataVersion(VERSION1)
                .qrMetadataIssuerUen(UEN)
                .qrMetadataExpiryTimestamp(SGQRTIMESTAMP)
                .mid(MID)
                .tid(TID)
                .signature(sig);

        SgQrObject netsMerchantInfo = netsInfoBuilder.build();

        HashMap<Integer, SgQrObject> addnDataWithRef = new HashMap<>();
        addnDataWithRef.put(SgQrConsts.AddnData.ID_BILL_NUMBER,
                            new SgQrObject(SgQrConsts.AddnData.ID_BILL_NUMBER,
                                           REFERENCE.length(),
                                           REFERENCE));

        SgQrModel.Builder sgQrBuilder = new SgQrModel.Builder()
                .payloadFormatIndicator(SgQrModel.Builder.PAYLOAD_FORMAT_INDICATOR_DEFAULT)
                .pointOfInitiationMethod(SgQrModel.Builder.POINT_OF_INITIATION_STATIC)
                .addAcctInfoSg(Integer.valueOf(netsMerchantInfo.id), netsMerchantInfo)
                .sgQrIdentityInformation(SGQRID)
                .merchantName(MERCHANT_NAME)
                .transactionAmount(AMOUNT)
                .setAdditionalData(addnDataWithRef)
                .postalCode(POSTALCODE);

        SgQrModel sgQrModel = sgQrBuilder.build();
        System.out.println(sgQrModel.toString());

        try {
            String      netsQrModelString = QrUtils.convertSgQrToNetsQr(sgQrModel.toString());
            NetsQrModel netsQrModel       = QrUtils.parseNetsQrString(netsQrModelString);
            System.out.println(netsQrModel.toString());
            System.out.println(netsQrModel.toDebugString());
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }


    //MID_ORG = "11135036100"; TID_ORG = "35036103";
    //No Amt, No Ref
    //00020101021133810011SG.COM.NETS0123101234567893212312359590211111350361000308350361039908FACF425E51161234567890ABCDEF5204000053037025802SG5918TEST_MERCHANT_NETS6009SINGAPORE610688833363041ca0
    //Yes Amt, Yes Ref, Qualifier = False
    //00020101021133860011SG.COM.NETS0123101234567893212312359590211111350361000308350361030901099083F1E694151161234567890ABCDEF5204000053037025408000012345802SG5918TEST_MERCHANT_NETS6009SINGAPORE6106888333621601121A2B3C4D5E6F6304a137
    //Yes Amt, Ref empty, Qualifier = False
    //00020101021133860011SG.COM.NETS0123101234567893212312359590211111350361000308350361030901099083F1E694151161234567890ABCDEF5204000053037025408000012345802SG5918TEST_MERCHANT_NETS6009SINGAPORE610688833362070103***630403c4
    //Yes Amt, Yes Ref, Qualifier = True
    //00020101021133860011SG.COM.NETS012310123456789321231235959021111135036100030835036103090119908AE53D1B751161234567890ABCDEF5204000053037025408000012345802SG5918TEST_MERCHANT_NETS6009SINGAPORE6106888333621601121A2B3C4D5E6F63042ff3
    //Yes Amt, Yes Ref
    //00020101021133810011SG.COM.NETS012310123456789321231235959021111135036100030835036103990851DF710C51161234567890ABCDEF5204000053037025408000012345802SG5918TEST_MERCHANT_NETS6009SINGAPORE6106888333621601121A2B3C4D5E6F630419ab

    //MID, TID
    //No Amt, No Ref
    //Yes Amt, Yes Ref, Qual = false
    //Yes Amt, Ref Empty, Qual = false
    //Yes Amt, Yes ref, Qual = true
    //Yes Amt, Yes Ref
    @Test
    public void generateNetsAndSgQr() {
        String mid             = null;
        String merchantName    = null;
        String tid             = null;
        String amount          = null;
        String timeStamp       = null;
        String minMax          = null;
        String reference       = null;
        String qrForm          = null;
        String txnAmtQualifier = null;


        //EDIT HERE
        //COMMENT OUT IF NOT INCLUDED IN QR
        //MODIFY VALUE AS NEEDED

        mid = MID;
        merchantName = MERCHANT_NAME;
        tid = TID;
        amount = AMOUNT;
//        timeStamp = TIMESTAMP;
//        minMax = MINMAX;
        reference = REFERENCE;
//        qrForm = QRFORM0;
//        txnAmtQualifier = QUALIFIER_TRUE;
        String  version            = VERSION1;
        boolean shouldGenerateSgQr = true;

        //END OF VARIABLES TO EDIT


        NetsQrModel.Builder modelBuilder = new NetsQrModel.Builder()
                .header(HEADER)
                .version(version)
                .qrIssuer(UEN)
                .qrExpDate(EXPIRY)
                .mid(mid)
                .merchantName(merchantName);

        if (tid != null) {
            modelBuilder.tid(tid);
        }
        if (amount != null) {
            modelBuilder.txnAmout(amount);
        }
        if (timeStamp != null) {
            modelBuilder.txnTimestamp(timeStamp);
        }
        if (minMax != null) {
            modelBuilder.minMaxValue(minMax);
        }
        if (reference != null) {
            modelBuilder.txnRefNumber(reference);
        }
        if (qrForm != null) {
            modelBuilder.qrForm(qrForm);
        }
        if (txnAmtQualifier != null) {
            modelBuilder.txnAmountQualifier(txnAmtQualifier);
        }
        NetsQrModel model = modelBuilder.secretString(SECRET_STRING)
                .build();

        String sig = model.signature.substring(0, 8);


        String initial = model.toString();
        System.out.println(initial);
        System.out.println(model.toDebugString());
        try {
            QrUtils.parseNetsQrString(model.toString());
        } catch (Exception e) {
            System.out.println("ERROR");
            Assert.fail();
        }

        if (!shouldGenerateSgQr) {
            return;
        }


        MerchantInfoBuilder.Nets netsInfoBuilder = new MerchantInfoBuilder.Nets()
                .qrMetadataVersion(VERSION1)
                .qrMetadataIssuerUen(UEN)
                .qrMetadataExpiryTimestamp(SGQRTIMESTAMP)
                .mid(mid)
                .signature(sig);
        if (tid != null) {
            netsInfoBuilder.tid(tid);
        }
        if (txnAmtQualifier != null) {
            netsInfoBuilder.txnAmtModifier(txnAmtQualifier);
        }

        SgQrObject netsMerchantInfo = netsInfoBuilder.build();


        HashMap<Integer, SgQrObject> addnDataWithRef = new HashMap<>();
        if (reference != null && !reference.isEmpty()) {
            addnDataWithRef.put(SgQrConsts.AddnData.ID_BILL_NUMBER,
                                new SgQrObject(SgQrConsts.AddnData.ID_BILL_NUMBER,
                                               reference.length(),
                                               reference));
        } else if (reference != null && reference.isEmpty()) {
            addnDataWithRef.put(SgQrConsts.AddnData.ID_BILL_NUMBER,
                                new SgQrObject(SgQrConsts.AddnData.ID_BILL_NUMBER,
                                               3,
                                               "***"));
        }


        SgQrModel.Builder sgQrBuilder = new SgQrModel.Builder()
                .payloadFormatIndicator(SgQrModel.Builder.PAYLOAD_FORMAT_INDICATOR_DEFAULT)
                .pointOfInitiationMethod(SgQrModel.Builder.POINT_OF_INITIATION_STATIC)
                .addAcctInfoSg(Integer.valueOf(netsMerchantInfo.id), netsMerchantInfo)
                .sgQrIdentityInformation(SGQRID)
                .merchantName(merchantName)
                .postalCode(POSTALCODE);

        if (amount != null) {
            sgQrBuilder.transactionAmount(amount);
        }
        if (reference != null) {
            sgQrBuilder.setAdditionalData(addnDataWithRef);
        }

        SgQrModel sgQrModel = sgQrBuilder.build();
        System.out.println(sgQrModel.toString());

        try {
            String      netsQrModelString = QrUtils.convertSgQrToNetsQr(sgQrModel.toString());
            NetsQrModel netsQrModel       = QrUtils.parseNetsQrString(netsQrModelString);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }


    //Txn Amt = N
    //Txn Ref = N
    private void generateQr1() {
        NetsQrModel model = new NetsQrModel.Builder()
                .header(HEADER)
                .version(VERSION0)
                .qrIssuer(UEN)
                .qrExpDate(EXPIRY)
                .mid(MID)
                .merchantName(MERCHANT_NAME)
                .tid(TID)
//                .txnRefNumber(REFERENCE)
                .secretString(SECRET_STRING)
                .build();

        System.out.println(model.toString());
        try {
            QrUtils.parseNetsQrString(model.toString());
        } catch (Exception e) {
            System.out.println("ERROR");
        }
    }


    //Txn Amt = Y
    //Txn Ref = Y
    //Txn Qualifier = False
    private void generateQr2() {
        NetsQrModel model = new NetsQrModel.Builder()
                .header(HEADER)
                .version(VERSION0)
                .qrIssuer(UEN)
                .qrExpDate(EXPIRY)
                .mid(MID)
                .merchantName(MERCHANT_NAME)
                .tid(TID)
                .txnAmout(AMOUNT)
//                .txnTimestamp(TIMESTAMP)
//                .minMaxValue(MINMAX)
                .txnRefNumber(REFERENCE)
//                .qrForm(QRFORM0)
                .txnAmountQualifier(QUALIFIER_FALSE)
                .secretString(SECRET_STRING)
                .build();

        System.out.println(model.toString());
        try {
            QrUtils.parseNetsQrString(model.toString());
        } catch (Exception e) {
            System.out.println("ERROR");
        }
    }


    //Txn Amt = Y
    //Txn Ref = Len 0
    //Txn Qualifier = False
    private void generateQr3() {
        NetsQrModel model = new NetsQrModel.Builder()
                .header(HEADER)
                .version(VERSION0)
                .qrIssuer(UEN)
                .qrExpDate(EXPIRY)
                .mid(MID)
                .merchantName(MERCHANT_NAME)
                .tid(TID)
                .txnAmout(AMOUNT)
//                .txnTimestamp(TIMESTAMP)
//                .minMaxValue(MINMAX)
                .txnRefNumber(REFERENCE_EMPTY)
//                .qrForm(QRFORM0)
                .txnAmountQualifier(QUALIFIER_FALSE)
                .secretString(SECRET_STRING)
                .build();

        System.out.println(model.toString());
        try {
            QrUtils.parseNetsQrString(model.toString());
        } catch (Exception e) {
            System.out.println("ERROR");
        }
    }


    //Txn Amt = Y
    //Txn Ref = Y
    //Txn Qualifier = True
    private void generateQr4() {
        NetsQrModel model = new NetsQrModel.Builder()
                .header(HEADER)
                .version(VERSION0)
                .qrIssuer(UEN)
                .qrExpDate(EXPIRY)
                .mid(MID)
                .merchantName(MERCHANT_NAME)
                .tid(TID)
                .txnAmout(AMOUNT)
//                .txnTimestamp(TIMESTAMP)
//                .minMaxValue(MINMAX)
                .txnRefNumber(REFERENCE)
//                .qrForm(QRFORM0)
                .txnAmountQualifier(QUALIFIER_TRUE)
                .secretString(SECRET_STRING)
                .build();

        System.out.println(model.toString());
        try {
            QrUtils.parseNetsQrString(model.toString());
        } catch (Exception e) {
            System.out.println("ERROR");
        }
    }


    //Txn Amt = Y
    //Txn Ref = Y
    private void generateQr5() {
        NetsQrModel model = new NetsQrModel.Builder()
                .header(HEADER)
                .version(VERSION0)
                .qrIssuer(UEN)
                .qrExpDate(EXPIRY)
                .mid(MID)
                .merchantName(MERCHANT_NAME)
                .tid(TID)
                .txnAmout(AMOUNT)
//                .txnTimestamp(TIMESTAMP)
//                .minMaxValue(MINMAX)
                .txnRefNumber(REFERENCE)
//                .qrForm(QRFORM0)
//                .txnAmountQualifier(QUALIFIER_FALSE)
                .secretString(SECRET_STRING)
                .build();
        System.out.println(model.toString());
        try {
            QrUtils.parseNetsQrString(model.toString());
        } catch (Exception e) {
            System.out.println("ERROR");
        }
    }


    //endregion

    //region SGQR

    //txn AMT = Y
    //txn Ref = Y
    //Txn Qualifier = False
    private void generateSgQr1() {
        MerchantInfoBuilder.Nets netsInfoBuilder = new MerchantInfoBuilder.Nets()
                .qrMetadataVersion(VERSION1)
                .qrMetadataIssuerUen(UEN)
                .qrMetadataExpiryTimestamp(SGQRTIMESTAMP)
                .mid(MID)
                .tid(TID)
                .txnAmtModifier(QUALIFIER_FALSE)
                .signature("B998CBCA");
//                .signature("D54984C3");

        SgQrObject netsMerchantInfo = netsInfoBuilder.build();

        HashMap<Integer, SgQrObject> addnDataWithRef = new HashMap<>();
        addnDataWithRef.put(SgQrConsts.AddnData.ID_BILL_NUMBER,
                            new SgQrObject(SgQrConsts.AddnData.ID_BILL_NUMBER,
                                           REFERENCE.length(),
                                           REFERENCE));

        SgQrModel.Builder sgQrBuilder = new SgQrModel.Builder()
                .payloadFormatIndicator(SgQrModel.Builder.PAYLOAD_FORMAT_INDICATOR_DEFAULT)
                .pointOfInitiationMethod(SgQrModel.Builder.POINT_OF_INITIATION_STATIC)
                .addAcctInfoSg(Integer.valueOf(netsMerchantInfo.id), netsMerchantInfo)
                .sgQrIdentityInformation(SGQRID)
                .transactionAmount(AMOUNT)
                .merchantName(MERCHANT_NAME)
                .setAdditionalData(addnDataWithRef)
                .postalCode(POSTALCODE);

        SgQrModel sgQrModel = sgQrBuilder.build();
        System.out.println(sgQrModel.toString());

        try {
            String      netsQrModelString = QrUtils.convertSgQrToNetsQr(sgQrModel.toString());
            NetsQrModel netsQrModel       = QrUtils.parseNetsQrString(netsQrModelString);
            if (PRINT_DEBUG) {
                System.out.println(netsQrModel.toDebugString());
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

    //AMT
    //Ref = "" / ***
    //Txn Qualifier = False
    private void generateSgQr2() {
        MerchantInfoBuilder.Nets netsInfoBuilder = new MerchantInfoBuilder.Nets()
                .qrMetadataVersion(VERSION1)
                .qrMetadataIssuerUen(UEN)
                .qrMetadataExpiryTimestamp(SGQRTIMESTAMP)
                .mid(MID)
                .tid(TID)
                .txnAmtModifier(QUALIFIER_FALSE)
                .signature("85EA1655");
//                .signature("AC9D081A");

        SgQrObject netsMerchantInfo = netsInfoBuilder.build();

        HashMap<Integer, SgQrObject> addnDataBlankRef = new HashMap<>();
        addnDataBlankRef.put(SgQrConsts.AddnData.ID_BILL_NUMBER,
                             new SgQrObject(SgQrConsts.AddnData.ID_BILL_NUMBER,
                                            3,
                                            "***"));

        SgQrModel.Builder sgQrBuilder = new SgQrModel.Builder()
                .payloadFormatIndicator(SgQrModel.Builder.PAYLOAD_FORMAT_INDICATOR_DEFAULT)
                .pointOfInitiationMethod(SgQrModel.Builder.POINT_OF_INITIATION_STATIC)
                .addAcctInfoSg(Integer.valueOf(netsMerchantInfo.id), netsMerchantInfo)
                .sgQrIdentityInformation(SGQRID)
                .transactionAmount(AMOUNT)
                .merchantName(MERCHANT_NAME)
                .setAdditionalData(addnDataBlankRef)
                .postalCode(POSTALCODE);

        SgQrModel sgQrModel = sgQrBuilder.build();
        System.out.println(sgQrModel.toString());

        try {
            String      netsQrModelString = QrUtils.convertSgQrToNetsQr(sgQrModel.toString());
            NetsQrModel netsQrModel       = QrUtils.parseNetsQrString(netsQrModelString);
            if (PRINT_DEBUG) {
                System.out.println(netsQrModel.toDebugString());
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

    //amt
    //ref
    //qualifier = true
    private void generateSgQr3() {
        MerchantInfoBuilder.Nets netsInfoBuilder = new MerchantInfoBuilder.Nets()
                .qrMetadataVersion(VERSION1)
                .qrMetadataIssuerUen(UEN)
                .qrMetadataExpiryTimestamp(SGQRTIMESTAMP)
                .mid(MID)
                .tid(TID)
                .txnAmtModifier(QUALIFIER_TRUE)
                .signature("55F0C3AA");
//                .signature("2F2528FC");

        SgQrObject netsMerchantInfo = netsInfoBuilder.build();

        HashMap<Integer, SgQrObject> addnDataWithRef = new HashMap<>();
        addnDataWithRef.put(SgQrConsts.AddnData.ID_BILL_NUMBER,
                            new SgQrObject(SgQrConsts.AddnData.ID_BILL_NUMBER,
                                           REFERENCE.length(),
                                           REFERENCE));

        SgQrModel.Builder sgQrBuilder = new SgQrModel.Builder()
                .payloadFormatIndicator(SgQrModel.Builder.PAYLOAD_FORMAT_INDICATOR_DEFAULT)
                .pointOfInitiationMethod(SgQrModel.Builder.POINT_OF_INITIATION_STATIC)
                .addAcctInfoSg(Integer.valueOf(netsMerchantInfo.id), netsMerchantInfo)
                .sgQrIdentityInformation(SGQRID)
                .transactionAmount(AMOUNT)
                .merchantName(MERCHANT_NAME)
                .setAdditionalData(addnDataWithRef)
                .postalCode(POSTALCODE);

        SgQrModel sgQrModel = sgQrBuilder.build();
        System.out.println(sgQrModel.toString());

        try {
            String      netsQrModelString = QrUtils.convertSgQrToNetsQr(sgQrModel.toString());
            NetsQrModel netsQrModel       = QrUtils.parseNetsQrString(netsQrModelString);
            if (PRINT_DEBUG) {
                System.out.println(netsQrModel.toDebugString());
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }


    //amt
    //ref
    private void generateSgQr5() {
        MerchantInfoBuilder.Nets netsInfoBuilder = new MerchantInfoBuilder.Nets()
                .qrMetadataVersion(VERSION1)
                .qrMetadataIssuerUen(UEN)
                .qrMetadataExpiryTimestamp(SGQRTIMESTAMP)
                .mid(MID)
                .tid(TID)
//                .txnAmtModifier(QUALIFIER_TRUE)
                .signature("6F37061D");
//                .signature("1FFBFEBB");

        SgQrObject netsMerchantInfo = netsInfoBuilder.build();

        HashMap<Integer, SgQrObject> addnDataWithRef = new HashMap<>();
        addnDataWithRef.put(SgQrConsts.AddnData.ID_BILL_NUMBER,
                            new SgQrObject(SgQrConsts.AddnData.ID_BILL_NUMBER,
                                           REFERENCE.length(),
                                           REFERENCE));

        SgQrModel.Builder sgQrBuilder = new SgQrModel.Builder()
                .payloadFormatIndicator(SgQrModel.Builder.PAYLOAD_FORMAT_INDICATOR_DEFAULT)
                .pointOfInitiationMethod(SgQrModel.Builder.POINT_OF_INITIATION_STATIC)
                .addAcctInfoSg(Integer.valueOf(netsMerchantInfo.id), netsMerchantInfo)
                .sgQrIdentityInformation(SGQRID)
                .transactionAmount(AMOUNT)
                .merchantName(MERCHANT_NAME)
                .setAdditionalData(addnDataWithRef)
                .postalCode(POSTALCODE);

        SgQrModel sgQrModel = sgQrBuilder.build();
        System.out.println(sgQrModel.toString());

        try {
            String      netsQrModelString = QrUtils.convertSgQrToNetsQr(sgQrModel.toString());
            NetsQrModel netsQrModel       = QrUtils.parseNetsQrString(netsQrModelString);
            if (PRINT_DEBUG) {
                System.out.println(netsQrModel.toDebugString());
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
    }

    //no amt
    //no ref
    private void generateSgQr6() {
        MerchantInfoBuilder.Nets netsInfoBuilder = new MerchantInfoBuilder.Nets()
                .qrMetadataVersion(VERSION1)
                .qrMetadataIssuerUen(UEN)
                .qrMetadataExpiryTimestamp(SGQRTIMESTAMP)
                .mid(MID)
                .tid(TID)
//                .txnAmtModifier(QUALIFIER_FALSE)
                .signature("60B96D6C");
//                .signature("5B3E20D8");

        SgQrObject netsMerchantInfo = netsInfoBuilder.build();


        SgQrModel.Builder sgQrBuilder = new SgQrModel.Builder()
                .payloadFormatIndicator(SgQrModel.Builder.PAYLOAD_FORMAT_INDICATOR_DEFAULT)
                .pointOfInitiationMethod(SgQrModel.Builder.POINT_OF_INITIATION_STATIC)
                .addAcctInfoSg(Integer.valueOf(netsMerchantInfo.id), netsMerchantInfo)
                .sgQrIdentityInformation(SGQRID)
//                .transactionAmount(AMOUNT_NETSQR)
                .merchantName(MERCHANT_NAME)
                .postalCode(POSTALCODE);

        SgQrModel sgQrModel = sgQrBuilder.build();
        System.out.println(sgQrModel.toString());

        try {
            String netsQrModelString = QrUtils.convertSgQrToNetsQr(sgQrModel.toString());
//            String netsQrModelString = QrUtils.convertSgQrToNetsQr("00020101021133810011SG.COM.NETS012310123456789321231235959021110101254114030835036105990860B96D6C51161234567890ABCDEF5204000053037025802SG5918TEST_MERCHANT_NETS6009SINGAPORE610688833363040245");
            NetsQrModel netsQrModel = QrUtils.parseNetsQrString(netsQrModelString);
            if (PRINT_DEBUG) {
                System.out.println(netsQrModel.toDebugString());
            }
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }

    }

    //endregion

}
